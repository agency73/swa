<?php
	namespace Theme\Admin;

	use Theme\Models\Post;
	use Theme\Models\PostMeta;
	use Illuminate\Support\Collection;

	class Search {
		public static $instance = NULL;

		public static function getInstance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new Search();
			}

			return self::$instance;
		}

		private function __construct() {}

		public $query;

		/**
		 * @param string $query
		 *
		 * @return array
		 */
		public function getResults( $query = '' ) {
			$this->query    = $query;
			$search_results = [];
			$results        = [];
			if ( !empty( $query ) ) {

				//search string only if that in double quotes
				preg_match_all( '/"([^"]+)"/', $query, $matches );

				if( empty( $matches[0] ) ) {
					preg_match_all( '/[\w-\w]{2,}|[\w_\w]{2,}|[\w]{2,}/', $query, $matches );
				}

				if ( ! empty( $matches[0] ) ) {
					$matches[0][]  = $query;
					$queries_array = str_replace('"', '', $matches[0] );
					$keyed_result = new Collection();

					foreach ( $queries_array as $single_query_string ) {
						$services_found      = $this->getServices( $single_query_string );
						$related_pages_found = $this->getPagesAttachedTo( $services_found );
						$stories_found       = $this->getStories( $single_query_string );
						$landings_found      = $this->getLandings( $single_query_string );
						$pages_found         = $this->getPages( $single_query_string );
						$table_found         = $this->getTables( $single_query_string );

						foreach ( $table_found as $k => $item ) {

							//get accordions id where table placed
							$builder = PostMeta::select( 'post_id' )
							                   ->where( function ( $query ) use ( $item ) {
								                   $query->where( 'meta_value', 'LIKE', "%" . $item['post_title'] . "%" );
							                   } )
							                   ->where( "meta_key", "LIKE", 'slides' )
							                   ->get()
							                   ->toArray();

							foreach ( $builder as $item ) {
								$builder_accardeon = $item['post_id'];

								//get page id where accordions placed
								$builder = PostMeta::select( 'post_id' )
								                   ->where( 'meta_value', 'LIKE', "%$builder_accardeon%" )
								                   ->whereIn( 'post_id', function ( $qw ) {
									                   $qw->select( 'ID' )->from( "posts" )->whereRaw( 'wp_posts.post_type = "page" OR wp_posts.post_type = "landing" ' );
								                   } )
								                   ->get()
								                   ->toArray();

								foreach ( $builder as $page_result ) {

									$page_ID = $page_result['post_id'];

									//get page content
									$builder = Post::select( 'ID', 'post_title', 'post_content', 'guid' )
									               ->where( function ( $q ) use ( $page_ID ) {
										               $q->where( 'ID', 'LIKE', "%$page_ID%" );
									               } )
									               ->first()
									               ->toArray();


									$collection   = collect( [ $builder ] );
									$keyed        = $collection->keyBy( 'ID' );
									$keyed_result = $keyed_result->concat( $keyed->all() );


								}

							}

						}

						$table_search_result = $keyed_result->keyBy( 'ID' )->toArray();

						$results[] = $related_pages_found + $stories_found + $landings_found + $pages_found + $table_search_result;
					}

					foreach ( $results as $k => $result ) {
						// Merging of the numeric arrays with overwritting
						usort( $result, function ( $a, $b ) {
							return $a['post_date'] < $b['post_date'];
						} );

						if ( ! empty( $result ) ) {
							$search_results += $this->attachFields( $result, $queries_array );
						}
					}
				}
			}

			$filtered_result = super_unique( $search_results, 'ID' );

			return $filtered_result;
		}

		public function attachFields( $results, $queries_array ) {
			$fields = ['page_tag', 'page_color'];
			if ( ! empty( $results ) ) {

				foreach ( $results as $k => $result ) {

					//Need to discussed
//					$results[$k]['found_text'] = $result['found_text'];
					$results[$k]['found_text'] = $this->stripAll($result['post_content']);
					foreach ($queries_array as $query){
						$results[$k]['found_text'] = str_ireplace($query, '<span class="result">'.$query.'</span>',  $results[$k]['found_text']);
					}
					foreach($fields as $field) {
						$results[ $k ][ $field ] = $this->attachField( $result, $field );
					}
					$results[$k]['image_thumb_url'] = $this->attachImage($result);
				}

			}

			return $results;
		}

		public function attachImage( $result ) {
			$thumbnail_id = get_post_thumbnail_id( $result['ID'] );
			if ( ! $thumbnail_id ) {
				$thumbnail_id = meta( 'promo_image', $result['ID'] );
			}

			return wp_get_attachment_image_url( $thumbnail_id );
		}

		public function attachField( $result, $field ) {
			if($result['post_type'] !== 'stories') {
				return;
			}

			$related_page = meta('related_page', $result['ID']);
			return meta($field, $related_page);
		}

		public function getServices( $query ) {
			$fields_to_search = [
				'subtitle',
				'columns',
				'info',
				'text_info',
				'service_info_middle_text',
				'service_info_middle_note',
				'service_info_left_title',
				'service_info_left_text',
				'service_info_right_title',
				'service_info_right_text'
			];

			$builder          = Post::join( 'postmeta', 'posts.ID', '=', 'postmeta.post_id' );
			$builder->where( 'post_status', '=', 'publish' );
			$builder->where( 'post_type', '=', 'services' );

			$this->addConditions( $builder, $query, $fields_to_search );

			$results = $builder->get()->toArray();

			return $this->getPostText( $results );
		}

		public function addConditions( &$builder, $query, $fields_to_search ) {
			// fields conditions

			$builder->where( function ( $q ) use ( $query, $fields_to_search ) {
				// standart post fields
				$q->where( function ( $qp ) use ( $query ) {
					$qp->where( 'post_title', 'like', '%' . $query . '%' )
					   ->orWhere( 'post_content', 'like', '%' . $query . '%' );
				} );
				// parse meta data fields
				foreach ( $fields_to_search as $field ) {
					$this->getCondition( $q, $query, $field );
				}
			} );
		}

		public function getPosts( $query, $post_type, $fields_to_search ) {
			$posts = [];
			$builder = Post::join( 'postmeta', 'posts.ID', '=', 'postmeta.post_id' );
			$builder->where( 'post_status', '=', 'publish' )
			        ->where( 'post_type', '=', $post_type );

			$this->addConditions( $builder, $query, $fields_to_search );

			$results = $builder->get()->toArray();
			$results = $this->getPostText( $results );
			foreach ($results as $k => $result) {
				$posts[$k] = (array) get_post( $k );
				if(!is_string($result)) {
					//td($fields_to_search);
				}
				$posts[ $k ]['found_text'] = $this->stripAll( $result );
			}

			return $posts;
		}

		public function getStories( $query = '' ) {
			$fields_to_search = [
				'story_teaser_title',
				'story_intro',
				'story_icons_title',
				'story_testimonial_text',
				'story_testimonial_author',
				'story_content_image_text'
			];

			return $this->getPosts( $query, 'stories', $fields_to_search );
		}

		public function getTables( $query = '' ) {
			$fields_to_search = [
				'tablepress_table',
			];

			return $this->getPosts( $query, 'tablepress_table', $fields_to_search );
		}

		public function getLandings( $query = '' ) {
			$fields_to_search = [
				'landing_title',
				'landing_details_title',
				'landing_details_text',
				'landing_info_top',
				'landing_info_middle',
				'landing_info_bottom',
				'landing_info_add_title',
				'landing_info_add_text',
				'landing_text_blocks_title',
				'landing_text_blocks_subtitle',
				'landing_text_blocks',
				'landing_tables_text'
			];

			return $this->getPosts( $query, 'landing', $fields_to_search );
		}

		public function getPages( $query = '' ) {
			$fields_to_search = [
				'promo_title',
				'promo_subtitle',
				'promo_text_title',
				'promo_text',
				'home_title',
				'home_subtitle',
				'home_text',
				'message_title',
				'message_text',
				'message_author',
				'message_post',
				'message_post',
				'highlights_title',
				'view_box_title',
				'view_box_subtitle',
				'view_box_text',
				'view_box_image_text_top',
				'view_box_image_text_left',
				'view_box_image_text',
				'map_title',
				'map_subtitle',
				'map_text',
				'region_title',
				'region_subtitle',
				'region_data',
				'top_rating_title',
				'top_rating_subtitle',
				'top_rating_list',
				'success_title',
				'reporting_title',
				'reporting_subtitle',
				'reporting_text',
				'awards_title',
				'awards_subtitle',
				'awards_text',
				'archive_title',
				'archive_text',
				'archive_box',
				'gri_title',
				'gri_text',
			];

			return $this->getPosts( $query, 'page', $fields_to_search );
		}

		/**
		 * Get the search result text for providing it to output
		 *
		 * @param $results
		 *
		 * @return array
		 */
		private function getPostText( $results ) {
			$fields_found = [];

			foreach ( $results as $result ) {
				if ( ! empty( $result['meta_key'] ) && $result['meta_key'] == '_edit_last' ) {
					// check general content
					$string_found = $result['post_content'];
				} else {
					// check meta content
					$string_found = is_serialized( $result['meta_value'] ) ? unserialize( $result['meta_value'] ) : $result['meta_value'];
				}
				if ( ! isset( $fields_found[ $result['ID'] ] ) ) {
					$fields_found[ $result['ID'] ] = $string_found;
				}
			}

			return $fields_found;
		}

		private function getCondition( $builder, $query, $field ) {
			$builder->orWhere( function ( $q ) use ( $query, $field ) {
				$q->where( 'meta_key', '=', $field )
				  ->where( 'meta_value', 'like', '%' . $query . '%' );
			} );

			return $builder;
		}

		private function getPagesAttachedTo( $services_found = [] ) {
			$pages_found = [];
			$service_ids = array_keys( $services_found );

			// get all landings
			$pages = Post::getPosts( [ 'landing' ] )->toArray();
			foreach ( $pages as $page ) {
				// get attached services from every landing
				$page_services = Post::getCollectionField( $page['ID'], 'service_list', 'service_item' );
				// go through every landing to find services from search results
				foreach ( $page_services as $page_service ) {
					if ( in_array( $page_service, $service_ids ) ) {
						$pages_found[ $page['ID'] ]               = $page;
						$pages_found[ $page['ID'] ]['found_text'] = $this->getMergeFound( $services_found[ $page_service ] );
					}
				}
			}

			return $pages_found;
		}

		private function getMergeFound( $found_text ) {
			$merged_text = [];
			// Media field has an attached image ID which should not be displayed
			$filtered_fields = [ 'media', 'image' ];
			if ( is_array( $found_text ) ) {
				foreach ( $found_text as $field => $text ) {
					if ( ! in_array( $field, $filtered_fields ) ) {
						$text = $this->getMergeFound( $text );
						// TODO check why it is not working
						/*if(stripos($this->query, $text) !== false) {
							$merged_text[] = $text;
						}*/
						$merged_text[] = $this->stripAll( $text );
					}
				}

				return implode(' - ', $merged_text);
			}
			return $this->stripAll( $found_text );
		}

		private function stripAll( $text ) {
			if ( is_array( $text ) ) {
				foreach ( $text as $parts ) {
					$text = implode( ' - ', $parts );
				}
			}
			$output = trim( strip_shortcodes( strip_tags( $text ) ) );

			$max       = 500;
			$separator = '...';

			$substr       = substr( $output, 0, $max );
			$sentence_end = strrpos( $substr, '.' );

			// cut the substr result at the end of the sentence
			if ( $sentence_end !== FALSE ) {
				return substr( $substr, 0, $sentence_end ) . $separator;
			}

			return $substr;
		}
	}
	