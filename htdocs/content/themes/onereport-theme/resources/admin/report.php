<?php
	namespace Theme\Admin;
	
	use Theme\Models\Post;
	use Themosis\Page\Option;
	
	class Report {
		public function form(){
			$report_options = Option::get('report');
			$this->_getMediaUrl($report_options['report_parts'], 'report_part_thumb' );
			
			$report = [
				'cover_image' => wp_get_attachment_image_url($report_options['report_cover'], 'original'),
				'reader_link' => $report_options['report_reader'],
				'report_full' => wp_get_attachment_url($report_options['report_full']),
				'report_form' => wp_get_attachment_url($report_options['report_form']),
				'report_parts' => $report_options['report_parts'],
			];
			
			return view('layouts.report', compact('report'));
		}
		
		private function _getMediaUrl(&$source, $field) {
			foreach ($source as $k => $part) {
				$source[$k][$field] = wp_get_attachment_image_url($part[$field], 'original');
			}
			return $source;
		}
	}