<?php
namespace Theme\Admin;

use Theme\Models\Accordion;
use Themosis\Facades\Action;
use Themosis\Page\Option;

class Shortcodes {
	public function init() {
		add_shortcode( 'box', [ Shortcodes::class, 'box' ] );
		add_shortcode( 'faq', [ Shortcodes::class, 'faq' ] );
		add_shortcode( 'button', [ Shortcodes::class, 'button' ] );
		add_shortcode( 'accordion', [ Shortcodes::class, 'accordion' ] );
		add_shortcode( 'quote', [ Shortcodes::class, 'quote' ] );
		add_shortcode( 'iconstats', [ Shortcodes::class, 'iconstats' ] );
		add_shortcode( 'testimonial', [ Shortcodes::class, 'testimonial' ] );
		add_shortcode( 'imagetext', [ Shortcodes::class, 'imagetext' ] );
		add_shortcode( 'videobox', [ Shortcodes::class, 'videobox' ] );
		add_shortcode( 'infographic', [ Shortcodes::class, 'infographic' ] );
		add_shortcode( 'hl', [ Shortcodes::class, 'hyperlink' ] );
		add_shortcode( 'footnote', [ Shortcodes::class, 'footnote' ] );
		add_shortcode( 'columns', [ Shortcodes::class, 'columns' ] );
		add_shortcode( 'col', [ Shortcodes::class, 'col' ] );
		add_shortcode( 'download_report', [ Shortcodes::class, 'downloadReport' ] );
	}
	
	public static function formatFields( $fields ) {
		$data = [];
		if ( ! empty( $fields ) ) {
			foreach ( $fields as $field ) {
				$metadata = meta( $field );
				$data[ $field ] = is_string($metadata) ? do_shortcode($metadata) : $metadata;
			}
		}
		
		return $data;
	}
	
	public static function box( $atts, $content = '' ) {
		$atts = shortcode_atts( [
			'color' => 'red',
		], $atts, 'box' );
		
		return "<div class='notice' style='background: {$atts['color']}'>{$content}</div>";
	}
	
	public static function quote( $atts, $content = '' ) {
		return do_shortcode('<div class="inner-block animate-element opacity-animate">' . $content . '</div>');
	}

	public static function iconstats( $atts, $content = '' ) {

		$atts = shortcode_atts(
			[ 'num' => '' ], $atts, 'iconstats' );

		$fields = [
			'story_icons_title',
			'story_icons'
		];

		if ( ! empty( $fields ) ) {
			$field = self::formatFields( $fields );
			$result = $field['story_icons'][$atts['num']];
			return view( 'stories.icons',['iconstat' => $result]  );
		}
	}
	
	public static function testimonial( $atts, $content = '' ) {
		$fields = [
			'story_testimonial_position',
			'story_testimonial_text',
			'story_testimonial_author',
			'story_testimonial_image'
		];
		
		return view( 'stories.testimonial', self::formatFields( $fields ) );
	}
	
	public static function imagetext( $atts, $content = '' ) {
		$atts = shortcode_atts(
			[ 'num' => '' ], $atts, 'imagetext' );
		$fields = [
			'imagetext'
		];

		if ( ! empty( $fields ) ) {
			$field = self::formatFields( $fields );
			$result = !empty($field['imagetext'][$atts['num']]) ? $field['imagetext'][$atts['num']] : '';

			return view( 'stories.imagetext',['imagetext' => $result]  );
		}

	}
	
	public static function faq( $atts, $content = '' ) {
		$atts = shortcode_atts( [
			'color'     => 'red',
			'direction' => 'left',
		], $atts, 'faq' );
		
		return "<div class='faq' style='float: {$atts['direction']}; background: {$atts['color']};'>{$content}</div>";
	}
	
	public static function button( $atts, $content = '' ) {
		$atts = shortcode_atts( [
			'url' => '/',
		], $atts, 'button' );
		
		return "<a href='{$atts['url']}' class='button'>{$content}</a>";
	}
	
	public static function accordion( $atts, $content = '' ) {
		$atts = shortcode_atts( [
			'id' => '',
		], $atts, 'accordion' );
		
		if ( empty( $atts['id'] ) || ! is_numeric( $atts['id'] ) ) {
			return;
		}
		
		$accordion = Accordion::find( $atts['id'] );
		
		return view( 'layouts.accordion', $accordion->formatAccordion() );
	}
	
	public static function videobox( $atts, $content = '' ) {
		$atts = shortcode_atts( [
		], $atts, 'videobox' );
		
		return '<div class="wrap-video"><iframe class="video" src="'.$content.'" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>';
	}
	
	public static function infographic( $atts, $content = '' ) {
		$fields = [
			'story_infographic',
		];
		
		return view( 'stories.infographic', self::formatFields( $fields ) );
	}
	
	public static function hyperlink( $atts, $content = '' ) {
		$atts = shortcode_atts( [
		], $atts, 'hl' );
		
		return '<sup class="footnote-link" style="cursor: pointer">' . $content . '</sup>';
	}
	
	public static function columns( $atts, $content = '' ) {
		return apply_filters('the_content', "<div class='columns'>".trim($content)."</div>", 'columns', 'col');
	}
	
	public static function col( $atts, $content = '' ) {
		return apply_filters('the_content', "<div class='column'>".trim($content)."</div>", 'columns', 'col');
	}
	
	public static function downloadReport( $atts, $content = '' ) {
		$atts = shortcode_atts( [
		], $atts, 'download_report' );
		
		$report = new Report();
		
		return $report->form();
	}
	
	public static function filterContent( $content, $wrapper, $inner ) {
		$block = join( "|", array( $wrapper, $inner ) );
		$rep   = preg_replace( "/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content );
		$rep   = preg_replace( "/(<p>)?\[\/($block)](<\/p>|<br \/>)?/", "[/$2]", $rep );
		
		return $rep;
	}
	
	public function filterShortcode(){
		add_filter("the_content", [ Shortcodes::class, 'filterContent' ], 10, 3);
	}

	public static function footnote( $atts, $content = '' ) {
		$atts = shortcode_atts(
			[ 'number' => '' ], $atts, 'footnote' );
		$footnote = '';

		if ( ! empty( $atts['number'] ) ) {
			$footnotes = Option::get( 'footnotes' );
			if ( ! empty( $footnotes['columns'][1]['text'] ) ) {
				$str  = $footnotes['columns'][ $atts['number'] ]['text'];
				$res  = preg_replace( '/([footnote\snumber=\"[\s0-9]+"\])([\s|0-9|,]+)(\[\/footnote\])/', "<sup><a href='/footnotes-disclosures/#footnote-$2' target='_blank' class='ga-footnote'> $2 </a></sup>", $str );
				$footnote = '<span class="descriptions-popup"><span class="descriptions-popup-inner">' . $res . '</span></span>';
			}
		}

		$footnote_item = '<sup class="footnote-link" style="cursor: pointer">' . $content .$footnote . '</sup>';
		return $footnote_item;
	}
}

Action::add( 'init', Shortcodes::class );