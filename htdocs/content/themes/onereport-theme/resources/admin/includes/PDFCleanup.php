<?php

// Clear the old PDFs out of the cache
	$dir = WP_CONTENT_DIR . '/uploads/generatedPDF/';
	
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if( $file != '.' && $file != '..' && strpos($file, '.') != 0){
					// 10800 = 3 hours
					// check for files that are really old.
					if(  filemtime($dir.'/'.$file) < time() - 10800  ){
						unlink($dir.'/'.$file);
					}
				}
			}
			closedir($dh);
		}
	}