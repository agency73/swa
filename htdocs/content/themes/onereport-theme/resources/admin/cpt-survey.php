<?php
namespace Theme\Admin;
use Theme\Models\Survey;
use Theme\Models\Post;
use \Themosis\Facades\PostType;
use \Themosis\Facades\Metabox;
use \Themosis\Facades\Field;
use \Themosis\Facades\Action;

class AdminSurvey {
	/**
	 * Creating Story post type fields
	 */
	public function init() {
		PostType::make(Survey::SLUG, Survey::LABEL_PLURAL, Survey::LABEL_SINGLE)->set([
			'menu_position' => 5,
			'public'        => true,
			'supports'      => [ 'title' ],
			'labels'        => [
				'add_item' => __( 'Add Survey', 'THEME_TEXTDOMAIN' )
			],
			'menu_icon' => 'dashicons-clipboard',
		]);
		
		Metabox::make('1. How would you identify yourself (select all the apply)',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::text( 'sf-identify-yourself', [ 'title' => 'Identify yourself' ] ),
		]);

		Metabox::make('2. Which of our priority topics are most interested in (check all that apply))',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::text( 'sf-topics-interested', [ 'title' => 'Priority topics are most interested in' ] ),
		]);

		Metabox::make('3. Please rate your experience in the following areas (scale 1-5)',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::text( 'sf-rate-1-overall-site', [ 'title' => 'Overall visual appereal/design aesthetics of the site' ] ),
			Field::text( 'sf-rate-1-photos', [ 'title' => 'Photos' ] ),
			Field::text( 'sf-rate-1-videos', [ 'title' => 'Videos' ] ),
			Field::text( 'sf-rate-1-infographics-charts', [ 'title' => 'Infographics and charts' ] ),
		]);

		Metabox::make('4. Please rate your experience in the following areas (scale 1-5)',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::text( 'sf-rate-2-stories-relevant', [ 'title' => 'The stories were relevant' ] ),
			Field::text( 'sf-rate-2-stories-compelling', [ 'title' => ' The stories were compelling/engaging' ] ),
			Field::text( 'sf-rate-2-stories-topics', [ 'title' => 'The stories were about topics I care about' ] ),
		]);

		Metabox::make('5. Please rate your experience in the following areas (scale 1-5)',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::text( 'sf-rate-3-info-report', [ 'title' => 'The information I was looking for was covered in the report' ] ),
			Field::text( 'sf-rate-3-info-easy-find', [ 'title' => 'The information I was looking for was easy to find' ] ),
		]);

		Metabox::make('Do you have any other thoughts/comments you\'d like to share?',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::textarea( 'sf-your-message', [ 'title' => 'Comments' ] ),
		]);

		Metabox::make('User Info',
			Survey::SLUG, ['priority' => 'high'])->set([
			Field::text( 'sf-user-ip', [ 'title' => 'User IP' ] ),
		]);
	}
}

Action::add('init', AdminSurvey::class);