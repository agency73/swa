<?php

/**
 * Define WordPress actions for your theme.
 *
 * Based on the WordPress action hooks.
 * https://developer.wordpress.org/reference/hooks/
 *
 */

/**
 * Handle Browser Sync.
 *
 * The framework loads by default the local environment
 * where the constant BS (Browser Sync) is defined to true for development purpose.
 *
 * Note: make sure to update the script statement below based on the terminal/console message
 * when launching the "gulp watch" task.
 */
use Theme\Admin\Session;
use Theme\Models\Story;

define('ASSETS_VER', '8.0.1');

Action::add( 'init', function () {
	Asset::add( 'magnific', 'js/magnific.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
	Asset::add( 'peity', 'js/peity.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
	Asset::add( 'pageSlide', 'js/pageslide.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
	Asset::add( 'theme', 'js/theme.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
	Asset::add( 'tinymce', 'js/tinymce.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
	Asset::add( 'pageback', 'js/pageback.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
	Asset::add( 'ga', 'js/ga.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
  Asset::add( 'custom', 'js/custom.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
  Asset::add( 'modal', 'js/modal.min.js', [ 'jquery' ], ASSETS_VER, TRUE );
} );

Action::add( 'init', function () {
	Session::init();
} );

Filter::add( 'tablepress_table_output', function ( $output, $table, $render_options ) {
	$output = "<div class=\"table-wrap\">\n{$output}\n</div>";

	return $output;
}, 10, 3 );

Filter::add( 'nav_menu_css_class', function ( $classes, $item ) {
	$cpt_name = 'stories';
	if ( $cpt_name == get_post_type() && ! is_admin() ) {
		global $post;
		// - we really just want the object_id so it cane be compared to the landing ID.
		if ( ! empty( $item->object_id ) ) {
			$post_model = Story::find( $post->ID );
			if ( ! empty( $post_model->ID ) ) {
				$landing = $post_model->landing()->first();
				// check if item_object_id matches landing ID.
				if ( ! empty( $landing->meta_value ) && $item->object_id == $landing->meta_value ) {
					$classes[] = 'current-menu-item';
				}
			}
		}
	}

	return $classes;

}, 10, 2 );

// Helper function.
// Filter array with duplicates of key/value pair.
function super_unique( $array, $key ) {
	$temp_array = array();
	foreach ( $array as &$v ) {
		if ( ! isset( $temp_array[ $v[ $key ] ] ) ) {
			$temp_array[ $v[ $key ] ] =& $v;
		}
	}
	$array = array_values( $temp_array );

	return $array;
}

// Allow SVG
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

  global $wp_version;
  if ( $wp_version !== '4.7.1' ) {
    return $data;
  }

  $filetype = wp_check_filetype( $filename, $mimes );

  return [
    'ext'             => $filetype['ext'],
    'type'            => $filetype['type'],
    'proper_filename' => $data['proper_filename']
  ];

}, 10, 4 );

function add_svg_to_upload_mimes( $upload_mimes ) {
  $upload_mimes['svg'] = 'image/svg+xml';
  $upload_mimes['svgz'] = 'image/svg+xml';
  return $upload_mimes;
}

Filter::add( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

Filter::add('wp_handle_upload', 'custom_upload_filter' );

function custom_upload_filter( $file ){
    if($file['type'] == 'application/pdf') {
        $pdf = $file['file'];
        add_action('admin_notices', 'general_admin_notice');
        $opened_pdf = fopen($pdf,"r"); //open pdf to read the first line to know which version of pdf it is
        if ($opened_pdf) {
            $line_first = fgets($opened_pdf); //get fisrt line of pdf

            fclose($opened_pdf); //close pdf file

            // extract number such as 1.4 ,1.5 from first read line of pdf file
            preg_match_all('!\d+!', $line_first, $matches);

            $pdfversion = implode('.', $matches[0]);
        } else {
            die('Cant reach pdf file');
        }
        if($pdfversion != "1.4") {
            shell_exec('nohup sh -c "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -o \"' . $pdf.'new\"' . ' \"' . $pdf .'\" && rm \"' . $pdf. '\" && mv \"' . $pdf.'new\" \"' . $pdf . '\"" > /dev/null 2>&1 &');

            return $file;

        } else {
            return $file;
        }
    } else {
        return $file;
    }
}