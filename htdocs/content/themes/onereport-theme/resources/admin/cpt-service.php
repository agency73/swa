<?php
namespace Theme\Admin;
use Theme\Models\Post;
use Theme\Models\Service;
use \Themosis\Facades\PostType;
use \Themosis\Facades\Metabox;
use \Themosis\Facades\Field;
use \Themosis\Facades\Action;

class AdminService {
	/**
	 * Creating Service post type fields
	 */
	public function init() {
		PostType::make(Service::SLUG, Service::LABEL_PLURAL, Service::LABEL_SINGLE)->set([
			'menu_position' => 5,
			'public'        => true,
			'supports'      => [ 'title', 'editor' ],
			'labels'        => [
				'add_item' => __( 'Add Section', 'THEME_TEXTDOMAIN' )
			]
		]);
		
		Metabox::make( 'Text structure', Service::SLUG )->set( [
			Field::number( 'service_text_cols',
				[
					'title'   => 'Choose number of columns for the content ',
					'default' => 1
				],
				[
					'min' => '1',
					'max' => '3'
				]
			),
		] );
		
		Metabox::make('Image', Service::SLUG)->set([
			Field::media('service_image', ['title' => 'Upload promo image', 'type' => 'image']),
		]);
		
		Metabox::make('Description', Service::SLUG)->set([
			Field::text('subtitle'),
			Field::infinite('columns', [
				Field::textarea('text', ['title' => 'Text']),
			])
		]);
		
		Metabox::make('Information', Service::SLUG)->set([
			Field::infinite('info', [
				Field::media('media'),
				Field::text('title'),
				Field::textarea('text'),
			])
		]);
		
		Metabox::make( 'Service info (white circles)', Service::SLUG )->set( [
			Field::editor( 'service_info_middle_text', [ 'title' => 'Middle text' ] ),
			Field::text( 'service_info_middle_note', [ 'title' => 'Middle notice' ] ),
			Field::text( 'service_info_left_title', [ 'title' => 'Left title' ] ),
			Field::editor( 'service_info_left_text', [ 'title' => 'Left text' ] ),
			Field::media( 'service_info_left_icon', [ 'title' => 'Left icon' ] ),
			Field::text( 'service_info_right_title', [ 'title' => 'Right title' ] ),
			Field::editor( 'service_info_right_text', [ 'title' => 'Right text' ] ),
			Field::media( 'service_info_right_icon', [ 'title' => 'Right icon' ] )
		] );
		
		Metabox::make('Stories', Service::SLUG)->set([
			Field::select('stories-information', [ Post::getTitles( ['stories'] ) ], ['title' => 'Choose a story:'], ['multiple'])
		]);
		
		Metabox::make( 'Text information', Service::SLUG )->set( [
			Field::infinite( 'text_info', [
				Field::textarea( 'text', [ 'title' => 'Text' ] ),
			], [ 'title' => 'Text blocks' ] )
		] );
	}
}

Action::add('init', AdminService::class);