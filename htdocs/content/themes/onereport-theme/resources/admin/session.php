<?php
	namespace Theme\Admin;
	
	class Session {
		public static function init(){
			if(!session_id()) {
				session_start();
			}
		}
		
		public static function set($values = []) {
			if(!empty($values)) {
				foreach ($values as $k => $value) {
					$_SESSION[ $k ] = $value;
				}
			}
		}
		
		public static function get( $key ) {
			if ( empty( $_SESSION[ $key ] ) ) {
				return FALSE;
			}
			
			return $_SESSION[ $key ];
		}
		
		public static function clear( $key ) {
			unset( $_SESSION[ $key ] );
		}
		
		public static function reset() {
			session_unset();
		}
		
		public static function destroy() {
			session_destroy();
		}
	}