<?php

namespace Theme\Admin;

use \Themosis\Facades\Field;
use \Themosis\Facades\Action;
use \Themosis\Facades\Section;
use \Themosis\Facades\Page;
use Theme\Models\Post;

class Settings {
	/**
	 * Creating Settings page
	 */
	public function init() {
		$sections = static::setSections();
		$settings = static::setSettings();
		$page     = Page::make( 'general-settings', 'General settings' )->set();
		$page->addSections( $sections );
		$page->addSettings( $settings );
	}
	
	/**
	 * Formatting sections
	 *
	 * @return array
	 */
	public static function setSections() {
		$sections[] = Section::make( 'main', 'Main meta tag' );
		$sections[] = Section::make( 'header', 'Header' );
		$sections[] = Section::make( 'footer', 'Footer' );
		$sections[] = Section::make( 'footnotes', 'Footnotes' );
		$sections[] = Section::make( 'report', 'Report' );
		$sections[] = Section::make( 'google', 'Google' );
		$sections[] = Section::make( 'survey', 'Survey' );
		$sections[] = Section::make( 'stories', 'Stories Settings' );

		return $sections;
	}
	
	/**
	 * Formatting settings
	 *
	 * @return mixed
	 */
	public static function setSettings() {
		$settings['main'] = [
			Field::text( 'twitter' ),
//			Field::text( 'facebook' ),
//			Field::text( 'home-page' ),
//			Field::text( 'instagram' ),
//			Field::text( 'youTube' ),

		];
		
		$settings['header'] = [
//			Field::text( 'street_address', [ 'title' => 'Street Address'] ),
//			Field::text( 'phone', [ 'title' => 'Phone'] ),
			Field::text( 'title', [ 'title' => 'Title'] ),
			Field::media( 'theme_logo', [ 'title' => 'Logo'] )
		];
		
		$settings['footer'] = [
			Field::textarea( 'contact', [ 'title' => 'Contact Block' ] ),
			Field::textarea( 'copyright', [ 'title' => 'Contact Block' ] ),
			Field::textarea( 'social_list', [ 'title' => 'Social List' ] ),
			Field::text( 'title', [ 'title' => 'Title' ] ),
			Field::media( 'footer_logo', [ 'title' => 'Logo' ] ),
			Field::text( 'link_to', [ 'title' => 'Link to...' ] ),

		];
		
		$settings['footnotes'] = [
			Field::text( 'title', [ 'title' => 'Title'] ),
			Field::infinite( 'columns', [
				Field::textarea( 'text', [ 'title' => 'Text' ]),
			], [ 'title' => 'Columns'] ),
		];
		
		$settings['report'] = [
			Field::media( 'report_cover', [ 'title' => 'Report cover image'] ),
			Field::text( 'report_reader', [ 'title' => 'Reader link'] ),
			Field::media( 'report_full', [ 'title' => 'Full report file'] ),
			Field::media( 'report_form', [ 'title' => 'Form report file'] ),
			Field::media( 'report_part_start', [ 'title' => 'Start report file'] ),
			Field::media( 'report_part_end', [ 'title' => 'End report file'] ),
			Field::infinite('report_parts', [
				Field::media( 'report_part_thumb', [ 'title' => 'Attach thumbnail'] ),
				Field::text('report_part_title', ['title' => 'PDF name']),
				Field::media( 'report_part_file', [ 'title' => 'Attach pdf file'] ),
			], ['title' => 'Report parts']),
		];

		$settings['google'] = [
			Field::textarea( 'ga_tracking_code', [ 'title' => 'Google Analytics Tracking Code' ] ),
		];

		$settings['survey'] = [
			Field::text( 'survey_title', [ 'title' => 'Title'] ),
			Field::textarea( 'survey_subtitle', [ 'title' => 'Sub Title'] ),
			Field::media( 'survey_logo', [ 'title' => 'Survey Logo' ] ),
			Field::media( 'survey_form_bg', [ 'title' => 'Survey Form Background' ] ),
			Field::text( 'survey_first_question', [ 'title' => 'First question'] ),
			Field::text( 'survey_second_question', [ 'title' => 'Second question'] ),
			Field::text( 'survey_third_question', [ 'title' => 'Third question'] ),
			Field::text( 'survey_fourth_question', [ 'title' => 'Fourth question'] ),
			Field::text( 'survey_fifth_question', [ 'title' => 'Fifth question'] ),
			Field::text( 'survey_comments_advise', [ 'title' => 'Thoughts/comments question'] ),
		];

        $settings['stories'] = [
            Field::select('key_topic_order_1_column', [
                self::getStoriesPageTags()
            ], ['title' => 'Choose a type for 1 column:']),

            Field::select('key_topic_order_2_column', [
                self::getStoriesPageTags()
            ], ['title' => 'Choose a type for 2 column:']),

            Field::select('key_topic_order_3_column', [
                self::getStoriesPageTags()
            ], ['title' => 'Choose a type for 3 column:'])
        ];
		
		return $settings;
	}

    public static function getStoriesPageTags()
    {
        $options = array();
        $pages = Post::getPosts( [ 'landing' ] )->toArray();

        if (!empty($pages) && is_array($pages)) {
            foreach ($pages as $page) {

                if($page_tag = meta('page_tag', $page['ID'])){
                    $options[$page_tag] = $page_tag;
                }

            }
        }
        return $options;
    }
}

Action::add('init', Settings::class);