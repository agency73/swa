<?php
namespace Theme\Admin;

use \Themosis\Facades\Action;
use \Themosis\Facades\Filter;

class AdminTinyMce {
	public function admin_init() {
		if ( is_admin() ) {
			Filter::add( 'mce_external_plugins', static::class );
			Filter::add( 'mce_buttons', static::class );
		}
	}
	
	public function mce_external_plugins( $plugins ) {
		$plugins['onereport'] = themosis_theme_assets() . '/js/tinymce.min.js';
		return $plugins;
	}
	
	public function mce_buttons( $buttons ) {
		$newBtns = [
			'accordionBtn',
			'quoteBtn',
			'iconstatsBtn',
			'testimonialBtn',
			'contentimageBtn',
			'videoBtn',
			'infographicBtn',
			'hlBtn',
			'footnoteBtn',
		];
		$buttons = array_merge( $buttons, $newBtns );
		
		return $buttons;
	}
}

Action::add( 'admin_init', AdminTinyMce::class );