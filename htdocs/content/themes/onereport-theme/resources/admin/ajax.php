<?php
	use Themosis\Page\Option;
	use Theme\Admin\Session;
    use Theme\Models\Survey;

	Filter::add('themosisGlobalObject', function($data) {
		$data['nonce'] = wp_create_nonce('get-pdf-file');
		return $data;
	});
	
	Ajax::listen('generatePDF', function(){
		// Check nonce value
//		check_ajax_referer('get-pdf-file', 'security');
		
		
		$link    = '';
		$subdir = '/uploads/generatedPDF/';
		$pdf_dir = WP_CONTENT_DIR . $subdir;
		
		if ( ! empty( $_POST['filelist'] ) ) {
			
			// sanitize checkboxes
			foreach ( $_POST['filelist'] as $filelist_id ) {
				$filelist_ids[] = filter_var( $filelist_id, FILTER_SANITIZE_NUMBER_INT );
			}
			
			include_once('includes/PDFMerger.php');
			$outputPDF = new \PDFMerger\PDFMerger();
			
			// add both report parts, start and end
			$report_options = Option::get('report');
			/*echo "<pre>";
			print_r($report_options);
			exit;*/
			if ( ! empty( $report_options['report_part_start'] ) ) {
				array_unshift( $filelist_ids, $report_options['report_part_start'] );
			}
			if ( ! empty( $report_options['report_part_end'] ) ) {
				array_push( $filelist_ids, $report_options['report_part_end'] );
			}
			//print_r(count( $filelist_ids ));
			//exit;
			if ( count( $filelist_ids ) > 15 ) {
				$link = wp_get_attachment_url($report_options['report_full']);
			} else {
				// add attachment pdfs
				foreach ( $filelist_ids as $id ) {
					$pdf = get_attached_file( $id );
					$outputPDF->addPDF( $pdf, 'all' );
				}
				// create a name for the generated pdf
				$PDFName = '2019SouthwestOneReport-' . sha1( rand( 0, 1000 ) * rand( 0, 100 ) ) . '.pdf';
				$outputPDF->merge( 'file', $pdf_dir . $PDFName );
				$link = WP_CONTENT_URL . $subdir . $PDFName;
				
			}
			wp_send_json_success($link);
		}
		
		wp_send_json_error('Please, check the reports');
		include_once('includes/PDFCleanup.php');
		die();
	});


	Ajax::listen('surveyForm', function(){
		// Check nonce value
		check_ajax_referer('get-pdf-file', 'security');

		$survey_data = [];
		$survey_form = $_POST['survey'];

		foreach ( $survey_form as $key => $item ) {

			if( !empty( rtrim(  $item['value'] ) ) ) {
				$survey_data[] = [
					'name'  => $item['name'],
					'value' => $item['value']
				];
			}
		}

		if( !empty( $survey_data ) ) {
			//		// Create post
			$id = wp_insert_post( array(
				'post_title'    => 'Survey #' . date('Y-m-d H:i:s'),
				'post_date'     => date('Y-m-d H:i:s'),
	//			'post_author'   => $user_id,
				'post_type'     => Survey::SLUG,
				'post_status'   => 'private',
			) );

			if ( !empty( $id ) ) {
                foreach ($survey_data as $survey) {
                    // Add meta data
                    if ($survey['name'] !== 'sf-identify-yourself' && $survey['name'] !== 'sf-topics-interested') {
                        update_post_meta($id, $survey['name'], $survey['value']);
                    }

                    if ($survey['name'] === "sf-identify-yourself") {
                        $meta_value[] = $survey['value'];
                        $result = implode(', ', $meta_value);
                        update_post_meta($id, 'sf-identify-yourself', $result);
                    }

                    if ($survey['name'] === "sf-topics-interested") {
                        $meta_value_s[] = $survey['value'];
                        $result = implode(', ', $meta_value_s);
                        update_post_meta($id, 'sf-topics-interested', $result);
                    }
                }


				if( !empty( $_SERVER['REMOTE_ADDR'] ) ){
					add_post_meta( $id, 'sf-user-ip', $_SERVER['REMOTE_ADDR'] );
				}

				wp_send_json_success( 'Thank you for your feedback!' );
			}
		}

		wp_send_json_error('There was an error trying to send your survey. Please try again later.');
		die();
	});