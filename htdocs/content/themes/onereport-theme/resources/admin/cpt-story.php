<?php
namespace Theme\Admin;
use Theme\Models\Story;
use Theme\Models\Post;
use \Themosis\Facades\PostType;
use \Themosis\Facades\Metabox;
use \Themosis\Facades\Field;
use \Themosis\Facades\Action;
use \Themosis\Facades\Taxonomy;

class AdminStory {
	/**
	 * Creating Story post type fields
	 */
	public function init() {
		PostType::make(Story::SLUG, Story::LABEL_PLURAL, Story::LABEL_SINGLE)->set([
			'menu_position' => 4,
			'public'        => true,
			'supports'      => [ 'title', 'editor', 'thumbnail' ],
			'labels'        => [
			'add_item'      => __( 'Add Story', 'THEME_TEXTDOMAIN' )
			]
		]);
		
		Metabox::make('Information', Story::SLUG, ['priority' => 'high'])->set([
			Field::select( 'related_page', [ Post::formatSelectTitles( ['landing'] ) ], [ 'title' => 'Page the story attached to:' ] ),
			Field::media('promo_image', [ 'title' => 'Promo image', 'type' => 'image' ]),
		]);
		
		Metabox::make('Story teaser title', Story::SLUG, ['priority' => 'high'])->set([
			Field::text( 'story_teaser_title', [ 'title' => 'Teaser title' ] ),
		]);

		Metabox::make('Story Hover content', Story::SLUG, ['priority' => 'high'])->set([
			Field::text( 'story_hover_content', [ 'title' => 'Hover Content' ] ),	
			Field::text( 'story_sdg_title', [ 'title' => 'SDG Title' ] ),
			//Field::text( 'story_sdg_title_link', [ 'title' => 'SDG Title Link' ] ),		
			Field::infinite( 'story_sdg_images', [
				Field::media( 'story_sdg_image', [ 'title' => 'Add SDG icon', 'type' => 'image' ] ),				
			], [ 'title' => 'SDG Icons', 'limit' => Story::LIMIT_SDG_ICON  ] ),
		]);

		Metabox::make('Story GRI & SASB Entries', Story::SLUG, ['priority' => 'high'])->set([
			Field::text( 'story_gris_title', [ 'title' => 'GRI Title' ] ),
			//Field::text( 'story_gris_title_link', [ 'title' => 'GRI Title Link' ] ),
			Field::infinite( 'story_gris', [
				Field::text( 'story_gri', [ 'title' => 'Add GRI index']),				
			], [ 'title' => 'GRI index'] ),
			
			Field::text( 'story_sasbs_title', [ 'title' => 'SASB Title' ] ),
			//Field::text( 'story_sasbs_title_link', [ 'title' => 'SASB Title Link' ] ),
			Field::infinite( 'story_sasbs', [
				Field::text( 'story_sasb', [ 'title' => 'Add SASB index'] ),				
			], [ 'title' => 'SASB index'] ),
		]);
				
		Metabox::make('Story intro - [quote] shortcode', Story::SLUG, ['priority' => 'high'])->set([
			Field::textarea( 'story_intro', [ 'title' => 'Intro' ] ),
		]);

        Metabox::make('Testimonial secondary [testimonial-secondary] shortcode', Story::SLUG, ['priority' => 'high'])->set([
            Field::textarea( 'testimonial_secondary_text', [ 'title' => 'Testimonial text' ] ),
            Field::text( 'testimonial_secondary_author', [ 'title' => 'Testimonial author' ] ),
            Field::media( 'testimonial_secondary_image', [ 'title' => 'Testimonial image', 'type' => 'image' ] ),
        ]);
		
		Metabox::make('Story icons - [iconstats] shortcode', Story::SLUG, ['priority' => 'high'])->set([
			Field::text( 'story_icons_title', [ 'title' => 'Intro' ] ),
			Field::infinite( 'story_icons', [
				Field::media( 'image', [ 'title' => 'Add icon', 'type' => 'image' ] ),
				Field::text( 'title', [ 'title' => 'Icon title' ] ),
				Field::text( 'description', [ 'title' => 'Icon description' ] ),
			], [ 'title' => 'Icons' ] ),
		]);
		
		Metabox::make('Testimonial box - [testimonial] shortcode', Story::SLUG, ['priority' => 'high'])->set([
			Field::radio('story_testimonial_position', [ 'horizontal', 'vertical' ], ['title' => 'Check testimonial position']),
			Field::textarea( 'story_testimonial_text', [ 'title' => 'Testimonial text' ] ),
			Field::text( 'story_testimonial_author', [ 'title' => 'Testimonial author' ] ),
			Field::media( 'story_testimonial_image', [ 'title' => 'Testimonial image', 'type' => 'image' ] ),
		]);

		Metabox::make( 'Image box - [imagetext] shortcode', Story::SLUG, [ 'priority' => 'high' ] )->set( [
			Field::infinite( 'imagetext', [
				Field::media( 'story_content_image', [ 'title' => 'Main image', 'type' => 'image' ] ),
				Field::radio( 'story_content_image_position', [
					'half',
					'full-screen'
				], [ 'title' => 'Check image position' ] ),
				Field::text( 'story_content_image_text_top', [ 'title' => 'Top text position', 'default' => '55%' ] ),
				Field::text( 'story_content_image_text_left', [ 'title' => 'Left text position', 'default' => '50%' ] ),
				Field::textarea( 'story_content_image_text', [ 'title' => 'Image text' ] ),
			] )
		] );
		
		Metabox::make('Fast Fact - [infographic] shortcode', Story::SLUG, ['priority' => 'high'])->set([
			Field::media( 'story_infographic', [ 'title' => 'Upload image', 'type' => 'image' ] ),
		]);

		// Key Topic  Taxonomy
		$key_topic_taxonomy = Taxonomy::make( Story::TAXONOMY_TOPIC_SLUG, [ Story::SLUG ], 'Key Topic', 'Key Topic' )->set( [
			'public'       => true,
			'rewrite'      => true,
			'query_var'    => false,
			'hierarchical' => true,
			'meta_box_cb'  => null,
			'separate_items_with_commas' => false
		]);

        $key_topic_taxonomy -> addFields([
            Field::text('order'),
        ]);

        $key_topic_taxonomy -> sanitize([
            'order' => ['num'],
        ]);

		$story = new Story();
		$info = '';

		if( isset( $_REQUEST['post'] ) ){
			$story = Story::find( $_REQUEST['post'] );
		} else {
			$info = 'Please save a post and then choose value!';
		}

		/* skip action when move to trash */
		$stories = ('trash' != $_REQUEST['action']) ? $story->getAdditionalStoriesForField() : array();

		Metabox::make('Explore Additional Stories Blocks', Story::SLUG, ['priority' => 'high'])->set([
			Field::infinite( 'story_add_infinite', [
				Field::select('story_add_infinite_block', $stories,	['title' => 'Story', 'info' => $info ] ),
			], [ 'title' => 'Blocks', 'limit' => Story::LIMIT_ADDITIONAL_STORIES ] ),

		]);
	}
}

Action::add('init', AdminStory::class);