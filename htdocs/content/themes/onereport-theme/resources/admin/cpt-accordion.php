<?php
namespace Theme\Admin;

use Theme\Models\Accordion;
use \Themosis\Facades\PostType;
use \Themosis\Facades\Metabox;
use \Themosis\Facades\Field;
use \Themosis\Facades\Action;

class AdminAccordion {
	/**
	 * Creating Accordion post type fields
	 */
	public function init() {
		PostType::make(Accordion::SLUG, Accordion::LABEL_PLURAL, Accordion::LABEL_SINGLE)->set([
			'menu_position' => 8,
			'public'        => true,
			'supports'      => [ 'title' ],
			'labels'        => [
				'add_item' => __( 'Add Section', 'THEME_TEXTDOMAIN' )
			]
		]);
		
		Metabox::make('Slides', Accordion::SLUG)->set([
			Field::infinite('slides', [
				Field::text('title', ['title' => 'Title']),
				Field::textarea('text', ['title' => 'Content']),
				Field::media( 'image', [ 'title' => 'Image', 'type' => 'image' ] ),
                Field::media( 'doc', [ 'title' => 'Upload PDF'] ),
                Field::text( 'table_content', [ 'title' => 'Table Content' ] ),
			], ['title' => 'Slide list'])
		]);
	}
}

Action::add('init', AdminAccordion::class);