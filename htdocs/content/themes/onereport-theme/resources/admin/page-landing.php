<?php
namespace Theme\Admin;
use Theme\Models\Post;
use \Themosis\Facades\PostType;
use \Themosis\Facades\Metabox;
use \Themosis\Facades\Field;
use \Themosis\Facades\Action;

class AdminLanding {
	/**
	 * Creating Landing post type fields
	 */
	public function init() {
		PostType::make(Post::LANDING_SLUG, Post::LANDING_LABEL_PLURAL, Post::LANDING_LABEL_SINGLE)->set([
			'menu_position' => 5,
			'public'        => true,
			'supports'      => [ 'title', 'editor' ],
			'labels'        => [
				'add_item' => __( 'Add Landing', 'THEME_TEXTDOMAIN' )
			]
		]);
		
		Metabox::make( 'Promo image', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_title_button', [ 'title' => 'Banner Button Title' ] ),
			Field::media( 'promo_image', [ 'title' => 'Upload image', 'type' => 'image' ] ),
			Field::media( 'landing_mobile_image', [ 'title' => 'Upload mobile image', 'type' => 'image' ] ),
		] );
		
		/*Metabox::make( 'Page details', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_title', [ 'title' => 'Title' ] ),
			Field::text( 'landing_details_title', [ 'title' => 'Numbers' ] ),
			Field::media( 'landing_details_icon', [ 'title' => 'Icon', 'type' => 'image' ] ),
			Field::editor( 'landing_details_text', [ 'title' => 'Text' ] ),
		] );*/
		Metabox::make( 'Page details', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_title', [ 'title' => 'Title' ] ),
			Field::media( 'landing_details_icon1', [ 'title' => 'Details icon 1', 'type' => 'image' ] ),
            Field::text( 'landing_details_title1', [ 'title' => 'Details title 1'] ),
            Field::text( 'landing_details_subtitle1', [ 'title' => 'Details subtitle 1'] ),
            Field::media( 'landing_details_icon2', [ 'title' => 'Details icon 2', 'type' => 'image' ] ),
            Field::text( 'landing_details_title2', [ 'title' => 'Details title 2'] ),
            Field::text( 'landing_details_subtitle2', [ 'title' => 'Details subtitle 2'] ),
            Field::media( 'landing_details_icon3', [ 'title' => 'Details icon 3', 'type' => 'image' ] ),
            Field::text( 'landing_details_title3', [ 'title' => 'Details title 3'] ),
            Field::text( 'landing_details_subtitle3', [ 'title' => 'Details subtitle 3'] ),
		] );

		Metabox::make( 'PDF Download Data', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_one_report_pdf_btn_name', [ 'title' => 'One Report PDF Button Name' ]),			
			Field::media( 'landing_one_report_pdf', [ 'title' => 'One Report PDF'] ),
			Field::text( 'landing_sasb_index_btn_name', [ 'title' => 'SASB Index Button Name' ]),	
            Field::media( 'landing_sasb_index', [ 'title' => 'SASB Index'] ),
            Field::text( 'landing_gri_index_btn_name', [ 'title' => 'GRI Index Button Name' ]),	
            Field::media( 'landing_gri_index', [ 'title' => 'GRI Index'] ),
            Field::text( 'landing_people_fact_sheet_btn_name', [ 'title' => 'People Fact Sheet Button Name' ]),	
            Field::media( 'landing_people_fact_sheet', [ 'title' => 'People Fact Sheet'] ),
		] );

		Metabox::make( 'Datatable', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_datatable_info', [ 'title' => 'Datatable Info' ]),
		] );
		
		/*Metabox::make( 'Statistic information', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
            Field::select('landing_info_select', [['infographic', 'video',]], ['title' => 'Choose a Statistic information:']),
            Field::text( 'landing_info_video', [ 'title' => 'Video' ] ),
            Field::media( 'landing_info_image', [ 'title' => 'Image', 'type' => 'image' ] ),
            Field::text( 'landing_info_top', [ 'title' => 'Top text' ] ),
            Field::text( 'landing_info_middle', [ 'title' => 'Middle text' ] ),
            Field::text( 'landing_info_bottom', [ 'title' => 'Bottom text' ] ),
		] );*/
		
		/*Metabox::make( 'Additional information', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_info_add_title', [ 'title' => 'Title' ] ),
			Field::media( 'landing_info_add_icon', [ 'title' => 'Icon', 'type' => 'image' ] ),
			Field::editor( 'landing_info_add_text', [ 'title' => 'Text' ] ),
		] );*/
		
		Metabox::make( 'Page tag', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'page_tag', [ 'title' => 'Tag' ]),
			Field::color( 'page_color', [ 'title' => 'Color for story teaser', 'default' => '#d5152e' ] ),
		] );
		
		Metabox::make( 'Services list', Post::LANDING_SLUG )->set( [
			Field::infinite( 'service_list', [
				Field::select( 'service_item', [ Post::getTitles( ['services'] ) ], [ 'title' => 'Service' ] )
			], [ 'title' => 'Services' ] ),
		] );


		Metabox::make( 'Tables box', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set( [
			Field::text( 'landing_tables_title', [ 'title' => 'Title' ] ),
			Field::text( 'landing_tables_anchor',
				[
					'title' => 'Anchor',
					'info'  => 'Add a string to use as an anchor. For example: this-is-anchor.<br /> 
								You can use it by adding to page url.<br /> 
								For example: "http://your-site.com/page-url/#this-is-anchor".',
				]
			),
			Field::editor( 'landing_tables_text', [ 'title' => 'Text' ] ),
		] );
		
		/*Metabox::make( 'Landing text blocks', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
			Field::text( 'landing_text_blocks_title', [ 'title' => 'Title' ] ),
			Field::text( 'landing_text_blocks_subtitle', [ 'title' => 'Subtitle' ] ),
			Field::media( 'landing_text_blocks_image', [ 'title' => 'Upload image', 'type' => 'image' ] ),
			Field::infinite( 'landing_text_blocks', [
				Field::text( 'title', [ 'title' => 'Title' ] ),
				Field::textarea( 'text', [ 'title' => 'Text' ] )
			], [ 'title' => 'Create the block' ] ),
		] );*/

        Metabox::make( 'System map', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
            Field::text( 'map_title', [ 'title' => 'Title'] ),
            Field::text( 'map_subtitle', [ 'title' => 'Subtitle'] ),
            Field::editor( 'map_text', [ 'title' => 'Text'] ),
            Field::media( 'map_image', [ 'title' => 'Upload image', 'type' => 'image' ] ),
            Field::media( 'map_background', [ 'title' => 'Upload background image', 'type' => 'image' ] ),
        ] );

        Metabox::make( 'Region box', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
            Field::text( 'region_title', [ 'title' => 'Title'] ),
            Field::text( 'region_subtitle', [ 'title' => 'Subtitle'] ),
            Field::radio( 'region_toggle_type',	[ 'Image', 'Chart' ], [ 'title' => 'Toggle Type', 'default' => 'Image' ] ),
            Field::media( 'region_data_image',
                [ 'title' => 'Upload data image', 'type' => 'image' ]
            ),
            Field::infinite( 'region_data_chart', [
                Field::color( 'region_data_chart_color', [ 'title' => 'Color' ] ),
                Field::text( 'region_data_chart_name', [ 'title' => 'Name'] ),
                Field::number( 'region_data_chart_data', [ 'title' => 'Data'] ),
            ], [ 'title' => 'Chart' ] ),
        ] );

        Metabox::make( 'Top rating', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
            Field::text( 'top_rating_title', [ 'title' => 'Title'] ),
            Field::text( 'top_rating_subtitle', [ 'title' => 'Subtitle'] ),
            Field::infinite( 'top_rating_list', [
                Field::text( 'code', [ 'title' => 'Code'] ),
                Field::text( 'city', [ 'title' => 'City'] ),
            ], [ 'title' => 'Add item to the list' ] ),
        ] );

        Metabox::make( 'Sharing our Success', Post::LANDING_SLUG, [ 'priority' => 'high' ] )->set([
            Field::text( 'success_title', [ 'title' => 'Title'] ),

            /*central image*/
            Field::media( 'success_central_image', [ 'title' => 'Upload Central success image', 'type' => 'image' ] ),
            Field::text('success_sub_title_central',[ 'title' => 'Success sub title central' ]),
            Field::text('success_desc_central',[ 'title' => 'Success description central']),


            /*left image*/
            Field::media( 'success_left_image', [ 'title' => 'Upload Left success image', 'type' => 'image' ] ),
            Field::text('success_sub_title_left',[ 'title' => 'Success sub title left' ]),
            Field::text('success_desc_left',[ 'title' => 'Success description left']),

            /*right image*/
            Field::media( 'success_right_image', [ 'title' => 'Upload Right success image', 'type' => 'image' ] ),
            Field::text('success_sub_title_right',[ 'title' => 'Success sub title right' ]),
            Field::text('success_desc_right',[ 'title' => 'Success description right']),
        ] );

       /* Metabox::make('Testimonial box', Post::LANDING_SLUG, ['priority' => 'low'])->set([
            Field::radio('story_testimonial_position', [ 'horizontal', 'vertical' ], ['title' => 'Check testimonial position']),
            Field::textarea( 'story_testimonial_text', [ 'title' => 'Testimonial text' ] ),
            Field::text( 'story_testimonial_author', [ 'title' => 'Testimonial author' ] ),
            Field::media( 'story_testimonial_image', [ 'title' => 'Testimonial image', 'type' => 'image' ] ),
        ]);
*/
		add_filter( 'post_type_link', array( $this, 'removeSlug' ), 10, 3 );
		add_action( 'pre_get_posts', array( $this, 'parseRequest' ) );
	}
	
	/**
	 * Remove post type slug from url
	 * @param $post_link
	 * @param $post
	 * @param $leavename
	 *
	 * @return mixed
	 */
	public function removeSlug( $post_link, $post, $leavename ) {
		if ( 'landing' != $post->post_type || 'publish' != $post->post_status ) {
			return $post_link;
		}
		$post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
		
		return $post_link;
	}
	
	/**
	 * Parse url without removed slug
	 *
	 * @param $query
	 */
	public function parseRequest( $query ) {
		if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
			return;
		}
		
		if ( ! empty( $query->query['name'] ) ) {
			$query->set( 'post_type', array( 'post', 'landing', 'page' ) );
		}
	}
}

Action::add( 'init', AdminLanding::class );