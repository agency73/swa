<?php
namespace Theme\Admin;

class Cookie {
    public static function set($name, $value = "", $expire = 0, $path = "") {
        setcookie($name, $value, $expire, $path);
    }

    public static function get( $key ) {
        if ( empty( $_COOKIE[ $key ] ) ) {
            return FALSE;
        }

        return $_COOKIE[ $key ];
    }

    public static function clear( $key ) {
        unset( $_COOKIE[ $key ] );
    }
}