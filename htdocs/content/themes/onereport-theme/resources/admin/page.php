<?php
namespace Theme\Admin;
use Theme\Models\Post;
use \Themosis\Facades\Metabox;
use \Themosis\Facades\Field;
use \Themosis\Facades\Action;
use Theme\Models\Story;

class AdminPage {
	public function init() {
		/**
		 * Creating page fields
		 */
		
		$home = (int) get_option( 'page_on_front' );

		if ( themosis_is_post( $home ) ) {
			Metabox::make( 'Promo image', 'page', [ 'priority' => 'high' ] )->set([
				Field::media( 'promo_image', [ 'title' => 'Promo image', 'type' => 'image' ] ),
				Field::text( 'promo_title', [ 'title' => 'Title' ] ),
				Field::text( 'promo_subtitle', [ 'title' => 'Subtitle' ] ),
				Field::text( 'promo_url', [ 'title' => 'Add link' ] ),
                Field::text( 'promo_linkable_text', [ 'title' => 'Linkable text' ] ),
            ] );

			Metabox::make( 'Home description', 'page', [ 'priority' => 'high' ] )->set([
                Field::media( 'home_image', [ 'title' => 'Home image', 'type' => 'image' ] ),
				Field::text( 'home_title', [ 'title' => 'Title' ] ),
				Field::text( 'home_subtitle', [ 'title' => 'Subtitle' ] ),
                Field::text( 'home_author', [ 'author' => 'text' ] ),
			] );

            Metabox::make( 'Video block', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'video_url', [ 'title' => 'video' ] ),
                Field::media( 'view_box_image', [ 'title' => 'Upload image', 'type' => 'image' ] ),
            ] );

            Metabox::make( 'View box', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'view_box_title', [ 'title' => 'Title' ] ),
                Field::text( 'view_box_subtitle', [ 'title' => 'Subtitle' ] ),
                Field::editor( 'view_box_text', [ 'title' => 'Text' ] ),
                //Field::media( 'view_box_image', [ 'title' => 'Upload image', 'type' => 'image' ] ),
                /*Field::text( 'view_box_image_text_top', [ 'title' => 'Top text position' ] ),
                Field::text( 'view_box_image_text_left', [ 'title' => 'Left text position' ] ),*/
                Field::editor( 'view_box_image_text', [ 'title' => 'Image text' ] ),
            ] );

            Metabox::make( 'Slider section', 'page' )->set( [
                
                Field::infinite( 'slider', [
                    Field::media( 'slider_image', [ 'title' => 'Slider Background image', 'type' => 'image' ] ),
                    Field::text( 'slider_title', [ 'title' => 'Slider title' ] ),
                    Field::text( 'story_title', [ 'title' => 'Story title'] ),                                        
                    Field::textarea( 'text', [ 'title' => 'Write the text'] ),
                    
                    //Field::media( 'slider_background_image1', [ 'title' => 'Slider Background image 1', 'type' => 'image' ] ),
                    Field::media( 'slider_icon1', [ 'title' => 'Slider icon 1', 'type' => 'image' ] ),
                    Field::text( 'slider_title1', [ 'title' => 'Slider title 1'] ),
                    Field::text( 'slider_subtitle1', [ 'title' => 'Slider subtitle 1'] ),                                        
                    Field::media( 'slider_icon2', [ 'title' => 'Slider icon 2', 'type' => 'image' ] ),
                    Field::text( 'slider_title2', [ 'title' => 'Slider title 2'] ),
                    Field::text( 'slider_subtitle2', [ 'title' => 'Slider subtitle 2'] ),                
                    Field::media( 'slider_icon3', [ 'title' => 'Slider icon 3', 'type' => 'image' ] ),
                    Field::text( 'slider_title3', [ 'title' => 'Slider title 3'] ),
                    Field::text( 'slider_subtitle3', [ 'title' => 'Slider subtitle 3'] ),
                    Field::text( 'slider_button_text', [ 'title' => 'Button Slider text' ] ),
                    Field::text( 'url', [ 'title' => 'Button Slider url'] ),
                    //Field::text( 'url', [ 'title' => 'Url'] ),
                ], [ 'title' => 'Add content to the slider' ] ),
                Field::text( 'slider_button_text', [ 'title' => 'Button text'] ),
            ] );

            Metabox::make( 'Supporting Global Goals section', 'page', [ 'priority' => 'high' ] )->set([
                Field::media( 'goals_image', [ 'title' => 'Goals image', 'type' => 'image' ] ),
                Field::text( 'goals_title', [ 'title' => 'Title' ] ),
                Field::text( 'goals_subtitle', [ 'title' => 'Goals Description' ] ),
                Field::text( 'goals_button_text', [ 'title' => 'Button Goals text' ] ),
                Field::text( 'goals_button_url', [ 'title' => 'Button Goals url' ] ),
            ] );
            
            $story = new Story();
            $info = '';            
            $stories = $story->getStoriesForField();            
            Metabox::make( 'Our Stories section', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'stories_title', [ 'title' => 'Title' ] ),
                Field::text( 'stories_subtitle', [ 'title' => 'Stories Description' ] ),            
                Field::infinite( 'story_add_infinite', [
                    Field::select('story_add_infinite_block', $stories, ['title' => 'Story', 'info' => $info ] ),
                ], [ 'title' => 'Stories', 'limit' => Story::LIMIT_ADDITIONAL_STORIES ] ),
                Field::text( 'stories_button_text', [ 'title' => 'Stories text' ] ),
                Field::text( 'stories_button_url', [ 'title' => 'Stories url' ] ),
            ] );

            /*Metabox::make( 'View stories section', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'stories_button_text', [ 'title' => 'Button stories text' ] ),
                Field::text( 'stories_button_url', [ 'title' => 'Button stories url' ] ),
            ] );*/

            /*Metabox::make( 'Reporting box', 'page', [ 'priority' => 'high' ] )->set([
                Field::media( 'reporting_image', [ 'title' => 'Upload image', 'type'=>'image' ] ),
                Field::text( 'reporting_title', [ 'title' => 'Title' ] ),
                Field::text( 'reporting_subtitle', [ 'title' => 'Subtitle' ] ),
                Field::editor( 'reporting_text', [ 'title' => 'Text'] ),
            ] );*/

            Metabox::make( 'Awards box', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'awards_title', [ 'title' => 'Title' ] ),
                Field::text( 'awards_subtitle', [ 'title' => 'Subtitle' ] ),
                Field::editor( 'awards_text', [ 'title' => 'Text'] ),
                Field::infinite( 'awards_images', [
                    Field::media( 'image', [ 'title' => 'Upload image'] ),
                ], [ 'title' => 'Add image to the list' ] ),
            ] );

		} else {
			Metabox::make( 'Page background', 'page', [ 'priority' => 'high' ] )->set([
				Field::media( 'promo_image', [ 'title' => 'Promo image', 'type' => 'image' ] ),
			] );

            Metabox::make( '2019 at a Glance', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'glance_title', [ 'title' => 'Title' ] ),
                Field::editor( 'glance_text', [ 'title' => 'Text' ] ),
            ] );

            Metabox::make( 'Our Approach Section', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'our_approach_title', [ 'title' => 'Title' ] ),           
                Field::editor( 'our_approach_detail', [ 'title' => 'Details' ] ),
                /*Field::media( 'our_approach_image', [ 'title' => 'Image', 'type' => 'image' ] ),
                Field::media( 'our_approach_doc', [ 'title' => 'Upload PDF'] ),
                Field::text( 'our_approach_table_content', [ 'title' => 'Table Content' ] ),*/
            ] );

            Metabox::make( 'Support the Global Goals Section', 'page', [ 'priority' => 'high' ] )->set([
                Field::text( 'hw_goals_title', [ 'title' => 'Title' ] ),
                Field::text( 'hw_goals_description', [ 'title' => 'Goals Description' ] ),
                Field::infinite( 'hw_goals_icons_data', [
                    Field::media( 'hw_goals_icon', [ 'title' => 'Image', 'type' => 'image' ] ),
                    Field::text( 'hw_goals_icon_title', [ 'title' => 'Title' ] ),
                    Field::textarea( 'hw_goals_icon_detail', [ 'title' => 'Details' ] ),
                ], [ 'title' => 'SDG Icons Data', 'limit' => post::LIMIT_SDG_IMAGES ] ),                
            ] );

            /*Metabox::make( 'One report tabs', 'page' )->set( [
                Field::infinite( 'tabs', [
                    Field::text( 'tab_title', [ 'title' => 'Title'] ),
                    Field::color( 'color', [ 'title' => 'Write color'] ),
                    Field::select('area_one_media', [['image', 'video',]], ['title' => 'Choose a media for area 1:']),
                    Field::media( 'area_one_image', [ 'title' => 'Image for area 1', 'type' => 'image'] ),
                    Field::text( 'area_one_video', [ 'title' => 'Video for area 1' ] ),
                    Field::text( 'area_one_title', [ 'title' => 'Title for area 1'] ),
                    Field::textarea( 'area_one_text', [ 'title' => 'Write the text to area 1'] ),
                    Field::text( 'area_one_url', [ 'title' => 'Url for area 1'] ),
                    Field::text( 'area_two_title', [ 'title' => 'Title for area 2'] ),
                    Field::textarea( 'area_two_text', [ 'title' => 'Write the text to area 2'] ),
                    Field::text( 'area_two_url', [ 'title' => 'Url for area 2'] ),
                    Field::media( 'area_three_image', [ 'title' => 'Upload image for area 3', 'type' => 'image'] ),
                    Field::text( 'url_page', [ 'title' => 'Url page'] ),
                ], [ 'title' => 'Add content to the tabs' ] ),
                Field::text( 'tabs_button_text', [ 'title' => 'Tabs button text'] ),
            ] );*/

			Metabox::make( 'Archive box', 'page', [ 'priority' => 'high' ] )->set([
				Field::text( 'archive_title', [ 'title' => 'Title' ] ),
				Field::text( 'archive_link_text', [ 'title' => 'Button text' ] ),
				Field::editor( 'archive_text', [ 'title' => 'Text' ] ),
				Field::infinite( 'archive_box', [
					Field::text( 'title', [ 'title' => 'Title' ] ),
					Field::media( 'doc', [ 'title' => 'Upload doc'] ),
					Field::media( 'image', [ 'title' => 'Upload thumbnail', 'type' => 'image'] ),
				], [ 'title' => 'Add archive to the list' ] ),
			] );

			Metabox::make( 'GRI box', 'page', [ 'priority' => 'high' ] )->set([
				Field::text( 'gri_title', [ 'title' => 'Title' ] ),
				Field::editor( 'gri_text', [ 'title' => 'Text' ] ),
			] );
		}
	}
}

Action::add( 'init', AdminPage::class );