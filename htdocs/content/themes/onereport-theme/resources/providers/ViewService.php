<?php
namespace Theme\Providers;

use Themosis\Foundation\ServiceProvider;
use Themosis\Page\Option;

class ViewService extends ServiceProvider {
	/**
	 * Define Blade directives.
	 */
	public function register() {
		$blade = container( 'view' )
			->getEngineResolver()
			->resolve( 'blade' )
			->getCompiler();
		
		$blade->directive( 'menu', function ( $expression ) {
			return '<?php wp_nav_menu(' . $expression . '); ?>';
		} );
		
		$blade->directive( 'sidebar', function ( $sidebar_name ) {
			return '<?php dynamic_sidebar( ' . $sidebar_name . ' ); ?>';
		} );
		
		$blade->directive( 'report', function () {
			return '<?php echo do_shortcode( "[download_report]" ); ?>';
		} );
	}
	
	public function boot() {
		view()->composer( 'layouts.navigation', function ( $view ) {
			$view->with( 'header', Option::get( 'header' ) );
		} );
		
		view()->composer( 'layouts.footinfo', function ( $view ) {
			$view->with( 'footer', Option::get( 'footer' ) );
		} );
		
		view()->composer( 'layouts.footnotes', function ( $view ) {
			$view->with( 'footnotes', Option::get( 'footnotes' ) );
		} );

		view()->composer( 'layouts.header', function ( $view ) {
			$view->with( 'google', Option::get( 'google' ) );
		} );

		view()->composer( 'layouts.surveyform', function ( $view ) {
			$view->with( 'survey', Option::get( 'survey' ) );
		} );
	}
}