<?php
	
namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Theme\Models\Service;

class ServiceController extends BaseController {
	public function index( $post ) {
		$service = Service::find( $post->ID );
		
		return view( 'pages.service', $service->formatService() );
	}
}
