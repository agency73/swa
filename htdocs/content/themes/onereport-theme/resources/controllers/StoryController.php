<?php
	
namespace Theme\Controllers;

use Theme\Models\Story;
use Themosis\Route\BaseController;

class StoryController extends BaseController {
	public function index( $post ) {
		$story = Story::find( $post->ID );

		return view( 'pages.story', $story->formatStory() );
	}
}
