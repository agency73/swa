<?php
	
	namespace Theme\Controllers;
	
	use Theme\Admin\Session;
	use Themosis\Route\BaseController;
	use Theme\Models\Post;
	
	class LandingController extends BaseController {
		public function index( $post ) {
			$post = Post::find( $post->ID );
			Session::set(['landing_id' => $post->ID]);
			Session::clear( 'home' );
			return view( 'pages.landing', $post->formatLandingPage() );
		}
	}
