<?php
	
namespace Theme\Controllers;

use Theme\Admin\Session;
use Theme\Models\Story;
use Themosis\Foundation\Request;
use Themosis\Route\BaseController;
use Theme\Models\Post;
use Theme\Admin\Search;

class PageController extends BaseController {
	public function index( $post ) {
		$post = Post::find( $post->ID );
		
		return view( 'pages.page', $post->formatPage() );
	}
	
	public function home( $post ) {
		$post = Post::find( $post->ID );
		Session::clear( 'landing_id' );
		Session::set(['home' => true]);

		return view( 'pages.home', $post->formatHomePage() );
	}

	public function allStories( $post ) {
		$post = Post::find( $post->ID );
		Session::set(['landing_id' => $post->ID]);
		Session::clear( 'home' );
		return view( 'pages.all-stories', $post->formatAllStoriesLanding() );
	}

	public function footnotes( $post ) {
		return view( 'pages.footnotes' );
	}
	
	public function search() {
		// TODO Add filter for marking the results
		$query   = stripslashes(Request::capture()->input( 's' ));

		$search  = Search::getInstance();
		$results = $search->getResults( $query );

		return view( 'pages.search', compact( [ 'results', 'query' ] ) );
	}
	
	public function notFound() {
		return view( 'pages.404' );
	}
}
