<?php

/**
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

Route::get('front', 'PageController@home');

Route::get('page', ['all-stories', 'uses'           => 'PageController@allStories']);
Route::get('page', ['footnotes-disclosures', 'uses' => 'PageController@footnotes']);
Route::get('page', 'PageController@index');

Route::get('singular', ['stories', 'uses' => 'StoryController@index']);
Route::get('singular', ['services', 'uses' => 'ServiceController@index']);
Route::get('singular', ['landing', 'uses' => 'LandingController@index']);

Route::get('search', 'PageController@search');
Route::get('404', 'PageController@notFound');