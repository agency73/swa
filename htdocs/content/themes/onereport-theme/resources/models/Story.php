<?php

namespace Theme\Models;

use function Couchbase\basicDecoderV1;
use Illuminate\Database\Eloquent\Model;
use Theme\Admin\Cookie;
use Theme\Admin\Session;
use Theme\Models\PostMeta;
use Themosis\Facades\DB;
use Themosis\Page\Option;
use \WP_Query;


/**
 * Class Story.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class Story extends Model {
	protected $table = 'posts';
	protected $primaryKey = 'ID';
	public $timestamps = FALSE;

	const SLUG = 'stories';
	const LABEL_SINGLE = 'Story';
	const LABEL_PLURAL = 'Stories';
	const TAXONOMY_SLUG = 'story-list';
	const LABEL_TAXONOMY_SINGLE = 'Tag';
	const LABEL_TAXONOMY_PLURAL = 'Tags';
	const LIMIT_ADDITIONAL_STORIES = 3;
	const PAGE_ALL_STORIES = '/all-stories';
	const TAXONOMY_TOPIC_SLUG = 'key_topic';
	const LIMIT_SDG_ICON = 5;

	/**
	 * Getting story posts
	 *
	 * @return mixed
	 */
	public static function getStories() {
		return Post::getPosts( [ 'stories' ] );
		
		/*$args = array( 'orderby' => 'ID', 'order' => 'DESC', 'post_type' => 'stories' );
		return $query = new WP_Query( $args );*/

	}
    static function storiesSort ($key) {
	    return function ($a, $b) use($key) {
            if ($a[0][$key] == $b[0][$key]) {
                return 0;
            }

            return($a[0][$key] < $b[0][$key]) ? -1 : 1;
        };
    }
    /**
     * Retrieve data for all stories
     *
     * @return array
     * @throws \Themosis\Page\OptionException
     */
    //public static function getAllStories()
    public function getAllStories()
    {
        $data = [];
        $result = [];

        $stories = Story::getStories();
		//echo "<pre>";      
        $stories_data = array();
        foreach ($stories as $key => $val) {        		
			$stories_data[$key]['ID'] = $val->ID;
			$stories_data[$key]['post_author'] = $val->post_author;
			$stories_data[$key]['post_date'] = $val->post_date;
			$stories_data[$key]['post_date_gmt'] = $val->post_date_gmt;
			$stories_data[$key]['post_content'] = $val->post_content;
			$stories_data[$key]['post_title'] = $val->post_title;
			$stories_data[$key]['post_excerpt'] = $val->post_excerpt;
			$stories_data[$key]['post_status'] = $val->post_status;
			$stories_data[$key]['comment_status'] = $val->comment_status;
			$stories_data[$key]['ping_status'] = $val->ping_status;
			$stories_data[$key]['post_password'] = $val->post_password;
			$stories_data[$key]['post_name'] = $val->post_name;
			$stories_data[$key]['to_ping'] = $val->to_ping;
			$stories_data[$key]['pinged'] = $val->pinged;
			$stories_data[$key]['post_modified'] = $val->post_modified;
			$stories_data[$key]['post_modified_gmt'] = $val->post_modified_gmt;
			$stories_data[$key]['post_content_filtered'] = $val->post_content_filtered;
			$stories_data[$key]['post_parent'] = $val->post_parent;
			$stories_data[$key]['guid'] = $val->guid;
			$stories_data[$key]['menu_order'] = $val->menu_order;
			$stories_data[$key]['post_type'] = $val->post_type;
			$stories_data[$key]['post_mime_type'] = $val->post_mime_type;
			$stories_data[$key]['comment_count'] = $val->comment_count;
        }
        //$stories = Story::getAdditionalStoriesData1( $stories_data );
        //print_r($stories_data); 
        //exit;
       
        
        $report_options = Option::get('stories');
        $options_values = is_array($report_options) ? array_values($report_options) : array();     

        foreach ($stories as $story) {
            $story = Story::find($story->ID);
            $story_data = $story->formatAllStoriesThumb();


            if (!empty($story_data)) {
                $data[$story_data['page_tag']]['page_color'] = $story_data['page_color'];
                $data[$story_data['page_tag']]['stories'][reset($story_data['topic_tag'])][] = $story_data;
            }
        }

        /*echo "<pre>";
        print_r($data);
        exit;*/

        foreach ($data as $tag => $value) {
            $key_topics = $value['stories'];

            uasort($key_topics, Story::storiesSort('order'));

            $data[$tag]['stories'] = $key_topics;
        }

        /* sort $result by option settings */
        if (!empty($options_values)) {
            foreach ($options_values as $value) {
                if (isset($data[$value])) {
                    $result[$value] = $data[$value];
                }
            }
        }
        //print_r($result);exit;
        return $result;
    }

    public function getAdditionalStoriesData1($stories_data)
    {

       	$results['stories'] = $stories_data;
        //$add_stories = meta('story_add_infinite', $this->ID);
        //$stories_ids = !empty($add_stories) ? array_filter(array_column($add_stories, 'story_add_infinite_block')) : [];
        

        // Fetch Stories Desending order wise......................
        //if (sizeof($stories_ids) < self::LIMIT_ADDITIONAL_STORIES) {
            $page_tags = Story::getAllPageTags();            
            $stories_page_tags = array_filter(array_column($results['stories'], 'page_tag_id'));
			
			echo "<pre>";print_r($page_tags);exit;

            foreach ($page_tags as $tag) {

                //if (!in_array($tag['value'], $stories_page_tags) && sizeof($results['stories']) < self::LIMIT_ADDITIONAL_STORIES) {
                    $story = PostMeta::select('post_id')
                        ->where('meta_key', 'related_page')
                        ->where('meta_value', $tag['value'])
                        ->orderBy('post_id', 'desc')
                        ->get();
                    //echo "<pre>"; print_r($story);exit;    
                    $id = $story[0]->post_id;                    
                    $_story = Story::find($id);

                    $image_id = meta('promo_image', $id);
                    $related_id = meta('related_page', $id);
                    $results['stories'][] = [
                        'title' => $_story->post_title,
                        'promo_image' => wp_get_attachment_url($image_id),                        
	                    'story_sdg_images' => $this->getsdgIcons( $_story->ID, 'story_sdg_images' ),
                        'page_tag_id' => $related_id,
                        'page_tag' => meta('page_tag', $related_id),
                        'page_color' => meta('page_color', $related_id),
                        'url' => get_permalink($id),
                        //'story_teaser_title' => meta('story_teaser_title', $_story->ID),
                        'story_hover_content' => meta('story_hover_content', $_story->ID),
                    ];
                //}
            }           
        //}

        return $results;
	}

	/**
	 * Format data for all stories block
	 * @return array
	 */
	public function formatAllStoriesThumb() {
		$related_id = (int) meta( 'related_page', $this->ID );

		$data = [
			'title'              => $this->post_title,
			'promo_image'        => wp_get_attachment_image_url( meta( 'promo_image', $this->ID ), 'themosis' ),
			'page_tag'           => meta( 'page_tag', $related_id ),
			'page_color'         => meta( 'page_color', $related_id ),
			'url'                => get_permalink( $this->ID ),
			'topic_tag'          => wp_get_post_terms($this->ID, self::TAXONOMY_TOPIC_SLUG, [ 'fields' => 'names']),
            'post_content'       => $this->post_content,
            'order'              => get_term_meta(wp_get_post_terms($this->ID, self::TAXONOMY_TOPIC_SLUG, [ 'fields' => 'ids'])[0],'order')[0],
		];

		return $data;
	}

	/**
	 * Formatting stories for displaying
	 *
	 * @return array
	 */
	public function formatStory() {
		$image_id   = meta( 'promo_image', $this->ID );
		$related_id = meta( 'related_page', $this->ID );

		$post_next = $this->next();
		$post_prev = $this->prev();

		$data = [
			'title'       => $this->post_title,
			'promo_image' => wp_get_attachment_url( $image_id ),
			'body'        => apply_filters( 'the_content', $this->post_content ),
			'page_tag'    => meta( 'page_tag', $related_id ),
			'page_color'  => meta( 'page_color', $related_id ),
			'story_intro' => meta( 'story_intro', $this->ID ),
			'story_sdg_title' => meta( 'story_sdg_title', $this->ID ),
			//'story_sdg_title_link' => meta( 'story_sdg_title_link', $this->ID ),
			'story_gris_title' => meta( 'story_gris_title', $this->ID ),
			//'story_gris_title_link' => meta( 'story_gris_title_link', $this->ID ),
			'story_sasbs_title' => meta( 'story_sasbs_title', $this->ID ),
			//'story_sasbs_title_link' => meta( 'story_sasbs_title_link', $this->ID ),
			'story_sdg_images' => self::getsdgIcons( $this->ID, 'story_sdg_images' ),
			'story_gris_data' => meta( 'story_gris', $this->ID ),
			'story_sasbs_data' => meta( 'story_sasbs', $this->ID ),
			'testimonial_secondary_text' => meta( 'testimonial_secondary_text', $this->ID ),
			'testimonial_secondary_author' => meta( 'testimonial_secondary_author', $this->ID ),
			'testimonial_secondary_image' => meta( 'testimonial_secondary_image', $this->ID ),
			'url'         => get_permalink( $this->ID ),
			'post_next'    => $post_next,
			'post_prev'    => $post_prev,
			'url_back'     => $this->back(),

		];
		//echo "<pre>";print_r($data);exit;
		//exclude ids for randomize additional items
		$exclude_ids[0] = $this->ID;
		$exclude_ids[1] = !empty( $post_next['id'] ) ? $post_next['id'] : false;
		$exclude_ids[2] = !empty( $post_prev['id'] ) ? $post_prev['id'] : false;
		$data['additional_stories'] = $this->getAdditionalStoriesData( $exclude_ids );

		return $data;
	}
	
	public function formatStoryThumb() {
		$data = [];
		$landing_id = Session::get('landing_id');
		$related_id = (int) meta( 'related_page', $this->ID );
		
		if($landing_id && $related_id !== $landing_id) {
			return $data;
		}

		$image_id   = meta( 'promo_image', $this->ID );
		$data = [
			'ID'              => $this->ID,
			'title'              => $this->post_title,
			'promo_image'        => wp_get_attachment_url( $image_id ),			
			'story_sdg_images'   => self::getsdgIcons( $this->ID, 'story_sdg_images' ),
			'page_tag'           => meta( 'page_tag', $related_id ),
			'page_color'         => meta( 'page_color', $related_id ),
			'story_teaser_title' => meta( 'story_teaser_title', $this->ID ),
			'story_hover_content' => meta( 'story_hover_content', $this->ID ),
			'url'                => get_permalink( $this->ID ),
			'post_date'          => get_the_date('U', $this->ID ),
			'topic_tag'          => wp_get_post_terms($this->ID, self::TAXONOMY_TOPIC_SLUG, [ 'fields' => 'names'])
		];
		//echo "<pre>";print_r($data);
		return $data;
	}


	/**
	 * Retrieve data for posts navigation
	 *
	 * @param $post
	 *
	 * @return array
	 */
	public static function getPostNavData( $post = null ) {

        if (!$post) return false;

		$related_id = (int) meta( 'related_page', $post->ID );

		$result = [
			'id'          => $post->ID,
			'title'       => $post->post_title,
			'promo_image' => wp_get_attachment_image_url( meta( 'promo_image', $post->ID ), 'themosis' ),
			'story_sdg_images' => self::getsdgIcons( $post->ID, 'story_sdg_images' ),
			/*'story_hover_image1' => wp_get_attachment_image_url( meta( 'story_hover_image1', $post->ID ), 'themosis' ),
			'story_hover_image2' => wp_get_attachment_image_url( meta( 'story_hover_image2', $post->ID ), 'themosis' ),
			'story_hover_image3' => wp_get_attachment_image_url( meta( 'story_hover_image3', $post->ID ), 'themosis' ),*/
			'page_tag'    => meta( 'page_tag', $related_id ),
			'page_color'  => meta( 'page_color', $related_id ),
			'url'         => get_permalink( $post->ID ),
		];

		return $result;
	}

	public function prev() {
		$operation = '<';
		$prev_post = $this->getSiblingPost( $operation );

		return self::getPostNavData( $prev_post );
	}
	
	public function next() {
		$operation = '>';
		$next_post = $this->getSiblingPost( $operation );

		return self::getPostNavData( $next_post );
	}

  public function getSiblingPost($operation) {
    $order           = ($operation == '<') ? 'desc' : 'asc';
    $landing_page_id = meta('related_page', $this->ID);

    $builder = $this->getStoryByLandingId($landing_page_id);

    $story = $builder->where('post_date', $operation, $this->post_date)
                     ->orderBy('post_date', $order)
                     ->first();
    if (empty($story->ID)) {
      // If no story make query from scratch and get first or last story.
      $builder = $this->getStoryByLandingId($landing_page_id);
      $story   = $builder->where('ID', '<>', $this->ID)
                         ->orderBy('post_date', $order)
                         ->first();
    }
    return $story;
  }

  public function getStoryByLandingId($landing_page_id) {
    $query = Story::where('post_type', '=', $this->post_type)
                  ->where('post_status', '=', $this->post_status);
    $query = $query->whereHas('meta', function ($meta_query) use ($landing_page_id) {
      $meta_query->where('meta_key', 'related_page')
                 ->where('meta_value', $landing_page_id);
    });

    return $query;
  }

	public function back() {
		$landing_id = Session::get( 'landing_id' );
		if ( $landing_id ) {
			$previos_page = $_SERVER['HTTP_REFERER'];
//			return get_permalink( $landing_id );
			return $previos_page;
		};

		return get_home_url();
	}
	
	public function landing() {
		return $this->hasOne( PostMeta::class, 'post_id' )
		            ->where( 'meta_key', 'related_page' );
	}

	public static function getAllPageTags() {
		return PostMeta::select('meta_value')
		               ->where( 'meta_key', 'related_page' )
		               ->groupBy('meta_value')->get()->toArray();
	}

    public function service() {
        return $this->hasOne( PostMeta::class, 'meta_value' )
            ->where( 'meta_key', 'stories-information' );
    }
	
	public function isHome() {
		return $this->hasOne( PostMeta::class, 'post_id' )
		            ->where( 'meta_key', 'story_home' );
	}

	/**
	 * Get all stories for field AdditionalStories in admin panel
	 * Excluded ids from post navigation and current post
	 *
	 * @return array
	 */
	public function getAdditionalStoriesForField() {

		$exclude_ids = [];
		$result = [];

		if( !empty ( $this->ID ) ) {

			$prev = $this->prev();
			$prev = isset($prev['id']) ? $prev['id'] : false;

			$next = $this->next();
			$next = isset($next['id']) ? $next['id'] : false;

			//exclude  ids from result array (nav posts and current)
			array_push( $exclude_ids, $this->ID, $prev, $next );

			$all_stories = Post::where( 'post_status', 'publish' )->where('post_type', self::SLUG)->get();

			foreach ( $all_stories as $key => $story ) {

				if( !in_array( $story->ID, $exclude_ids ) ) {

					$id = $story->ID;
					$_story = Story::find($id );
					$related_id = (int) meta( 'related_page', $id );

					if( !empty( $related_id ) ) {
						$page_tag = meta( 'page_tag', $related_id );
						array_push($result, [$id => $_story->post_title . ' - ' . $page_tag]);
					}
				}
			}

			//revers array to show new posts on top of result
			rsort( $result );

			array_unshift($result, [ '0' => 'None']);
		}

		return $result;
	}

	public function getStoriesForField() {
		$result = [];
		$all_stories = Post::where( 'post_status', 'publish' )->where('post_type', self::SLUG)->get();
		foreach ( $all_stories as $key => $story ) {
			$id = $story->ID;
			$_story = Story::find($id );
			$related_id = (int) meta( 'related_page', $id );

			if( !empty( $related_id ) ) {
				$page_tag = meta( 'page_tag', $related_id );
				array_push($result, [$id => $_story->post_title . ' - ' . $page_tag]);
			}
		}
		//revers array to show new posts on top of result
		rsort( $result );
		array_unshift($result, [ '0' => 'None']);
		return $result;
	}

	/**
	 * Retrieve additional stories data
	 *
	 * @param array $exclude_ids from result array
	 *
	 * @return array
	 */
    public function getAdditionalStoriesData($exclude_ids = [])
    {
        $results['stories'] = [];
        $add_stories = meta('story_add_infinite', $this->ID);
        $stories_ids = !empty($add_stories) ? array_filter(array_column($add_stories, 'story_add_infinite_block')) : [];

        $results['all_stories_link'] = self::PAGE_ALL_STORIES;

        if (!empty($stories_ids[0])) {
            foreach ($add_stories as $story) {
                $id = $story['story_add_infinite_block'];

                if (empty($id)) continue;

                $_story = Story::find($id);

                $image_id = meta('promo_image', $id);                
                $related_id = meta('related_page', $id);
                $results['stories'][] = [
                    'title' => $_story->post_title,
                    'promo_image' => wp_get_attachment_url($image_id),                    
                    'story_sdg_images' => self::getsdgIcons( $_story->ID, 'story_sdg_images' ),
                    'page_tag_id' => $related_id,
                    'page_tag' => meta('page_tag', $related_id),
                    'page_color' => meta('page_color', $related_id),
                    'url' => get_permalink($id),
                    'story_teaser_title' => meta('story_teaser_title', $_story->ID),
                    'story_hover_content' => meta('story_hover_content', $_story->ID),
                ];
            }            
        }

        if (sizeof($stories_ids) < self::LIMIT_ADDITIONAL_STORIES) {
            $page_tags = self::getAllPageTags();
            $stories_page_tags = array_filter(array_column($results['stories'], 'page_tag_id'));

            foreach ($page_tags as $tag) {

                if (!in_array($tag['value'], $stories_page_tags) && sizeof($results['stories']) < self::LIMIT_ADDITIONAL_STORIES) {
                    $story = PostMeta::select('post_id')
                        ->where('meta_key', 'related_page')
                        ->where('meta_value', $tag['value'])
                        ->whereNotIn('post_id', $exclude_ids)
                        ->get()->random(1);

                    $id = $story[0]->post_id;
                    $_story = Story::find($id);

                    $image_id = meta('promo_image', $id);                    
                    $related_id = meta('related_page', $id);
                    $results['stories'][] = [
                        'title' => $_story->post_title,
                        'promo_image' => wp_get_attachment_url($image_id),                        
                        'story_sdg_images' => self::getsdgIcons( $_story->ID, 'story_sdg_images' ),
                        'page_tag_id' => $related_id,
                        'page_tag' => meta('page_tag', $related_id),
                        'page_color' => meta('page_color', $related_id),
                        'url' => get_permalink($id),
                        'story_teaser_title' => meta('story_teaser_title', $_story->ID),
                        'story_hover_content' => meta('story_hover_content', $_story->ID),
                    ];
                }
            }
        }

        return $results;
	}

  public function meta(){
    return $this->hasMany(PostMeta::class, 'post_id');
  }

  	public function getsdgIcons( $post_id, $field_list) {
        $story_sdg_images = meta( $field_list, $post_id );

        if (is_array($story_sdg_images) || is_object($story_sdg_images))
		{
	       foreach($story_sdg_images as $index => $val) {        	            
	            $story_sdg_images[$index]['story_sdg_image'] = wp_get_attachment_url($val['story_sdg_image']);
	        }
        	return $story_sdg_images;
		}
    }

    public function getsdgIcons_other( $post_id, $field_list) {

        $story_sdg_images = meta( $field_list, $post_id );   
        if (is_array($story_sdg_images) || is_object($story_sdg_images))
		{
			foreach ($story_sdg_images as $key => $value) {							
				if(!empty($value['story_sdg_image'])){						
					$story_sdg_images[$index]['story_sdg_image'] = wp_get_attachment_url($value['story_sdg_image']);
				}else{
					return $story_sdg_images = [];
				}
			}
			return $story_sdg_images;	     
		}
    }

}