<?php

namespace Theme\Models\Helpers;

use Theme\Models\Post;
use Themosis\Page\Option;

class MetaTags {

	public static function generateMetaTags() {

		$index_meta = '';

		$query_obj = get_queried_object();

		if(empty($query_obj)) return false;

		$id        = $query_obj->ID;

		$title_fb   = meta( '_yoast_wpseo_opengraph-title', $id );
		$title_twit = meta( '_yoast_wpseo_twitter-title', $id );;
		$site_url = get_bloginfo( 'url' );

		$site_logo        = meta( '_yoast_wpseo_opengraph-image' );
		$site_logo_twit   = meta( '_yoast_wpseo_twitter-image' );
		$description_fb   = meta( '_yoast_wpseo_opengraph-description', $id );
		$description_twit = meta( '_yoast_wpseo_twitter-description', $id );

		if ( empty( $site_logo ) ) {
			$site_logo = wp_get_attachment_url( meta( 'promo_image' ) );
		}

		if ( empty( $site_logo_twit ) ) {
			$site_logo_twit = wp_get_attachment_url( meta( 'promo_image' ) );
		}

		if ( empty( $description_fb ) ) {
			$description_fb = meta( '_yoast_wpseo_metadesc', $id );
		}

		if ( empty( $description_twit ) ) {
			$description_twit = meta( '_yoast_wpseo_metadesc', $id );
		}

		if ( empty( $description_twit ) ) {
			$description_twit = meta( '_yoast_wpseo_metadesc', $id );
		}


		if ( empty( $title_fb ) ) {
//			$title_fb = $query_obj->post_title . ' | ' . get_bloginfo( 'title' );
			$title_fb = empty(meta('_yoast_wpseo_title', $id)) ? $query_obj->post_title . ' | ' . get_bloginfo( 'title' ) : meta('_yoast_wpseo_title', $id);
		}

		if ( empty( $title_twit ) ) {
//			$title_twit = $query_obj->post_title . ' | ' . get_bloginfo( 'title' );
			$title_fb = empty(meta('_yoast_wpseo_title', $id)) ? $query_obj->post_title . ' | ' . get_bloginfo( 'title' ) : meta('_yoast_wpseo_title', $id);
		}


		$post_url = get_permalink( $id );

		$twitter = Option::get( 'main', 'twitter' );

		/**
		 * Facebook.
		 */
		$index_meta .= '<meta property="og:title" content="' . $title_fb . '" />';
		$index_meta .= '<meta property="og:description" content="' . $description_fb . '" />';
		$index_meta .= '<meta property="og:type" content="website" />';
		$index_meta .= '<meta property="og:url" content="' . $post_url . '" />';
		$index_meta .= '<meta property="og:image" content="' . $site_logo . '" />';
		$index_meta .= '<meta property="fb:app_id" content="163659574449645" />';
		$index_meta .= '<meta property="og:image:width" content="400" />';
		$index_meta .= '<meta property="og:image:height" content="300" />';


		/**
		 * Twitter.
		 */

		$index_meta .= '<meta name="twitter:image" content="' . $site_logo_twit . '" />';
		$index_meta .= '<meta name="twitter:card" content="summary_large_image"/>';
		$index_meta .= '<meta name="twitter:title" content="' . $title_twit . '" />';
		$index_meta .= '<meta name="twitter:url" content="' . $post_url . '" />';
		$index_meta .= '<meta name="twitter:description" content="' . $description_twit . '" />';
		$index_meta .= '<meta name="twitter:site" content="' . $twitter . '" />';

		return $index_meta;
	}

}
