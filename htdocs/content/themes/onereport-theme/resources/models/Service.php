<?php

namespace Theme\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Service.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class Service extends Model {
	protected $table = 'posts';
	protected $primaryKey = 'ID';
	public $timestamps = FALSE;
	
	const SLUG = 'services';
	const LABEL_SINGLE = 'Service';
	const LABEL_PLURAL = 'Services';
	const TAXONOMY_SLUG = 'service-list';
	
	/**
	 * Creating additional field for the image url
	 *
	 * @return mixed|string
	 */
	public function addMediaUrl() {
		$info = meta( 'info', $this->ID );
		
		if ( ! empty( $info ) ) {
			foreach ( $info as $k => $box ) {
				$info[ $k ]['media_url'] = wp_get_attachment_url( $box['media'] );
			}
		}
		
		return $info;
	}
	
	/**
	 * Get related stories for displaying
	 *
	 * @return array
	 */
	public function getRelatedStories() {
		$stories   = [];
		$story_ids = meta( 'stories-information', $this->ID, 'post', FALSE );
		
		if ( ! empty( $story_ids ) ) {
			foreach ( $story_ids as $story_id ) {
				$story      = Story::find( $story_id );
				$story_data = $story->formatStoryThumb();
				
				if ( ! empty( $story_data ) ) {
					$stories[ $story_id ] = $story_data;
				}
			}
		}
		
		// Sorting by post publish date
		usort( $stories, function ( $a, $b ) {
			if ( empty( $a['post_date'] ) || empty( $b['post_date'] ) ) {
				return 0;
			}
			
			if ( $a['post_date'] == $b['post_date'] ) {
				return 0;
			}
			
			return ( $a['post_date'] < $b['post_date'] ) ? - 1 : 1;
		});
		
		return $stories;
	}
	
	/**
	 * Formatting service output
	 * @return array
	 */
	public function formatService() {
		$image_id      = meta( 'service_image', $this->ID );
		$left_icon_id  = meta( 'service_info_left_icon', $this->ID );
		$right_icon_id = meta( 'service_info_right_icon', $this->ID );
		$data = [
			'id'                       => $this->ID,
			'title'                    => $this->post_title,
			'subtitle'                 => meta( 'subtitle', $this->ID ),
			'body'                     => Post::filterContent( $this->post_content ),
			'image_url'                => wp_get_attachment_url( $image_id ),
			'columns'                  => Post::filterContent( meta( 'columns', $this->ID ) ),
			'info'                     => $this->addMediaUrl(),
			'stories'                  => $this->getRelatedStories(),
			'text_blocks'              => meta( 'text_info', $this->ID ),
			'service_text_cols'        => meta( 'service_text_cols', $this->ID ),
			'service_info_middle_text' => meta( 'service_info_middle_text', $this->ID ),
			'service_info_middle_note' => meta( 'service_info_middle_note', $this->ID ),
			'service_info_left_title'  => meta( 'service_info_left_title', $this->ID ),
			'service_info_left_text'   => meta( 'service_info_left_text', $this->ID ),
			'service_info_left_icon'   => wp_get_attachment_url( $left_icon_id ),
			'service_info_right_title' => meta( 'service_info_right_title', $this->ID ),
			'service_info_right_text'  => meta( 'service_info_right_text', $this->ID ),
			'service_info_right_icon'  => wp_get_attachment_url( $right_icon_id ),
		];
		
		return $data;
	}
}
