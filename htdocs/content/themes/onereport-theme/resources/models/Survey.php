<?php

namespace Theme\Models;

use Illuminate\Database\Eloquent\Model;
use Theme\Models\PostMeta;

/**
 * Class Survey.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class Survey extends Model {
	protected $table = 'posts';
	protected $primaryKey = 'ID';
	public $timestamps = FALSE;

	const SLUG = 'surveys';
	const LABEL_SINGLE = 'Survey';
	const LABEL_PLURAL = 'Surveys';

	/**
	 * Getting story posts
	 *
	 * @return mixed
	 */
	public static function getStories() {
		return Post::getPosts( [ 'surveys' ] );
	}

	/**
	 * Formatting surveys for displaying
	 *
	 * @return array
	 */
	public function formatStory() {
		$data = [];


		return $data;
	}
}