<?php

namespace Theme\Models;

use Illuminate\Database\Eloquent\Model;
use Theme\Models\PostMeta;
use Theme\Models\Story;

/**
 * Class Post.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class Post extends Model {
	const LANDING_SLUG = 'landing';
	const LANDING_LABEL_SINGLE = 'Landing page';
	const LANDING_LABEL_PLURAL = 'Landing pages';
	const RANDOM_STORIES = 5;
	const LIMIT_ADDITIONAL_STORIES = 3;
	const LIMIT_SDG_IMAGES = 7;

	const PAGE_ALL_STORIES = '/all-stories';

	protected $table = 'posts';
	protected $primaryKey = 'ID';
	public $timestamps = FALSE;
	
	/**
	 * Getting posts of appropriate type
	 *
	 * @param $type
	 * @param string $status
	 *
	 * @return mixed
	 */
	public static function getPosts( array $type = [], $status = 'publish' ) {
		$query = Post::where( 'post_status', $status )->orderby('post_date', 'DESC');
		$query->where( function ( $q ) use ( $type ) {
			foreach ( $type as $k => $t ) {
				$q->orWhere( 'post_type', $t );
			}
		} );
		
		return $query->get();
	}
	
	/**
	 * Getting post titles
	 *
	 * @param $types
	 *
	 * @return array
	 */
	public static function getTitles( $types ) {
		$posts = self::getPosts( $types );
		
		return self::filterTitles( $posts );
	}
	
	/**
	 * Get related services
	 *
	 * @return array
	 */
	public function getRelatedServices() {
		$services     = [];
		$service_list = meta( 'service_list', $this->ID, 'post', FALSE );
		if ( ! empty( $service_list ) ) {
			$service_ids = $service_list[0];
			if ( ! empty( $service_ids ) ) {
				foreach ( $service_ids as $service_id ) {
					$id = $service_id['service_item'];
					if ( $id ) {
						$service         = Service::find( $id );
						$services[ $id ] = $service->formatService();
					}
				}
			}
		}
		
		return $services;
	}
	
	public static function getCollectionField( $post_id, $collection, $field, $entity = 'post' ) {
		$data           = [];
		$collection_arr = meta( $collection, $post_id, $entity, FALSE );
		if ( ! empty( $collection_arr[0] ) ) {
			foreach ( $collection_arr[0] as $container ) {
				$data[] = $container[ $field ];
			}
		}
		
		return $data;
	}
	
	public function getPageFields() {
		$fields = [
			'archive_title',
			'archive_link_text',
			'archive_text',
			'archive_box',
			'gri_title',
		];
		
		return $fields;
	}
	
	public function getFieldsData( $fields ) {
		$data = [];
		$fields_content = $this->getContentFields();
		
		foreach ( $fields as $field ) {
			$data[ $field ] = in_array( $field, $fields_content )
				? apply_filters( 'the_content', meta( $field, $this->ID ) )
				: meta( $field, $this->ID );
		}
		
		return $data;
	}
	
	public function getContentFields() {
		$fields = [
			'landing_details_text',
			'landing_info_add_text',
			'view_box_text',
			'view_box_image_text',
			/*'view_box_image_text_top',
			'view_box_image_text_left',*/
			//'reporting_text',
			'awards_text',
			'archive_link_text',
			'archive_text',
		];
		
		return $fields;
	}
	
	// Gets url list from the infinite image field
	public function getImageUrls( $post_id, $field_list, $field ) {
		$images = meta( $field_list, $post_id );
		if( !empty( $images ) ){
			array_walk( $images, function ( &$image ) use ( $field ) {
				$image = wp_get_attachment_url( $image[ $field ] );
			} );
		}

		return $images;
	}
	
	// Gets url from the simple image field
	public function getImageUrl( $field ) {
		return wp_get_attachment_image_url( meta( $field, $this->ID ), 'original' );
	}
	
	public function getLandingPageFields() {
		$fields = [
			'landing_title',
			/*'landing_details_title',
			'landing_details_text',*/
			'landing_details_icon1',
			'landing_details_title1',
			'landing_details_subtitle1',
			'landing_details_icon2',
			'landing_details_title2',
			'landing_details_subtitle2',
			'landing_details_icon3',
			'landing_details_title3',
			'landing_details_subtitle3',
			'landing_one_report_pdf_btn_name',
			'landing_one_report_pdf',
			'landing_sasb_index_btn_name',
			'landing_sasb_index',
			'landing_gri_index_btn_name',
			'landing_gri_index',
			'landing_people_fact_sheet_btn_name',
			'landing_people_fact_sheet',
			'landing_datatable_info',
			'landing_tables_title',
			'landing_tables_anchor',
			'landing_tables_text',
			/*'landing_info_add_title',
			'landing_info_add_text',*/
			/*'landing_text_blocks_title',
			'landing_text_blocks_subtitle',
			'landing_text_blocks',
			*/
            'map_title',
            'map_subtitle',
            'map_text',
            'region_title',
            'region_subtitle',
            'top_rating_title',
            'top_rating_subtitle',
            'top_rating_list',
            'success_title',
            'success_sub_title_central',
            'success_desc_central',
            'success_sub_title_left',
            'success_desc_left',
            'success_sub_title_right',
            'success_desc_right',
		];
		
		return $fields;
	}
	
	/**
	 * Filter titles for option list
	 *
	 * @param array $posts
	 *
	 * @return array
	 */
	public static function filterTitles( $posts = [] ) {
		$options = [];
		if ( empty( $posts ) ) {
			return $options;
		}
		
		foreach ( $posts as $post ) {
			$options[ $post->ID ] = $post->post_title;
		}
		
		return $options;
	}
	
	/**
	 * Filter content for enabling shortcodes
	 *
	 * @param $data
	 *
	 * @return array|mixed
	 */
	public static function filterContent( $data ) {
		if ( is_array( $data ) ) {
			foreach ( $data as $k => $item ) {
				$data[ $k ]['text'] = apply_filters( 'the_content', $item['text'] );
			}
			
			return $data;
		}
		
		return apply_filters( 'the_content', $data );
	}
	
	/**
	 * Formatting the homepage for displaying
	 *
	 * @return array
	 */
    public function getSliderStories( $post_id, $field_list, $field ) {
        $stories = meta( $field_list, $post_id );

        foreach ($stories as $index => $story) {
            $stories[$index]['image'] = wp_get_attachment_image_url($story['image'], 'original');
            $stories[$index]['area_one_image'] = wp_get_attachment_image_url($story['area_one_image'], 'original');
            $stories[$index]['area_three_image'] = wp_get_attachment_image_url($story['area_three_image'], 'original');
        }

        return $stories;
    }
    public function formatHomePage()
    {
    	$exclude_ids[0] = $this->ID;
    	
        $data = [
            'title' => $this->post_title,
            'body' => apply_filters('the_content', $this->post_content),
//Promo image
            'promo_image' => wp_get_attachment_image_url(meta('promo_image', $this->ID), 'original'),
            'promo_title' => meta('promo_title', $this->ID),
            'promo_subtitle' => meta('promo_subtitle', $this->ID),
            'promo_text_title' => meta('promo_text_title', $this->ID),
            'promo_text' => apply_filters('the_content', meta('promo_text', $this->ID)),
            'promo_url' => meta('promo_url', $this->ID),
            'promo_linkable_text' => meta('promo_linkable_text', $this->ID),
//Home description
            'home_image' => wp_get_attachment_image_url(meta('home_image', $this->ID), 'original'),
            'home_title' => meta('home_title', $this->ID),
            'home_subtitle' => meta('home_subtitle', $this->ID),
            'home_author' => meta('home_author', $this->ID),
//Video block
            'video_url' => meta('video_url', $this->ID),
//View box
            'view_box_title' => meta('view_box_title', $this->ID),
            'view_box_subtitle' => meta('view_box_subtitle', $this->ID),
            'view_box_text' => meta('view_box_text', $this->ID),
            'view_box_image' => wp_get_attachment_image_url(meta('view_box_image', $this->ID), 'original'),
            /*'view_box_image_text_top' => meta('view_box_image_text_top', $this->ID),
            'view_box_image_text_left ' => meta('view_box_image_text_left ', $this->ID),*/
//Slider section
            'slider_image' => wp_get_attachment_image_url(meta('slider_image', $this->ID), 'original'),
            'slider' => $this->getSliderStories( $this->ID, 'slider', 'story' ),

            //'slider_background_image1' => wp_get_attachment_image_url(meta('slider_background_image1', $this->ID), 'original'),
            'slider_icon1' => wp_get_attachment_image_url(meta('slider_icon1', $this->ID), 'original'),
            'slider_title1' => meta('slider_title1', $this->ID),
            'slider_subtitle1' => meta('slider_subtitle1', $this->ID),
            //'slider_background_image2' => wp_get_attachment_image_url(meta('slider_background_image2', $this->ID), 'original'),
            'slider_icon2' => wp_get_attachment_image_url(meta('slider_icon2', $this->ID), 'original'),
            'slider_title2' => meta('slider_title2', $this->ID),
            'slider_subtitle2' => meta('slider_subtitle2', $this->ID),
            //'slider_background_image3' => wp_get_attachment_image_url(meta('slider_background_image3', $this->ID), 'original'),
            'slider_icon3' => wp_get_attachment_image_url(meta('slider_icon3', $this->ID), 'original'),
            'slider_title3' => meta('slider_title3', $this->ID),
            'slider_subtitle3' => meta('slider_subtitle3', $this->ID),

            'slider_button_text' => meta('slider_button_text', $this->ID),
            
//View Global Goals section
            'goals_image' => wp_get_attachment_image_url(meta('goals_image', $this->ID), 'original'),
            'goals_title' => meta('goals_title', $this->ID),
            'goals_subtitle' => meta('goals_subtitle', $this->ID),
            'goals_button_url' => meta('goals_button_url', $this->ID),
            'goals_button_text' => meta('goals_button_text', $this->ID),
//View stories section
            'stories_title' => meta('stories_title', $this->ID),
            'stories_subtitle' => meta('stories_subtitle', $this->ID),
            //'stories' => $this->getRelatedStories(),
            'additional_stories' => $this->getAdditionalStoriesData($exclude_ids),
            'stories_button_url' => meta('stories_button_url', $this->ID),
            'stories_button_text' => meta('stories_button_text', $this->ID),           
//Reporting Approach
            /*'reporting_title' => meta('reporting_title', $this->ID),
            'reporting_subtitle' => meta('reporting_subtitle', $this->ID),
            'reporting_text' => meta('reporting_text', $this->ID),
            'reporting_image'   => $this->getImageUrl( 'reporting_image' ),*/
//Awards section
            'awards_title' => meta('awards_title', $this->ID),
            'awards_subtitle' => meta('awards_subtitle', $this->ID),
            'awards_text' => meta('awards_text', $this->ID),
            'awards_images'     => $this->getImageUrls( $this->ID, 'awards_images', 'image' ),
        ];

        $home_sticky_story_id = meta('stories', $this->ID);
        $home_sticky_story = !empty($home_sticky_story_id) ? Story::find($home_sticky_story_id) : '';

        if (!empty($home_sticky_story)) {
            $home_sticky_story_data = $home_sticky_story->formatStoryThumb();
            if (!empty($home_sticky_story_data)) {
                $data['stories'][] = $home_sticky_story_data;
            }
        }


        $stories = Story::getStories();

        if($stories->isEmpty()) return array();

        $rand_stories = $stories->where('ID', '!=', $home_sticky_story_id)->random(self::RANDOM_STORIES);

        foreach ($rand_stories as $story) {
            $story = Story::find($story->ID);
            $story_data = $story->formatStoryThumb();
            if (!empty($story_data)) {
                $data['stories'][] = $story_data;
            }
        }
        return array_map(array($this, 'applyShortcode'), $data);
    }
	
	/**
	 * Formatting the page for displaying
	 *
	 * @return array
	 */
	public function formatPage() {
		$fields = $this->getPageFields();
		$data = [
			'title'             => $this->post_title,
			'body'              => apply_filters( 'the_content', $this->post_content ),
			'gri_text'          => apply_filters( 'the_content', meta( 'gri_text', $this->ID ) ),
			'image'             => $this->getImageUrl( 'promo_image' ),
			'view_box_image'    => $this->getImageUrl( 'view_box_image' ),
			'graph_list'        => meta( 'graph_list', $this->ID ),
			'services'          => $this->getRelatedServices(),

            //2019 at a Glance
            'glance_title'        => meta( 'glance_title', $this->ID ),
            'glance_text'        => meta( 'glance_text', $this->ID ),

            //Our Approach Section
            'our_approach_title' => meta( 'our_approach_title', $this->ID ),
            'our_approach_detail' => meta( 'our_approach_detail', $this->ID ),
            /*'our_approach_image'  => $this->getImageUrl( 'our_approach_image' ),
            'our_approach_doc'  => meta('our_approach_doc',$this->ID),
            'our_approach_table_content' => meta( 'our_approach_table_content', $this->ID ),*/

            // How We Support the Global Goals
            'hw_goals_title' => meta( 'hw_goals_title', $this->ID ),
            'hw_goals_description' => meta( 'hw_goals_description', $this->ID ),
            'hw_goals_icons_data' => meta( 'hw_goals_icons_data', $this->ID ),                        

            //Tabs
            'tabs' => $this->getSliderStories( $this->ID, 'tabs', 'tab' ),
            'tabs_button_text' => meta('tabs_button_text', $this->ID),
		];
		/*echo "<pre>";
		print_r($data);
		exit;*/
		$data = array_merge( $this->getFieldsData( $fields ), $data );
		
		return array_map( array( $this, 'applyShortcode' ), $data );
	}
	
	public function formatLandingPage() {
		$fields = $this->getLandingPageFields();		
		$data = [
			'title'                     => $this->post_title,
			'page_name'                 => $this->post_name,
			'body'                      => $this->post_content,
			'landing_title_button'		=> meta( 'landing_title_button', $this->ID ),
			'landing_image'             => $this->getImageUrl( 'promo_image' ),
			'landing_mobile_image'      => $this->getImageUrl( 'landing_mobile_image' ),
			'landing_details_icon'      => $this->getImageUrl( 'landing_details_icon' ),
			//'landing_info_add_icon'     => $this->getImageUrl( 'landing_info_add_icon' ),
			'landing_text_blocks_image' => $this->getImageUrl( 'landing_text_blocks_image' ),
			'services'                  => $this->getRelatedServices(),

            //Map
            'map_image'                 => $this->getImageUrl( 'map_image' ),
            'map_background'            => $this->getImageUrl( 'map_background' ),

            //Capacity by Region
            'region_data'               => $this->getRegionDataBlock(),

            //Sharing our Success
            'success_center'            =>  $this->getImageUrl( 'success_central_image' ),
            'success_left'              =>  $this->getImageUrl( 'success_left_image' ),
            'success_right'             =>  $this->getImageUrl( 'success_right_image' ),

            //Story testimonial
            /*'story_testimonial_position'=> meta( 'story_testimonial_position', $this->ID ),
            'story_testimonial_text'    => meta( 'story_testimonial_text', $this->ID ),
            'story_testimonial_author'  => meta( 'story_testimonial_author', $this->ID ),
            'story_testimonial_image'   => meta( 'story_testimonial_image', $this->ID ),*/

            //Statistic information
            /*'landing_info_select'       => meta( 'landing_info_select', $this->ID ),
            'landing_info_video'        => meta( 'landing_info_video', $this->ID ),
            'landing_info_image'        =>  $this->getImageUrl( 'landing_info_image' ),
            'landing_info_top'          => meta( 'landing_info_top', $this->ID ),
            'landing_info_middle'       => meta( 'landing_info_middle', $this->ID ),
            'landing_info_bottom'       => meta( 'landing_info_bottom', $this->ID ),*/
        ];
		
		$data = array_merge( $this->getFieldsData( $fields ), $data );		
		return array_map( array( $this, 'applyShortcode' ), $data );
	}

	/**
	 * Formatting the All Stories Landing Page for displaying data
	 * @return array
	 */
	public function formatAllStoriesLanding() {
		$data = [
			'title'    => $this->post_title,
			'body'     => apply_filters( 'the_content', $this->post_content ),
			'stories'  => Story::getAllStories()
		];

		return $data;
	}
	
	/**
	 * Apply shortcode for every field in the content
	 *
	 * @param $content
	 *
	 * @return array|string
	 */
	public function applyShortcode( $content ) {
		if ( is_array( $content ) ) {
			return array_map( array( $this, 'applyShortcode' ), $content );
		}
		
		return do_shortcode( $content );
	}
	
	/**
	 * Building select options from titles
	 *
	 * @param $types
	 *
	 * @return array
	 */
	
	public static function formatSelectTitles( $types ) {
		$select_titles    = self::getTitles( $types );
		$select_titles[0] = '- None -';
		ksort( $select_titles );
		
		return $select_titles;
	}

	/**
	 * Retrieve data for region block for Pages
	 *
	 * @return array
	 */
	public function getRegionDataBlock() {
		$result = [];
		$type =  meta( 'region_toggle_type', $this->ID );

		if( !empty( $type ) ) {
			if( $type == 'Image' ) {
				$result[$type] = $this->getImageUrl( 'region_data_image' );
			}
			if( $type == 'Chart' ) {
				$result[$type] = meta( 'region_data_chart', $this->ID );
			}
		}

		return $result;
	}

	function getGraphListBlock() {
		$result = [];

		return $result;
	}

	/**
	 * Retrieve additional stories data
	 *
	 * @param array $exclude_ids from result array
	 *
	 * @return array
	 */
    public function getAdditionalStoriesData($exclude_ids = [])
    {

       	$results['stories'] = [];
        $add_stories = meta('story_add_infinite', $this->ID);
        $stories_ids = !empty($add_stories) ? array_filter(array_column($add_stories, 'story_add_infinite_block')) : [];
        //$results['all_stories_link'] = self::PAGE_ALL_STORIES; 
       
        // Fetch stories based on Stories selection from admin side dropdown      
        if (!empty($stories_ids[0])) {
            foreach ($add_stories as $story) {
                $id = $story['story_add_infinite_block'];

                if (empty($id)) continue;

                $_story = Story::find($id);
                
                $image_id = meta('promo_image', $id);                
                $related_id = meta('related_page', $id);
                $results['stories'][] = [
                    'title' => $_story->post_title,
                    'promo_image' => wp_get_attachment_url($image_id),                    
                    'story_sdg_images' => $this->getsdgIcons( $_story->ID, 'story_sdg_images' ),
                    'page_tag_id' => $related_id,
                    'page_tag' => meta('page_tag', $related_id),
                    'page_color' => meta('page_color', $related_id),
                    'url' => get_permalink($id),
                    //'story_teaser_title' => meta('story_teaser_title', $_story->ID),
                    'story_hover_content' => meta('story_hover_content', $_story->ID),
                ];
            }
        }
        	/*echo "<pre>";
            print_r($results['stories']);
            exit;*/
        // Fetch Stories Desending order wise......................
        if (sizeof($stories_ids) < self::LIMIT_ADDITIONAL_STORIES) {
            $page_tags = Story::getAllPageTags();            
            $stories_page_tags = array_filter(array_column($results['stories'], 'page_tag_id'));

            foreach ($page_tags as $tag) {

                if (!in_array($tag['value'], $stories_page_tags) && sizeof($results['stories']) < self::LIMIT_ADDITIONAL_STORIES) {
                    $story = PostMeta::select('post_id')
                        ->where('meta_key', 'related_page')
                        ->where('meta_value', $tag['value'])
                        ->orderBy('post_id', 'desc')
                        ->get();
                    //echo "<pre>"; print_r($story);exit;    
                    $id = $story[0]->post_id;                    
                    $_story = Story::find($id);

                    $image_id = meta('promo_image', $id);
                    $related_id = meta('related_page', $id);
                    $results['stories'][] = [
                        'title' => $_story->post_title,
                        'promo_image' => wp_get_attachment_url($image_id),                        
	                    'story_sdg_images' => $this->getsdgIcons( $_story->ID, 'story_sdg_images' ),
                        'page_tag_id' => $related_id,
                        'page_tag' => meta('page_tag', $related_id),
                        'page_color' => meta('page_color', $related_id),
                        'url' => get_permalink($id),
                        //'story_teaser_title' => meta('story_teaser_title', $_story->ID),
                        'story_hover_content' => meta('story_hover_content', $_story->ID),
                    ];
                }
            }           
        }

        return $results;
	}

	public function getsdgIcons( $post_id, $field_list) {
        $story_sdg_images = meta( $field_list, $post_id );
        if (is_array($story_sdg_images) || is_object($story_sdg_images))
		{
	       foreach($story_sdg_images as $index => $val) {        	            
	            $story_sdg_images[$index]['story_sdg_image'] = wp_get_attachment_url($val['story_sdg_image']);
	        }
        	return $story_sdg_images;
		}
    }
}