<?php

namespace Theme\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Accordion.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class Accordion extends Model {
	protected $table = 'posts';
	protected $primaryKey = 'ID';
	public $timestamps = FALSE;
	
	const SLUG = 'accordions';
	const LABEL_SINGLE = 'Accordion';
	const LABEL_PLURAL = 'Accordions';
	
	public function formatAccordion() {
		$slides = meta( 'slides', $this->ID );
		$data   = [
			'id' => $this->ID,
			'title'  => do_shortcode($this->post_title),
			'slides' => $slides,
			'image'  => $this->getImageUrl( 'image' ),
            'doc'  => meta('doc',$this->ID),
            'table_content' => meta( 'table_content', $this->ID ),
		];
		/*echo "<pre>";
		print_r($data);*/
		return $data;
	}

	public function getImageUrl( $field ) {
		return wp_get_attachment_image_url( meta( $field, $this->ID ), 'original' );
	}
}
