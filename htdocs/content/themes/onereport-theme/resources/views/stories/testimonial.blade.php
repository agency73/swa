@if($story_testimonial_text || $story_testimonial_author)
    <div class="inner{{ ($story_testimonial_position == 'vertical') ? ' inner-half' : ''}} animate-element opacity-animate">
        @php
            $noimage = (!$story_testimonial_image) ? 'noimage' : '';
        @endphp
        <div class="quote {{ $noimage }} animate-element opacity-animate">
            <div class="inner-quote ">
                @if($story_testimonial_text)
                    <q>{!! $story_testimonial_text !!}</q>
                @endif
                @if($story_testimonial_author)
                    <div class="sing animate-element opacity-animate">{!! $story_testimonial_author !!}</div>
                @endif
            </div>
            @if($story_testimonial_image)
                <img src="{{ wp_get_attachment_url($story_testimonial_image, 'original') }}" alt="">
            @endif
        </div>
    </div>
@endif