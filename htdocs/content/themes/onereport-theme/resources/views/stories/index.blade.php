<div class="story animate-element opacity-animate story-page">
    <div class="promo-img animate-element opacity-animate">
        <img src="{{ $promo_image }}" alt="{{ $title }}">
        <a href="{{ $url_back }}" class="main-button animate-element opacity-animate">Close this story</a>
    </div>
    <div class="container animate-element opacity-animate story-page__content">
        <!-- Show the story fields -->  
        <?php //print_r($story_sdg_images);  exit; ?>     
        @if(
            (!empty($story_sdg_images) && $story_sdg_images[1]['story_sdg_image'] != '') || 
            (!empty($story_gris_data) && $story_gris_data[1]['story_gri'] != '') || 
            (!empty($story_sasbs_data) && $story_sasbs_data[1]['story_sasb'] != '') 
        )
         <div class="page-tag-wrap_top">
            <div class="sdg_images_div">
                @if(!empty($story_sdg_images) && $story_sdg_images[1]['story_sdg_image'] != '')   
                    <!-- <h5>UN SDG Alignment <a href="/about-the-one-report/">(Learn more)</a></h5> -->
                    <h5>{!! $story_sdg_title !!}</a></h5>                                        
                    <div class="sdg_images">
                        @foreach($story_sdg_images as $k => $val)  
                        <img src="{{ $val['story_sdg_image'] }}" alt="{{ $k }}" class="shareable-image" width="50px">
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="gri_div">               
                @if(!empty($story_gris_data) && $story_gris_data[1]['story_gri'] != '')
                <ul>
                <h5>{!! $story_gris_title !!}</h5>  
                @foreach($story_gris_data as $k => $val)
                    <li>{!! $val['story_gri'] !!}</li>                    
                @endforeach                
                </ul>   
                @endif            
            </div>
            <div class="sasb_div"> 
                @if(!empty($story_sasbs_data) && $story_sasbs_data[1]['story_sasb'] != '')              
                <ul>
                <h5>{!! $story_sasbs_title !!}</h5>
                @foreach($story_sasbs_data as $k => $val)
                    <li>{{ $val['story_sasb'] }}</li>
                @endforeach
                </ul>
                @endif
            </div> 
        </div>
       @endif
        <div class="page-tag-wrap"><span class="page-tag" style="background-color: {{ $page_color }};">{!! $page_tag !!}</span></div>
        <h1 class="title">{!!  $title  !!}</h1>

        <!-- Social links -->
        <div class="addtoany_share_save_container animate-element opacity-animate">
            @php
                echo do_shortcode('[addthis tool="addthis_inline_share_toolbox"]');
            @endphp
        </div>

        <p class="intro animate-element opacity-animate">{!! do_shortcode($story_intro) !!}</p>
        <div class="story-text animate-element opacity-animate">{!! $body !!}</div>

        <!-- Testimonial secondary -->
        @if(!empty($testimonial_secondary_text || $testimonial_secondary_author))
            <div class="inner testimonial-secondary animate-element opacity-animate animate">
                <div class="quote  animate-element opacity-animate animate">
                    <div class="inner-quote ">
                        @if(!empty($testimonial_secondary_text))
                            <q>{!! $testimonial_secondary_text !!}</q>
                        @endif
                        @if(!empty($testimonial_secondary_author))
                            <div class="sing animate-element opacity-animate">{!! $testimonial_secondary_author !!}</div>
                        @endif
                    </div>
                    @if(!empty($testimonial_secondary_image))
                        <img src="{{ wp_get_attachment_url($testimonial_secondary_image, 'original') }}"
                             alt="{{$testimonial_secondary_author}}">
                    @endif
                </div>
            </div>
        @endif

        <!-- Social links -->
        <div class="addtoany_share_save_container">
            @php
                echo do_shortcode('[addthis tool="addthis_inline_share_toolbox"]');
            @endphp
        </div>


    </div>
    {{-- If the urls are equal to the current it means that it is only one story in a queue --}}
    {{-- In such case there is no need for showing it --}}
    @if( ($post_prev || $post_next) && !($post_prev['url'] === $url && $post_next['url'] === $url)  )
        <div class="story-nav animate-element animate">
            @if($post_prev)
                <div class="wrapper-related prev-story animate-element opacity-animate">
                    <a class="main-button prev" href="{{ $post_prev['url'] }}">Previous story</a>
                    <div class="related-info prev-info">
                        <div class="related-text related-text-prev">
                             <span class="page-tag"
                                   style="color: {{ $post_prev['page_color'] }};">{{ $post_prev['page_tag'] }}</span>
                            <span>{{ $post_prev['title'] }}</span>
                        </div>
                        <div class="related-image animate-element animate" style="background-image: url('{{ $post_prev['promo_image']  }}');"></div>
                    </div>
                </div>
            @endif
            @if($post_next)
                <div class="wrapper-related next-story animate-element animate">
                    <a class="main-button next" href="{{ $post_next['url'] }}">Next story</a>
                    <div class="related-info next-info">
                        <div class="related-text related-text-next">
                            <span class="page-tag"
                                  style="color: {{ $post_next['page_color'] }};">{{ $post_next['page_tag'] }}</span>
                            <span>{{ $post_next['title'] }}</span>
                        </div>
                        <div class="related-image animate-element animate" style="background-image: url('{{ $post_next['promo_image']  }}');"></div>
                    </div>
                </div>
            @endif
        </div>
    @endif
</div>
@include('stories.addstories')