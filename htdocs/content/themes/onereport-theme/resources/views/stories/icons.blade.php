<div class="icons animate-element opacity-animate">
    @if(!empty($story_icons_title))
        {!! $story_icons_title !!}
    @endif
    @if(!empty($iconstat))
        <div class="icon shareable-image animate-element opacity-animate">
            <img src="{{ wp_get_attachment_url($iconstat['image']) }}" alt="">
            @if(!empty($iconstat['title']))
                <h4 class="story-img-title">{!! do_shortcode($iconstat['title']) !!}</h4>
            @endif
            @if(!empty($iconstat['description']))
                <p class="story-img-desc">{!! do_shortcode($iconstat['description']) !!}</p>
            @endif
        </div>
    @endif
</div>