<a class="service-info-inner animate-element opacity-animate" href="{{ $story['url'] }}" style="background-image: url('{{ $story['promo_image'] }}')">
    <div class="page-tag animate-element opacity-animate" style="background-color: {{ $story['page_color'] }}">{!! $story['page_tag'] !!}</div>
    <span class="title animate-element opacity-animate story_hover_icon">
       
        @if(!empty($story['story_sdg_images']))                                           
        @foreach($story['story_sdg_images'] as $k => $val)  
            <?php if($val['story_sdg_image'] != ''){ ?>
            @if($k <= 3)                                           
            <img src="{{ $val['story_sdg_image'] }}" alt="{{ $k }}" class="shareable-image">
            @endif   
            <?php } ?>                                            
        @endforeach
        @endif
    </span>
    <div class="title animate-element opacity-animate"><?php echo implode(' ', array_slice(explode(' ', $story['story_hover_content']), 0, 30)); ?></div>
    <h3>{!! $story['title'] !!}</h3>
</a>
