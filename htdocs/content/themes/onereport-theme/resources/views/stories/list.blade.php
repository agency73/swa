@if(!empty($stories))
    <div class="service-info-block @if(count($stories) == 3) third-block @elseif(count($stories) == 2) two-block @elseif(count($stories) <= 1) solo-block @endif">
        <div class="container service-info animate-element opacity-animate">
            @foreach($stories as $story)
                @if($loop->index && !($loop->index%2))
                    </div><div class="container service-info">
                @endif
                <div class="column service-tablet-{{$loop->remaining}}-{{$loop->index%2}}">
                    @include('stories.teaser')
                </div>
            @endforeach
        </div>
    </div>
@endif