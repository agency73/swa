@if($imagetext)
    <div class="inner {{ ($imagetext["story_content_image_position"] == 'half') ? ' inner-half' : ''}}  animate-element opacity-animate">
        <div class="illustration-holder">
            <img src="{{ wp_get_attachment_url($imagetext['story_content_image']) }}" alt="" class="shareable-image">
            @if($imagetext['story_content_image_text'])
                <div class="badge" style="top: {{ $imagetext['story_content_image_text_top'] }}; left: {{ $imagetext['story_content_image_text_left'] }}">
                    <div>
                        {!! $imagetext['story_content_image_text'] !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif