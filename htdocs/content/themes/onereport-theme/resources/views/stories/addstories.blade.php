@if(!empty($additional_stories['stories']))
    <div class="view-stories-section">
        <h2 class="title animate-element opacity-animate">Explore Additional Stories</h2>
    <div class="view-stories-sub-section">
    <div class="wrap-service-info stories">
        
        <div class="service-info">
            <div class="container">                
                @foreach($additional_stories['stories'] as $story)                               
                <div class="column animate-element opacity-animate">
                    <a class="service-info-inner" href="{{ $story['url'] }}" style="background-image: url('{{ $story['promo_image'] }}')">
                        <span class="page-tag" style="background-color: {{ $story['page_color'] }};">{{ $story['page_tag'] }}</span>
                        <h3>{{ $story['title'] }}</h3>
                    </a>
                    <div class="column-hover">
                    <span class="title image-object animate-element opacity-animate story_hover_icon">    
                        <a href="{{ $story['url'] }}">
                            @if(!empty($story['story_sdg_images']))                                           
                            @foreach($story['story_sdg_images'] as $k => $val)  
                                <?php if($val['story_sdg_image'] != ''){ ?>
                                @if($k <= 3)                                           
                                <img src="{{ $val['story_sdg_image'] }}" alt="{{ $k }}" class="shareable-image">
                                @endif 
                                <?php } ?>                                              
                            @endforeach
                            @endif
                        </a>
                        </span>
                        <div class="title image-title animate-element opacity-animate">
                            <a href="{{ $story['url'] }}"><?php echo implode(' ', array_slice(explode(' ', $story['story_hover_content']), 0, 30)); ?>
                            </a>
                            </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        <div class="wrap-button">
            <a class="main-button animate-element opacity-animate" href="{{ $additional_stories['all_stories_link'] }}">Explore All Stories</a>
        </div>
    </div>
    </div>
    </div>
@endif