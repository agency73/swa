@if(!empty($stories))
<div class="view-stories-sub-section">
    <div class="stories">
        <div class="service-info">
            <?php 
            $width = '';
            if(count($stories) == 2){
                $width = "width:50%";
            } ?>
            <div class="container service-info-{{count($stories)}}">
                @foreach($stories as $story)
                @if($loop->index <= 2)
                <a class="service-info-inner1" href="{{ $story['url'] }}" style="background-image: url('{{ $story['promo_image'] }}')">
                    <div class="column animate-element opacity-animate service-tablet-{{$loop->remaining}}-{{$loop->index%2}}"
                        data-bottom-position="70" style="<?php echo $width; ?>">
                        @include('stories.teaser-landing')
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif