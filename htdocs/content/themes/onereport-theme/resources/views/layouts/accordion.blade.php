<div class="tabs">
    <!-- Hide accordian title on 'Our Approach' Section in 'About the one report' Page  -->    
    <div class="tabs-title">
      {!! $title !!}
    </div>    
    @if(!empty($slides))
        <div class="tabs-content">
            @foreach($slides as $slide)
                <div class="tab">                                    
                <div class="tab-button">
                    {!! do_shortcode($slide['title']) !!}
                </div>                
                <div class="tab-hide">
                   <p> {!! do_shortcode($slide['text']) !!}</p>
                    <div class="oa_moredetails our-approach-moredetails" id="oa_moredetails">
                        <?php if($slide['doc'] != '' || $slide['image'] != '') { ?>
                        <div class="oa_details our-approach-image">
                            <a <?php if(do_shortcode($slide['doc']) != ''){ ?> href="{{ wp_get_attachment_url(do_shortcode($slide['doc'])) }}" download <?php } ?> target="_blank" > <img src="{!! wp_get_attachment_url($slide['image']) !!}"> </a>
                        </div>
                    <?php } if($slide['table_content'] != ''){ ?>
                        <div class="oa_details our-approach-table">{!! do_shortcode($slide['table_content']) !!}</div>
                    <?php } ?>
                    </div>
                </div>
                </div>
            @endforeach
        </div>
    @endif
</div>