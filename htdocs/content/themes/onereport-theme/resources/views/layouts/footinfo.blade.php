<div class="footer-wrapper">
    @include('layouts.surveyform')

    <div class="foot-info">
        <div class="container">
            <div class="footer-top">
                <div class="wrap-logo">
                    <a class="logo" href="{!! !empty($footer['link_to']) ? $footer['link_to'] : '#' !!}" target="_blank">
                        @if($footer['footer_logo'])
                            <img src="{{ wp_get_attachment_image_url($footer['footer_logo'], 'original') }}" class="style-svg">
                        @endif
                        {!! $footer['title'] !!}
                    </a>
                    {{--{!! $footer['contact'] !!}--}}
                </div>
                @menu( ["theme_location" => "footer-nav", "menu_class" => ""] )
            </div>
            <div class="copyright">
                {!! $footer['copyright'] !!}
            </div>
        </div>
    </div>
</div>