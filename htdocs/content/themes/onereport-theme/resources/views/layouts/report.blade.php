<div class="report animate-element opacity-animate" id="section-report" style="background: #ffbf27">
    <div class="container">
        <h2 class="title animate-element opacity-animate">Download the One Report</h2>
        <div class="report-blocks">
            <div class="report-block animate-element opacity-animate">
                <div class="preview">
                    <div class="report-title">Read the Report</div>
                    <img src="{{ $report['cover_image'] }}" alt="">
                </div>
                <ul class="links-block">
                    <li><a class="main-button download-icon" href="{{ $report['report_full'] }}" target="_blank" download >Download One Report</a></li>
                    <li><a class="main-button download-icon" href="{{ $report['report_form'] }}" target="_blank" download >Download Form-10K</a></li>
                </ul>
            </div>
            <div class="report-block report-check animate-element opacity-animate">
                <div class="report-title">Create Your Own Report</div>
                <ol>
                    <li>Select the options that interest you.</li>
                    <li>Click “Create and Download My Report”</li>
                </ol>
                <form class="report-form" target="_blank">
                    <div class="list-checkbox">
                        @foreach($report['report_parts'] as $k => $part)
                            <div class="custom-checkbox">
                                <input id="ch{{$k}}" type="checkbox" value="{{$part['report_part_file']}}" />
                                <label for="ch{{$k}}"><span>{!! $part['report_part_title'] !!}</span></label>
                            </div>
                        @endforeach
                        <p class="notice" style="display: none;"></p>
                    </div>
                    <div class="list-button-section">
                        <button class="main-button" id="checkall">Select All</button>
                        <button class="main-button" id="clearall">Clear All</button>
                        <button class="main-button btn-generate download-icon download-icon-2 inactive" disabled>Create and Download My Report</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>