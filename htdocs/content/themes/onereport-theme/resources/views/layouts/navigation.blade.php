
<!-- Fixed navbar .navbar-fixed-top-->
<header>
    <div class="wrap-navigation">
        <div class="container">
            <a class="logo" href="/">
                @if($header['theme_logo'])
                    <img src="{{ wp_get_attachment_image_url($header['theme_logo'], 'original') }}" class="style-svg">
                @endif
                {!! $header['title'] !!}
            </a>
            <div class="gamburger">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
            <nav class="navbar navbar-collapse">
                    @menu( ["theme_location" => "header-nav", "menu_class" => "nav navbar-nav"] )

                    @include('search.search-form')
            </nav><!--/.nav-collapse -->
        </div>
    </div>
</header>