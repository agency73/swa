<div class="footnotes" id="footnotes">
    <h1>{!! $footnotes['title'] !!}</h1>
    <div class="footnotes-content">
        <div class="container container-content">
            @foreach($footnotes['columns'] as $key => $column)
                <div class="column" id="footnote-{!! $key !!}">
                    {!! do_shortcode($column['text']) !!}
                </div>
            @endforeach
        </div>
    </div>
</div>