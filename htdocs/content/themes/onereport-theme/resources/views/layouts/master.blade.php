@include('layouts.header')
@include('layouts.navigation')

<div>
    @yield('main')
</div>
@include('layouts.footer')