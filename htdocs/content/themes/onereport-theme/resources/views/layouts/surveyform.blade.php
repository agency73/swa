 <section class="survey-popup-wrap">
    <div class="survey-popup">
        <a class="close popup-button" type="button"></a>
        @if( !empty( $survey['survey_logo'] ) )
            <div class="logo">
                <img src="{{ wp_get_attachment_image_url($survey['survey_logo'], 'original') }}">
            </div>
        @endif
        <h1 class="title">
            Pssst.</br>
            Got a second to give us some feedback?
        </h1>
        <p> Help us make the One Report even better.</p>
        <div class="wrap-button">
            <a class="popup-button popup-link open-survey-form" type="button">Count me in</a>
        </div>
    </div>
</section>
<section class="form-leave-wrap">
    <div class="survey-popup">
        @if( !empty( $survey['survey_logo'] ) )
            <div class="logo">
                <img src="{{ wp_get_attachment_image_url($survey['survey_logo'], 'original') }}">
            </div>
        @endif
        <h1 class="title">
            Are you sure you want<br/> to leave this page?
        </h1>
        <div class="wrap-button">
            <a class="popup-button popup-link not-leave" type="button">No, don't leave</a>
            <a class="popup-button popup-link leave" type="button">Yes, see you later</a>
        </div>
    </div>
</section>
<div class="survey-block">
    <p>
        <span>Pssst. Got a second to give us some feedback?</span>Help us make the One Report even better.
        <button class='open-survey-form' type="submit">Count me in</button>
    </p>
    <div id="survey-form" class="magnific-form mfp-hide">
        <button title="Close (Esc)" type="button" class="mfp-close-custom">×</button>
        <div role="form">
            <div class="screen-reader-response"></div>

            @if( !empty( $survey['survey_form_bg'] ) )
                <div class="hold-image">
                    <img src="{{ wp_get_attachment_image_url($survey['survey_form_bg'], 'original') }}">
                </div>
            @endif
            <form method="post" id="form-survey-from" class="form">
                @if( !empty( $survey['survey_title'] ) )
                    <h1>{{ $survey['survey_title'] }}</h1>
                @endif
                @if( !empty( $survey['survey_subtitle'] ) )
                <h3>{{ $survey['survey_subtitle'] }}</h3>
                @endif
                <hr>
                {{--<h4>1. How would you identify yourself (select all the apply)</h4>--}}
                    @if( !empty( $survey['survey_first_question'] ) )
                        <h4>1. {{ $survey['survey_first_question'] }}</h4>
                    @endif
                <p>
                    <span class="form-control-wrap identify-yourself">
                        <span class="form-control radio identify-yourself">
                            <span class="list-item first">
                                <label>
                                    <input type="checkbox" name="sf-identify-yourself" value="Customer">
                                    <span class="list-item-label">Customer</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-identify-yourself" value="Employee">
                                    <span class="list-item-label">Employee</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-identify-yourself" value="Investor">
                                    <span class="list-item-label">Investor</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-identify-yourself"
                                           value="Non-Governmental Organization">
                                    <span class="list-item-label">Non-Governmental Organization</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-identify-yourself" value="Student">
                                    <span class="list-item-label">Student</span>
                                </label>
                            </span>
                            <span class="list-item last">
                                <label>
                                    <input class="other-radio" type="checkbox" name="sf-identify-yourself"
                                           value="Other (please fill in below)">
                                    <span class="list-item-label">Other (please fill in below)</span>
                                </label>
                            </span>
                        </span>
                    </span>
                    <span class="form-control-wrap identify-yourself-other">
                        <input type="text" name="sf-identify-yourself-other" value="" size="40"
                               class="form-control text other" aria-invalid="false">
                    </span>
                </p>
                {{--<h4>2. Which of our priority topics are most interested in (check all that apply)</h4>--}}
                    @if( !empty( $survey['survey_second_question'] ) )
                        <h4>2. {{ $survey['survey_second_question'] }}</h4>
                    @endif
                <p>
                    <span class="form-control-wrap topics-interested">
                        <span class="form-control radio identify-yourself">
                            <span class="list-item first">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Customers">
                                    <span class="list-item-label">Customers</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Employees">
                                    <span class="list-item-label">Employees</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Communities">
                                    <span class="list-item-label">Communities</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Energy">
                                    <span class="list-item-label">Energy</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Greenhouse Gas (GHG) Emissions">
                                    <span class="list-item-label">Greenhouse Gas (GHG) Emissions</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Waste">
                                    <span class="list-item-label">Waste</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested"  value="Economic Performance">
                                    <span class="list-item-label">Economic Performance</span>
                                </label>
                            </span>
                            <span class="list-item">
                                <label>
                                    <input type="checkbox" name="sf-topics-interested" value="Our Network">
                                    <span class="list-item-label">Our Network</span>
                                </label>
                            </span>
                            <span class="list-item last">
                                <label>
                                    <input class="other-radio" type="checkbox" name="sf-topics-interested" value="Other (please fill on below)">
                                    <span class="list-item-label">Other (please fill in below)</span>
                                </label>
                            </span>
                        </span>
                    </span>
                    <span class="form-control-wrap topics-interested-other">
                        <input type="text" name="sf-topics-interested-other" value="" size="40"
                               class="form-control text other" aria-invalid="false">
                    </span>
                </p>
                {{--<h4>3. Please rate your experience in the following areas (scale 1-5)</h4>--}}
                    @if( !empty( $survey['survey_third_question'] ) )
                        <h4>3. {{ $survey['survey_third_question'] }}</h4>
                    @endif
                <div class="form-inline-radio">
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            Overall visual appeal/design aesthetics of the site
                        </div>
                        <p>
                            <span class="form-control-wrap rate-1-overall-site">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio"  name="sf-rate-1-overall-site" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-overall-site" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-overall-site" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-overall-site" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-1-overall-site" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            Photos
                        </div>
                        <p><span class="form-control-wrap rate-1-photos">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-1-photos" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-photos" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-photos" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-photos" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-1-photos" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            Videos
                        </div>
                        <p>
                            <span class="form-control-wrap rate-1-videos">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-1-videos" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-videos" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-videos" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-videos" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-1-videos" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            Infographics and charts
                        </div>
                        <p>
                            <span class="form-control-wrap rate-1-infographics-charts">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-1-infographics-charts" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-infographics-charts" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-infographics-charts" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-1-infographics-charts" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-1-infographics-charts" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                </div>
                {{--<h4>4. Please rate your experience in the following areas (scale 1-5)</h4>--}}
                    @if( !empty( $survey['survey_fourth_question'] ) )
                        <h4>4. {{ $survey['survey_fourth_question'] }}</h4>
                    @endif
                <div class="form-inline-radio">
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            The stories were relevant
                        </div>
                        <p>
                            <span class="form-control-wrap rate-2-stories-relevant">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-relevant" value="1 Poor" >
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-relevant" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-relevant" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-relevant" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-relevant" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p></div>
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            The stories were compelling and engaging
                        </div>
                        <p>
                            <span class="form-control-wrap rate-2-stories-compelling">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-compelling" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-compelling" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-compelling" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-compelling" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-compelling" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            The stories were about topics I care about
                        </div>
                        <p>
                            <span class="form-control-wrap rate-3-stories-topics">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-topics" value="1 Poor" >
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-topics" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-topics" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-topics" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-2-stories-topics" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                </div>
                {{--<h4>5. Please rate your experience in the following areas (scale 1-5)</h4>--}}
                    @if( !empty( $survey['survey_fifth_question'] ) )
                        <h4>5. {{ $survey['survey_fifth_question'] }}</h4>
                    @endif
                <div class="form-inline-radio">
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            The information I was looking for was covered in the report
                        </div>
                        <p>
                            <span class="form-control-wrap rate-3-info-report">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-report" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-report" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-report" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label><input type="radio" name="sf-rate-3-info-report" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-report" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                    <div class="form-inline-row">
                        <div class="form-inline-cell">
                            The information I was looking for was easily accessible
                        </div>
                        <p>
                            <span class="form-control-wrap rate-2-info-easy-find">
                                <span class="form-control radio identify-yourself">
                                    <span class="list-item first">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-easy-find" value="1 Poor">
                                            <span class="list-item-label">1<br/>Poor</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-easy-find" value="2">
                                            <span class="list-item-label">2</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-easy-find" value="3">
                                            <span class="list-item-label">3</span>
                                        </label>
                                    </span>
                                    <span class="list-item">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-easy-find" value="4">
                                            <span class="list-item-label">4</span>
                                        </label>
                                    </span>
                                    <span class="list-item last">
                                        <label>
                                            <input type="radio" name="sf-rate-3-info-easy-find" value="5 Excellent">
                                            <span class="list-item-label">5<br/>Excellent</span>
                                        </label>
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                </div>
                <p><label> </label></p>
                {{--<h4>Do you have any other thoughts/comments you'd like to share?</h4>--}}
                    @if( !empty( $survey['survey_comments_advise'] ) )
                        <h4>{{ $survey['survey_comments_advise'] }}</h4>
                    @endif
                <p>
                    <span class="form-control-wrap your-message">
                        <textarea name="sf-your-message" cols="40" rows="10" class="form-control textarea" aria-invalid="false"></textarea>
                    </span>
                </p>
                <p>
                    <span class="loader"></span>
                    <input type="submit" value="Submit >" class="form-control submit"/>
                </p>
                <div class="error"></div>
                <div class="response-output"></div>
            </form>
            <div class="success-message"></div>
        </div>
    </div>
</div>