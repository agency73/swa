@extends('layouts.master')

@section('main')
    <div class="search-list">
        <div class="container">
            <h1 class="title">Search results</h1>
            @if($query)
                <div class="query">{!! $query !!}</div>
            @endif
            @if(!empty($results))
                @foreach($results as $result)
                    @include('search.index')
                @endforeach
            @else
                @include('search.empty')
            @endif
        </div>
    </div>
@endsection