@extends('layouts.master')

@section('main')
<?php $upload_dir = wp_get_upload_dir(); ?>
    <div class="wrap-all-page-block">
        @if(!empty($promo_image))
            <div class="all-page-block">
                <div class="intro">
                    <div class="bg-image" style="background-image: url('{{ $promo_image }}')"></div>
                    <div class="container-large">
                        <div class="container">
                            <h1 class="title">
                                {!! $promo_subtitle !!}
                                <strong>
                                    {!! $promo_title !!}
                                </strong>
                            </h1>
                        </div>
                        @if(!empty($promo_linkable_text))
                            <div class="intro-text">
                                <a target="_blank" href="{{$promo_url}}">{!! $promo_linkable_text !!}</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="description">
                    <div class="container container-content">
                        <div class="home_image"><img src="{{ $home_image }}" alt="" class="shareable-image1"></div>
                        <h1 class="title">{!! $home_title !!}</h1>
                        <h3 class="subtitle">{!! $home_subtitle !!}</h3>
                        <span>{{ $home_author }}</span>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="video-block video-block-home" id="section-message">
        <!-- <div class="container"> -->                       
            <div class="video-wrap animate-element opacity-animate">
                <a href="#model_video" rel="modal:open">                    
                    <!-- <img src="<?php echo $upload_dir['url']; ?>/play-button.png" class="play_icon"> -->
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-button.png"  class="play_icon"/>
                    <!--  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-button.png"> -->
                </a>               
                <img src="{{ $view_box_image }}" alt="Airplane wing" class="shareable-image">
                <div id="model_video" class="modal home-video-popup">
                  {!! $video_url !!}
                  <!-- <a href="#" rel="modal:close">Close</a> -->
                </div>                          
            </div>
       <!--  </div> -->
    </div>  
    <div class="service footview-service" id="section-footview">
        <!--div class="illustration-holder animate-element opacity-animate">
            <img src="{{ $view_box_image }}" alt="Airplane wing" class="shareable-image">  
            @if(!empty($view_box_image_text))
                <div class="badge animate-element opacity-animate" style="top: {{ $view_box_image_text_top }}; left: {{ $view_box_image_text_left }}">
                    <div>
                        {!! $view_box_image_text !!}
                    </div>
                </div>
            @endif
        </div-->
        <div class="container container-content animate-element opacity-animate">
            <h2 class="title">{!! $view_box_title !!}</h2>
            <div class="sub-title">{!! $view_box_subtitle !!}</div>
            <div class="sub-desc">
                {!! $view_box_text !!}
            </div>
        </div>
    </div>
   
    <div class="slider-section sliderSectionHomePage">
        <div class="slider">            
        @if(!empty($slider))    
        @foreach($slider as $story)      
        <?php $img_atts = wp_get_attachment_image_src( $story['slider_image'], 'full'); ?>  
            <div class="background_image_div" style="width:100%;background-image: url('<?php echo $img_atts[0]; ?>');">       
                <div class="slider-item">
                    <div class="slidertoptitle">
                        <h2>{!! $story['slider_title'] !!}</h2>
                    <!-- <h3>{!! $story['story_title'] !!}</h3> -->
                    <p class="slider-text">{!! $story['text'] !!}</p>
                    <!-- <div class="background-element"></div> -->
                    </div>
                    <div class="content_devider">
                        <ul class="image-list">
                            <li class="animate-element opacity-animate animate">
                                <?php $img_atts1 = wp_get_attachment_image_src( $story['slider_icon1'], 'full'); ?>
                                <div class="image-block">
                                    <img src="<?php echo $img_atts1[0]; ?>" alt="" class="shareable-image">
                                </div> 
                                <div class="image-content">
                                    <p class="story_sliderTitle">{!! $story['slider_title1'] !!}</p>
                                    <p class="story_sliderSubtitle">{!! $story['slider_subtitle1'] !!}</p>
                                </div>
                            </li>
                            <li class="animate-element opacity-animate animate">
                                <?php $img_atts2 = wp_get_attachment_image_src($story['slider_icon2'], 'full'); ?>
                                <div class="image-block">
                                    <img src="<?php echo $img_atts2[0]; ?>" alt="" class="shareable-image"> 
                                </div>
                                <div class="image-content">
                                    <p class="story_sliderTitle">{!! $story['slider_title2'] !!}</p>
                                    <p class="story_sliderSubtitle">{!! $story['slider_subtitle2'] !!}</p>
                                </div>
                            </li>
                            <li class="animate-element opacity-animate animate">
                                <?php $img_atts3 = wp_get_attachment_image_src($story['slider_icon3'], 'full'); ?>
                                <div class="image-block">
                                    <img src="<?php echo $img_atts3[0]; ?>" alt="" class="shareable-image">
                                </div> 
                                <div class="image-content">
                                    <p class="story_sliderTitle">{!! $story['slider_title3'] !!}</p>
                                    <p class="story_sliderSubtitle">{!! $story['slider_subtitle3'] !!}</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="wrap-button">
                        <a class="third-button animate-element opacity-animate animate" href="{{ $story['url'] }}">
                            <!-- {!! $slider_button_text !!} -->
                            {!! $story['slider_button_text'] !!}
                        </a>
                    </div>
                </div>
            </div><!-- /.background_image_div -->
        @endforeach;
        @endif;
        </div>
    </div>
   
    <div class="global_golas_section container container-content animate-element opacity-animate animate">
        <h2 class="title">{!! $goals_title !!}</h2>
        <div class="description">
             <div class="video-wrap">
                <img src="{{ $goals_image }}" alt="" class="shareable-image" width="80px">                
            </div>
            <div class="sub-title">{!! $goals_subtitle !!}</div>
        </div>
        <div class="wrap-button">
            <a class="third-button animate-element opacity-animate animate" href="{{ $goals_button_url }}">
                {!! $goals_button_text !!}
            </a>
        </div>
    </div>    
    @if (!empty($stories_button_text))
        <div class="view-stories-section">
            <div class="container container-content">
                <h2 class="title">{!! $stories_title !!}</h2>
                <div class="description">
                    <div class="sub-title">
                        {!! $stories_subtitle !!}
                    </div>
                </div>
            </div>
            <div class="view-stories-sub-section">
              <!--   @include('stories.addstories') -->
              <?php
             /* echo "<pre>";
              print_r($additional_stories['stories']);
              exit;*/
              ?>
              @if(!empty($additional_stories['stories']))
                <div class="wrap-service-info stories">                   
                    <div class="service-info">
                        <div class="container">
                            @foreach($additional_stories['stories'] as $story)                               
                                <div class="column animate-element opacity-animate">
                                    <a class="service-info-inner" href="{{ $story['url'] }}" style="background-image: url('{{ $story['promo_image'] }}')">
                                        <span class="page-tag" style="background-color: {{ $story['page_color'] }};">{{ $story['page_tag'] }}</span>
                                        <h3>{{ $story['title'] }}</h3>
                                    </a>
                                    <div class="column-hover">
                                    <a href="{{ $story['url'] }}">
                                        <span class="title image-object animate-element opacity-animate story_hover_icon">    
                                        
                                            @if(!empty($story['story_sdg_images']))                                           
                                            @foreach($story['story_sdg_images'] as $k => $val)  
                                                <?php if($val['story_sdg_image'] != ''){ ?>
                                                @if($k <= 3)                                           
                                                <img src="{{ $val['story_sdg_image'] }}" alt="{{ $k }}" class="shareable-image">
                                                @endif 
                                                <?php } ?>                                              
                                            @endforeach
                                            @endif
                                        </span>
                                        <div class="title image-title animate-element opacity-animate">
                                            <?php echo implode(' ', array_slice(explode(' ', $story['story_hover_content']), 0, 30)); ?>

                                            </div>
                                            </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>        
                </div>
            @endif
            </div>            

            <div class="wrap-button">
                <a class="third-button animate-element opacity-animate animate" href="{{ $stories_button_url }}">
                    {!! $stories_button_text !!}
                </a>
            </div>
        </div>
    @endif
    
    <!-- 
    <div class="reporting" id="section-reporting">
        <img class="title-image animate-element opacity-animate shareable-image" src="{{ $reporting_image }}" alt="">
        <div class="container container-content animate-element opacity-animate">
            <h2 class="title">{!! $reporting_title !!}</h2>
            <div class="sub-title">{!! $reporting_subtitle !!}</div>
            {!! $reporting_text !!}
        </div>
    </div>
     -->
    <div class="service wrap-image-list section-awards" id="section-awards">
        <div class="container">
            <h2 class="title animate-element opacity-animate">{!! $awards_title !!}</h2>
            <div class="sub-title animate-element opacity-animate">{!! $awards_subtitle !!}</div>
            {!! $awards_text !!}
        </div>
        @if(!empty($awards_images))
            <ul class="image-list">
                @foreach($awards_images as $awards_image)
                    <li class="animate-element opacity-animate">
                        <img src="{{ $awards_image }}" alt="" class="shareable-image">
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
@endsection