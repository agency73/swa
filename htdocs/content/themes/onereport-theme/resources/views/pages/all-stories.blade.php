@extends('layouts.master')

@section('main')
    <div class="all-stories top-block">
        <div class="container">
            <h1 class="title animate-element opacity-animate">{!! $title !!}</h1>
            <div class="content animate-element opacity-animate">{!! $body !!}</div>
            <?php //echo "<pre>"; print_r($stories);exit; ?>
            @if( !empty($stories) )
                <div class="all-stories-block">
                    @foreach( $stories as $tag => $page_tag )
                        <div class='all-stories-column'>
                            <p style="background-color: {!! $page_tag['page_color'] !!}"
                               class="story-column-name animate-element opacity-animate">
                               {!! $tag !!}
                               <span class="karat"></span>
                            </p>
                            <div class="stories-mobile">
                                @foreach( $page_tag['stories'] as $topic => $topic_tag )
                                    <div class="animate-element opacity-animate" data-bottom-position="80">
                                        <span class="story-key-topic">Key Topic: {!! $topic !!}</span>
                                        @foreach( array_reverse($topic_tag) as $story )
                                            <a href="{!! $story['url'] !!}" class="all-stories-item">
                                                <img src="{!! $story['promo_image'] !!}" alt="{!! $story['title'] !!}">
                                                <div class="wrap">
                                                    <p>{!! $story['title'] !!}</p>
                                                    <span>Learn More</span>
                                                </div>
                                                <i> &rsaquo;</i>
                                            </a>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection