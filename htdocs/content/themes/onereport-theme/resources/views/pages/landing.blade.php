@extends('layouts.master')

@section('main')
<div class="people-landing">
    @if($landing_image || $landing_mobile_image || $title || $body)
    <div class="top-block landing_page">
        @if($landing_image)
        <div class="bg-image" style="background-image: url({{ $landing_image }})"></div>
        @endif
        @if($landing_mobile_image)
        <div class="bg-image mobile-image">
            <img src="{{ $landing_mobile_image }}" alt="mobile-image" />
        </div>
        @endif
        @if($title || $body)
        <div class="container container-content">
            <h2 class="title animate-element opacity-animate">{!! do_shortcode($title) !!}</h2>
            <div class="content animate-element opacity-animate">{!! do_shortcode($body) !!}</div>
            <div class="button-block">
                <p class="button-text">{!! $landing_title_button !!}</p>
                @if(!empty($services))
                @foreach($services as $k => $service)
                <?php $title_link = '/'.$page_name.'/#'.str_replace(' ', '', $service['title']); ?>
                <div class="wrap-button">
                    <a class="third-button animate-element opacity-animate animate" href="<?php echo $title_link; ?>">
                        {!! $service['title'] !!}
                    </a>
                </div>
                @endforeach
                @endif
            </div>
        </div>
        @endif
    </div>
    @endif
    <!-- @if($landing_title || $landing_info_image || $landing_info_top || $landing_info_middle || $landing_info_bottom || $landing_info_add_title || $landing_info_add_text) -->
    <div class="landing-page key-accomplish" id="{!! sanitize_html_class( $landing_title ) !!}">
        @if($landing_title)
        <h2 class="title animate-element opacity-animate">{!! $landing_title !!}</h2>
        @endif
        <!-- @if($landing_details_title || $landing_details_icon || $landing_details_text)
                <div class="custom-info-block animate-element opacity-animate @if($landing_details_icon) with-image @endif">
                    @if($landing_details_title || $landing_details_icon)
                        <div class="number">
                            <span>{!! $landing_details_title !!}</span>
                            @if($landing_details_icon)
                                <img src="{{ $landing_details_icon }}" alt="" class="shareable-image">
                            @endif
                        </div>
                    @endif
                    @if($landing_details_text)
                        <div class="text">
                            {!! $landing_details_text !!}
                        </div>
                    @endif
                </div>
            @endif -->

        <div class="content_devider">
            <ul class="image-list slider-landing">
                <li class="animate-element opacity-animate animate">
                    <?php $img_atts1 = wp_get_attachment_image_src( $landing_details_icon1, 'full'); ?>
                    <div class="image-block">
                        <img src="<?php echo $img_atts1[0]; ?>" alt="" class="shareable-image">
                    </div>
                    <div class="image-content">
                        <p class="story_sliderTitle">{!! $landing_details_title1 !!}</p>
                        <p class="story_sliderSubtitle">{!! $landing_details_subtitle1 !!}</p>
                    </div>
                    <!-- <div class="div_devider">
                            <b>{!! $landing_details_title1 !!}</b><br>
                            {!! $landing_details_subtitle1 !!}
                        </div> -->
                </li>
                <li class="animate-element opacity-animate animate">
                    <?php $img_atts2 = wp_get_attachment_image_src($landing_details_icon2, 'full'); ?>
                    <div class="image-block">
                        <img src="<?php echo $img_atts2[0]; ?>" alt="" class="shareable-image">
                    </div>
                    <div class="image-content">
                        <p class="story_sliderTitle">{!! $landing_details_title2 !!}</p>
                        <p class="story_sliderSubtitle">{!! $landing_details_subtitle2 !!}</p>
                    </div>
                    <!-- <div class="div_devider">
                        <b>{!! $landing_details_title2 !!}</b><br>
                        {!! $landing_details_subtitle2 !!}
                    </div> -->
                </li>
                <li class="animate-element opacity-animate animate">
                    <?php $img_atts3 = wp_get_attachment_image_src($landing_details_icon3, 'full'); ?>
                    <div class="image-block">
                        <img src="<?php echo $img_atts3[0]; ?>" alt="" class="shareable-image">
                    </div>
                    <div class="image-content">
                        <p class="story_sliderTitle">{!! $landing_details_title3 !!}</p>
                        <p class="story_sliderSubtitle">{!! $landing_details_subtitle3 !!}</p>
                    </div>
                    <!-- <b>{!! $landing_details_title3 !!}</b><br>
                    {!! $landing_details_subtitle3 !!} -->
                </li>
            </ul>
        </div>
        <!-- 
            @if($landing_info_image || $landing_info_top || $landing_info_middle || $landing_info_bottom ||$landing_info_video )
                <div class="square-info animate-element opacity-animate">
                    @if($landing_info_select == 0)
                        <div class="square-info-wrap">
                            <div class="wrap-img">
                                <img src="{!! $landing_info_image !!}" alt="{!! $landing_info['top'] !!}" class="image shareable-image">
                            </div>
                            <div class="info">
                                <div class="info-inner animate-element" data-bottom-position='50'>
                                    <span>{!! $landing_info_top !!}</span>
                                    <span class="number">{!! $landing_info_middle !!}</span>
                                    <span>{!! $landing_info_bottom !!}</span>
                                </div>
                            </div>
                        </div>
                    @elseif($landing_info_select == 1)
                        <div class="video-block">
                            <div class="container">
                                <div class="video-wrap animate-element opacity-animate">
                                    {!! $landing_info_video !!}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endif

            @if($landing_info_add_title || $landing_info_add_text || $landing_info_add_icon)
                <div class="custom-info-block animate-element opacity-animate @if($landing_info_add_icon) with-image @endif">
                    <div class="number">
                        <span>{!! $landing_info_add_title !!}</span>
                        @if($landing_info_add_icon)
                            <img src="{{ $landing_info_add_icon }}" alt="" class="shareable-image">
                        @endif
                    </div>
                    <div class="text">
                        {!! $landing_info_add_text !!}
                    </div>
                </div>
            @endif
             -->
    </div>
    <!-- @endif -->
    <!-- 
    @if($story_testimonial_text)
        <div class="service">
            <div class="container container-content testimonial-block">
                @include('stories.testimonial')
            </div>
        </div>
    @endif
     -->
    @if(!empty($map_title))
    <div class="system-map animate-element opacity-animate" id="section-map"
        style="background-image: url({{ $map_background }})">
        <div class="container animate-element opacity-animate">
            <h2 class="title animate-element opacity-animate">{!! $map_title !!}</h2>
            <div class="date animate-element opacity-animate">{!! $map_subtitle !!}</div>
            {!! $map_text !!}
            <img class="map animate-element opacity-animate shareable-image" src="{{ $map_image }}" />
            <h2 class="title blue-text animate-element opacity-animate">{!! $region_title !!}</h2>
            <div class="date blue-text animat-element opacity-animate">{!! $region_subtitle !!}</div>

            {{-- Start region data block --}}
            @if( !empty( $region_data['Image'] ) )
            <div class="circle">
                <img class="map shareable-image" src="{{ $region_data['Image'] }}" />
            </div>
            @endif
            @if( !empty( $region_data['Chart'] ) )
            <div class="circle-wrap">
                <ul class="linear-charts">
                    @foreach( $region_data['Chart'] as $chart )
                    <li>
                        <span class="name-chart">{{ $chart['region_data_chart_name'] }}</span>
                        <span class="chart-cell">
                            <span class="linear-chart animate-element" data-bottom-position="80"
                                data-percent="{{ $chart['region_data_chart_data'] }}">
                                <span class="inner" style="background: {{ $chart['region_data_chart_color'] }}">
                                    <span class="number">{{ $chart['region_data_chart_data'] }}%</span>
                                </span>
                            </span>
                        </span>
                    </li>
                    @endforeach

                </ul>
            </div>
            @endif
            {{-- End region data block --}}
        </div>
    </div>
    @endif
    @if(!empty($top_rating_title))
    <div class="service list" id="section-rating">
        <div class="container container-content">
            <h2 class="title animate-element opacity-animate">{!! $top_rating_title !!}</h2>
            <div class="date animate-element opacity-animate">
                {!! $top_rating_subtitle !!}
            </div>
            @if(!empty($top_rating_list))
            <ol class="animate-element opacity-animate">
                @foreach($top_rating_list as $rating_item)
                <li><span><span class="code">{!! $rating_item['code'] !!}</span><span class="city">{!!
                            $rating_item['city'] !!}</span></span></li>
                @endforeach
            </ol>
            @endif
        </div>
    </div>
    @endif
    @if(!empty($success_title))
    <div class="service section-success" id="section-success" style="background: #eaeef1">
        <div class="container">
            <h2 class="title animate-element opacity-animate">{!! $success_title !!}</h2>
            <div class="succses-info">
                <div class="success-aside left animate-element opacity-animate">
                    <div class="images">
                        <img src="{!! $success_left !!}" alt="" class="animate-element">
                    </div>
                    <h4>{!! $success_sub_title_left !!}</h4>
                    <p>{!! $success_desc_left !!}</p>
                </div>
                <div class="success-center center animate-element opacity-animate">
                    <div class="center-image">
                        <img src="{!! $success_center !!}" alt="" class="animate-element">
                        <h4 class="center-title">{!! $success_sub_title_central !!}</h4>
                    </div>
                    <p>{!! $success_desc_central !!}</p>
                </div>
                <div class="success-aside right animate-element opacity-animate">
                    <div class="images">
                        <img src="{!! $success_right !!}" alt="" class="animate-element">
                    </div>
                    <h4>{!! $success_sub_title_right !!}</h4>
                    <p>{!! $success_desc_right !!}</p>
                </div>


                {{--@foreach($graph_list as $k => $graph)
                <div class="custom-circle-graph {{ $k%2 ? '' : 'graph-left' }}">
                <div class="inner-circle-graph">
                    <div class="circle-title" style="border-color: {{ $graph['graph_list_value_color'] }}">
                        {{ $graph['graph_list_title'] }}
                        <span class="percent">{{ $graph['graph_list_value'] }}%</span>
                    </div>
                    <span class="wrap-circle-graph" data-bottom-position="60">
                        <span class="circle-graph" data-percent="{{ $graph['graph_list_value'] }}"
                            data-peity='{ "fill": ["{{ $graph['graph_list_value_color'] }}", "{{ $graph['graph_list_remainder_color'] }}"],    "innerRadius": 10, "radius": 40 }'>0/100</span>
                    </span>

                    <span class="inner-text"><span class="wrap-text">{!! $graph['graph_list_circle_value']
                            !!}</span></span>
                    <span class="remainder" style="color: {{ $graph['graph_list_remainder_color'] }}">
                        {{ $graph['graph_list_title_remainder'] }}
                        @if( !empty( $graph['graph_list_value'] ) )
                        <span>{{ 100 - $graph['graph_list_value']  }}%</span>
                        @endif
                    </span>
                </div>
            </div>
            @endforeach--}}
        </div>
    </div>
</div>
@endif
@include('services.list')
<div class="looking-forward" id="looking-forward">
    <!-- 
    @if($landing_text_blocks_title || $landing_text_blocks_subtitle)
    <div class="title-looking-forward animate-element opacity-animate">
        <div class="container container-large">
            @if($landing_text_blocks_title)
            <h2>{!! $landing_text_blocks_title !!}</h2>
            @endif
            @if($landing_text_blocks_subtitle)
            <div class="sub-title">
                {!! $landing_text_blocks_subtitle !!}
            </div>
            @endif
        </div>
    </div>
    @endif
    @if( $landing_text_blocks_image )
    <img class="title-image animate-element opacity-animate shareable-image" src="{{ $landing_text_blocks_image }}" />
    @endif
    {{-- Themosis infinite fields contain at least one empty array of data after publishing --}}
    {{-- So checking the field is so complicated --}}
    @if(!empty($landing_text_blocks) && array_filter(array_first($landing_text_blocks)))
    <div class="container container-large">
        <ul class="list-squares-circles">
            @foreach($landing_text_blocks as $block)
            <li class="animate-element opacity-animate">
                <div class="title">{!! $block['title'] !!}</div>
                <div class="description">
                    {!! $block['text'] !!}
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
     -->
   
    @if($landing_tables_title || $landing_tables_text)
    @if(!empty($landing_tables_anchor))
    <div class="service" id="{!! str_replace(' ','-', strtolower($landing_tables_anchor)) !!}">
        @else
        <div class="service">
            @endif
            <div class="container container-content">
                <h2 class="title">{!! $landing_tables_title !!}</h2>
               {!! $landing_tables_text !!}
            </div>
        </div>
        @endif
    </div>


    <!-- <div class="landing-download-section">
        <div class="download-container report-block">
            <h2>Data and Downloads</h2>
            <div class="download-items">
                <ul>
                    <li>
                        <a class="third-button animate-element opacity-animate animate"
                            <?php if(!empty($landing_one_report_pdf)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_one_report_pdf) ?>" download <?php } ?>>
                            {!! $landing_one_report_pdf_btn_name !!}
                        </a>
                    </li>
                    <li>
                        <a class="third-button animate-element opacity-animate animate"
                            <?php if(!empty($landing_sasb_index)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_sasb_index) ?>" download <?php } ?>>
                            {!! $landing_sasb_index_btn_name !!}
                        </a>
                    </li>
                    <li>
                        <a class="third-button animate-element opacity-animate animate"
                            <?php if(!empty($landing_gri_index)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_gri_index) ?>" download <?php } ?>>
                            {!! $landing_gri_index_btn_name !!}
                        </a>
                    </li>
                    <li>
                        <a class="third-button animate-element opacity-animate animate download-icon"
                            <?php if(!empty($landing_people_fact_sheet)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_people_fact_sheet) ?>" download <?php } ?>>
                            {!! $landing_people_fact_sheet_btn_name !!}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> -->

    @if(!empty($landing_one_report_pdf) || !empty($landing_sasb_index) || !empty($landing_gri_index) ||
    !empty($landing_people_fact_sheet))
    <div class="landing-download-section">
        <div class="download-container report-block">
            <h2>Data and Downloads</h2>
            <div class="download-items">
                <ul>
                    @if($landing_one_report_pdf)
                    <li>
                        <a class="third-button animate-element opacity-animate animate"
                            <?php if(!empty($landing_one_report_pdf)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_one_report_pdf) ?>" download <?php } ?>>
                            {!! $landing_one_report_pdf_btn_name !!}
                        </a>
                    </li>
                    @endif
                    @if($landing_sasb_index)
                    <li>
                        <a class="third-button animate-element opacity-animate animate"
                            <?php if(!empty($landing_sasb_index)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_sasb_index) ?>" download <?php } ?>>
                            {!! $landing_sasb_index_btn_name !!}
                        </a>
                    </li>
                    @endif
                    @if($landing_gri_index)
                    <li>
                        <a class="third-button animate-element opacity-animate animate"
                            <?php if(!empty($landing_gri_index)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_gri_index) ?>" download <?php } ?>>
                            {!! $landing_gri_index_btn_name !!}
                        </a>
                    </li>
                    @endif
                    @if($landing_people_fact_sheet)
                    <li>
                        <a class="third-button animate-element opacity-animate animate download-icon"
                            <?php if(!empty($landing_people_fact_sheet)){ ?>
                            href="<?php echo wp_get_attachment_url($landing_people_fact_sheet) ?>" download <?php } ?>>
                            {!! $landing_people_fact_sheet_btn_name !!}
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection