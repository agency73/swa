@extends('layouts.master')

@section('main')
<style type="text/css">
    
</style>
    <div class="about-one-report">
    <div class="top-block">
        <div class="bg-image" style="background-image: url({{ $image }})"></div>
        <div class="bg-image mobile-image">
            <img src="{{ $image }}" alt="mobile-image" />
        </div>
        <div class="container container-content">            
            <h2 class="title animate-element opacity-animate">{!! $title !!}</h2>
            <div class="content animate-element opacity-animate">{!! $body !!}</div>
        </div>
    </div>
    <div class="our-approach container container-content animate-element opacity-animate">
        <h2 class="our-approach-title">{!! $our_approach_title !!}</h2>
        <div class="oa_details our-approach-desc">{!! $our_approach_detail !!}</div>
        <!-- <div class="oa_moredetails our-approach-moredetails" id="oa_moredetails" style="display: none;">
        <button id='show_less' class="third-button animate-element opacity-animate animate third-button third-button_hover">Show less</button>
            <div class="oa_details our-approach-image"> 
                <a <?php if($our_approach_doc != ''){ ?> href="{{ wp_get_attachment_url($our_approach_doc) }}" download <?php } ?> target="_blank" > <img src="{{$our_approach_image}}"> </a>
            </div>
            <div class="oa_details our-approach-table">{!! $our_approach_table_content !!}</div>
        </div> 
        <button id='hideshow' class="third-button animate-element opacity-animate animate third-button">Learn more</button>
        -->
    </div>
    <!-- <div class="service" id="glance-section">
        @if(!empty($glance_title))
            <div class="container container-content animate-element opacity-animate">
                <h2 class="title">{!! $glance_title !!}</h2>
                <div>{!! $glance_text !!}</div>
            </div>
        @endif
    </div>
    @if($tabs)
        <div class="one-report-tabs">
            <div class="container">
                <div class="tabs">
                    <div class="tabs-content-one-report">
                        <div class="slider-one-report">
                            @foreach($tabs as $tab)
                                <div id="{{$tab['tab_title']}}" class="tab-hide-one-report">
                                    <div class="slider-item">
                                        <div class="area-one flex">
                                            <div class="slider-media">
                                                <div class="wrap">
                                                    @if(!empty($tab['area_one_image']) && $tab['area_one_media'] == 0)
                                                        <img class="slider-img" src="{{$tab['area_one_image']}}" alt="{!! $tab['tab_title'] !!}">
                                                    @elseif(!empty($tab['area_one_video']) && $tab['area_one_media'] == 1)
                                                        {!! $tab['area_one_video'] !!}
                                                    @else
                                                        {!! $tab['area_one_video'] !!} || <img class="slider-img" src="{{$tab['area_one_image']}}" alt="{!! $tab['tab_title'] !!}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="slider-content">
                                                <div class="wrap">
                                                    <h3 class="title">{!! $tab['area_one_title'] !!}</h3>
                                                    <p class="tab-slider-text">{!! $tab['area_one_text'] !!}</p>
                                                    <a href="{{ $tab['area_one_url'] }}">Learn More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-wrap flex">
                                            <div class="area-two">
                                                <div class="slider-content">
                                                    <div class="wrap">
                                                        <h3 class="title">{!! $tab['area_two_title'] !!}</h3>
                                                        <p class="tab-text">{!! $tab['area_two_text'] !!}</p>
                                                        <a href="{{ $tab['area_two_url'] }}">Learn More</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="area-three">
                                                <div class="slider-media">
                                                    <div class="wrap">
                                                        <img src="{{$tab['area_three_image']}}" alt="{!! $tab['tab_title'] !!}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wrap-button">
                                            <a class="third-button animate-element opacity-animate animate" href="{{ $tab['url_page'] }}">
                                                {!! $tabs_button_text !!}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
     -->
    <div class="service animate-element opacity-animate corporate-report" id="section-gri">
        <div class="container container-content">
            <h2 class="title">{!! $gri_title !!}</h2>
            {!! $gri_text !!}
        </div>
    </div>
    <div class="global_golas_section container container-content animate-element opacity-animate animate" id="section-global_golas">
        <div class="global-goals-section-content">
            <h2 class="title">{!! $hw_goals_title !!}</h2>
            <div class="description">
                <div class="video-wrap">
                    <img src="{{ $goals_image }}" alt="" class="shareable-image" width="80px">                
                </div>
                <div class="sub-title">{!! $hw_goals_description !!}</div>
            </div>
        </div>
        @if(!empty($hw_goals_icons_data))
        <div class="sdg_icons_div">
            <div class="sdg_icons_img">
                @foreach($hw_goals_icons_data as $hw_goals_data)
                    <img src="{{ wp_get_attachment_url($hw_goals_data['hw_goals_icon']) }}" alt="" class="shareable-image">
                @endforeach
            </div>
        </div>
        <div class="sdg_details_div" id="sdg_details_div" style="display: none;">
            <button id='goals_show_less' class="third-button animate-element opacity-animate animate third-button_hover">Show less</button>
            @foreach($hw_goals_icons_data as $hw_goals_data)
                <div class="sdg_details_content">                  
                    <img src="{{ wp_get_attachment_url($hw_goals_data['hw_goals_icon']) }}" alt="" class="shareable-image">
                    <div class="sdg_details-desc">
                        <h5 class="sdg_details_title">{!! $hw_goals_data['hw_goals_icon_title'] !!}</h5>
                    <p class="sdg_details_Subtitle">{!! $hw_goals_data['hw_goals_icon_detail'] !!}</p>
                    </div>
                </div>
            @endforeach
        </div>  
        @endif              
        <button id='goals_hideshow' class="third-button animate-element opacity-animate animate">Full SDG Alignment</button>        
    </div> 

    @report()
    @if(!empty($archive_box))
        <div class="service reports-download report-about-download" id="section-archive">
            <div class="container">
                <h3 class="title animate-element opacity-animate">{!! $archive_title !!}</h3>
                <div class="image-tile report-archive">
                    @foreach($archive_box as $archive)
                        @if($loop->index <= 2)
                        <div class="image-wrap animate-element opacity-animate">
                            <div class="blue-wrap">
                                <div class="image-tile-title">{!! $archive['title'] !!}</div>
                                <img src="{{ wp_get_attachment_url($archive['image']) }}" alt="" class="shareable-image">
                            @if($archive['doc'])
                                <a href="{{ wp_get_attachment_url($archive['doc']) }}" target="_blank" class="main-button download-icon" download >{!! $archive_link_text !!}</a>
                            @endif
                            </div>
                        </div>
                        @endif
                        @if($loop->index > 2 && $archive['doc'])
                        <div class="image-wrap animate-element opacity-animate" style="background-color: #fff;">
                            <div class="link-wrap">
                                <a href="{{ wp_get_attachment_url($archive['doc']) }}" target="_blank" class="" download>
                                {!! $archive['title'] !!}{!! $archive_link_text !!}
                            </a>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
                {!! $archive_text !!}
            </div>
        </div>
    @endif
    </div>
    <script>
       jQuery(window).load(function() {
        console.log(1);
        var url = document.location.toString();
        if ( url.match('#') ) {
            var hash = url.split('#')[1];
            if(jQuery("#"+hash).length > 0){
                 console.log(2);
                 // collapse the expanded panel
                jQuery("#"+hash).parent().click();

                    jQuery('html, body').animate({
                        scrollTop: jQuery("#"+hash).offset().top
                    }, 2000);
               
            }

           
        }
    });
    </script>
@endsection