<div class="search-box">
    @if($result['image_thumb_url'])
        <div class="wrap-image">
            <img src="{{ $result['image_thumb_url'] }}" alt="">
        </div>
    @endif
    <div class="content">
        @if($result['page_tag'])
            <div class="page-tag"><span style="background: {{ $result['page_color'] }};">{!! $result['page_tag'] !!}</span></div>
        @endif

        @if($result['post_title'])
            <h2 class="title-search-box"><a href="{!! get_permalink($result['ID']) !!}">{!! $result['post_title'] !!}</a>
            </h2>
        @endif

        {!! $result['found_text'] !!}
    </div>
</div>