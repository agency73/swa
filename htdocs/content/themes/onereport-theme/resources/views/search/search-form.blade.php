<form class="search-form" action="{!! home_url() !!}" method="get">
    <div class="form-group">
        <input type="search" class="search" placeholder="Search" name="s">
        <button type="submit">Search</button>
    </div>
</form>