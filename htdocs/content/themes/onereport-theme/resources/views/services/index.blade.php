@if($image_url || $subtitle)
    <div class="title-topic animate-element opacity-animate" @if($title) id="{!! sanitize_html_class( $title ) !!}" @endif>
        <img src="{{ $image_url }}" class="shareable-image">
        @if($subtitle)
            <div class="sign"><span>{!! $subtitle !!}</span></div>
        @endif
    </div>
@endif
@if($title || $body)
    <div class="content animate-element opacity-animate">
        <div class="container">
            @if($title)
                <h2>
                    {!! $title  !!}
                </h2>
            @endif
            @if($body)
                <div class="rows-{{ $service_text_cols }}">
                    {!! $body !!}
                </div>
            @endif
            {{--TBD --}}
            {{--<img src="http://192.168.121.80/htdocs/content/uploads/2017/04/twiter-mesage.png"/>--}}
        </div>
    </div>
@endif

{{-- Themosis infinite fields contain at least one empty array of data after publishing --}}
{{-- So checking the field is so complicated --}}
@if(!empty($info) && array_filter(array_first($info)))
    <div class="custom-figure">
        <div class="container">
            @foreach($info as $box)
                <div class="column animate-element" data-bottom-position="80">
                    @if($box['media_url'])
                        <img src="{{ $box['media_url'] }}" alt="{{ $box['title'] }}" class="shareable-image">
                    @endif
                    <div class="animate-element opacity-animate" data-bottom-position="75">
                        @if($box['title'])
                            <div class="title">
                                {!! $box['title'] !!}
                            </div>
                        @endif
                        @if($box['text'])
                            <div>
                                {!! $box['text']  !!}
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
<!-- 
@if($service_info_left_text || $service_info_middle_text ||  $service_info_right_text)
    <div class="circle-content">
        <div class="container">
            @if($service_info_left_text || $service_info_left_title)
                <div class="circle animate-element" data-bottom-position="70">
                    <div class="inner-circle">
                        @if($service_info_left_title)
                            <div class="number">{!! $service_info_left_title !!}</div>
                        @endif
                        {!! $service_info_left_text !!}
                        <div class="wrap-image">
                            <img src="{{ $service_info_left_icon  }}" alt="" class="shareable-image">
                        </div>
                    </div>
                </div>
            @endif

            @if($service_info_middle_text || $service_info_middle_note)
                <div class="circle big-circle animate-element" data-bottom-position="70">
                    <div class="inner-circle">
                        {!! $service_info_middle_text !!}
                        @if($service_info_middle_note)
                            <div class="signature">{!! $service_info_middle_note !!}</div>
                        @endif
                    </div>
                </div>
            @endif

            @if($service_info_right_title || $service_info_right_text)
                <div class="circle animate-element" data-bottom-position="70">
                    <div class="inner-circle">
                        @if($service_info_right_title)
                            <div class="number">{!! $service_info_right_title !!}</div>
                        @endif
                        {!! $service_info_right_text !!}
                        <div class="wrap-image">
                            <img src="{{ $service_info_right_icon  }}" alt="" class="shareable-image">
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif
 -->
@include('stories.list-home')