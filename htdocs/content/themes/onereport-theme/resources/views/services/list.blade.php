@if(!empty($services))
    @foreach($services as $k => $service)
        @php
            $color_class = ($loop->index % 2) ? '' : ' blue-topic';
        @endphp
        <div class="topic{{ $color_class }} landing-loop-topic" data-service="{{ $service['id'] }}" id="service-{{$loop->index}}">
            @include('services.index', $service)
        </div>
    @endforeach
@endif