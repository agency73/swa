var webpack = require('webpack'),
    path = require('path');

module.exports = {
    cache: true,
    target: 'web',
    entry: {
        peity: path.join(__dirname, 'assets/js/jquery.peity.min.js'),
        magnific: path.join(__dirname, 'assets/js/jquery.magnific-popup.min.js'),
        theme: path.join(__dirname, 'assets/js/theme.js'),
        tinymce: path.join(__dirname, 'assets/js/tinymce.js'),
        pageslide: path.join(__dirname, 'assets/js/pageslide.js'),
        pageback: path.join(__dirname, 'assets/js/pageback.js'),
        ga: path.join(__dirname, 'assets/js/ga.js'),
        custom: path.join(__dirname, 'assets/js/custom.js'),
        modal: path.join(__dirname, 'assets/js/jquery.modal.min.js')
    },
    output: {
        path: path.join(__dirname, 'dist/js'),
        publicPath: '',
        filename: '[name].min.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    },
    externals: {
        jquery: 'jQuery',
        backbone: 'Backbone',
        underscore: '_'
    },
    plugins: [
        new webpack.ProvidePlugin({
            // Automatically detect jQuery and $ as free var in modules
            // and inject the jquery library
            // This is required by many jquery plugins
            jquery: "jQuery",
            $: "jQuery",
            backbone: "Backbone",
            underscore: "_"
        })
    ]
};