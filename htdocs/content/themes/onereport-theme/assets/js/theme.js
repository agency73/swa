import slider from "slick-carousel";
import objectFitImages from 'object-fit-images';

(function ($) {
    var openClass = 'open';
	$(function () {
        var $report = $('.report'),
            $nav = $('.wrap-navigation'),
            $btnGenerate = $report.find('.btn-generate'),
            $btnDownload = $report.find('.btn-download'),
            $status = $report.find('.status'),
            $lnkSlide = $('.lnk-slide'),
            $notice = $report.find('.notice'),
            inactiveClass = 'inactive',
            loadingClass = 'loading',
            fixClass = 'fix-element';

        //Polyfill for image in IE
        objectFitImages();

        // function getTimeReadSliderText(sliderText) {
        //     let times= [];
        //     for (let text of sliderText) {
        //         times.push(text.innerHTML.length);
        //     }
        //     return (Math.max.apply(null, times) + '00') / 2;
        // };

        const TabsButtonsName = (function getTabsButtonsName () {
            const tabs = $('.tab-hide-one-report');
            var arr = [];
            tabs.each(function (index, item) {
                arr.push(item.getAttribute('id'));
            })
            return arr;
        })();

        $('.slider').slick({
            dots: true,
            adaptiveHeight: false,
            autoplay: true,
            autoplaySpeed: 35000,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: false,
            useCSS: true,
            waitForAnimate: true,
            zIndex: 0
        });

    $('.slider-landing').slick({
            dots: true,
            adaptiveHeight: false,
            autoplay: true,
            autoplaySpeed: 35000,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            fade: false,
            useCSS: true,
            waitForAnimate: true,
            zIndex: 0,
            responsive: [
              {
                breakpoint: 1023,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 1022,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
        });

        $('.slider-one-report').slick({
            dots: true,
            dotsClass: 'tab-buttons-wrap',
            customPaging: function(slider, i) {
                return '<span class="tab-button-one-report">' + TabsButtonsName[i] + '</span>';
            },
            adaptiveHeight: true,
            autoplay: true,
            autoplaySpeed: 35000,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
        });

        var report = new Report();

        $btnGenerate.on('click', function (event) {
          event.preventDefault();

          if ($btnGenerate.hasClass(inactiveClass)) {
            return;
          }

          if(report.loading) {
            return;
          }

          var data = report.buildFileList();

          $btnDownload.addClass(inactiveClass).attr("disabled", true);
          $status.addClass(inactiveClass);
          report.sendRequest(data);
        });

        var $btnCheckAll = $('.report #checkall'),
            $btnClearAll = $('.report #clearall'),
            $reportCheckboxes = $('.report .report-block').find('input[type="checkbox"]');

        var nav = {
              $gamburger: $('.gamburger'),
              $navbar: $('.navbar'),
              $parent: $('body')
            },
            measure = {
              $parent: $('body'),
              measureSelector: '.measure',
              buttonSelector: '.main-button',
              hideInfoSelector: '.hide-info',
              isCloseAnother: false
            },
            subNav = {
              $keyTopics: $('.key-topic'),
              keyTopicTitle: '.disable-link'
            },
            tabs= {
              $parent: $('.tabs-content'),
              tabsSelector: '.tab-hide',
              tabButtonSelector: '.tab-button',
              tabParentSelector: '.tab',
              isCloseAnother: true
            },
            allStory = {
              $parent: $('.all-stories-block'),
              columnHideSelector: '.stories-mobile',
              columnClickSelector: '.story-column-name',
              columnSelector: '.all-stories-column',
              $mobileElement: $('.karat')
            },
            $window = $(window);

        $window.on('load', function () {
            OpenTabs();
            SurveyFormPopupInit();
            sendSurveyForm();
            activeStateSearchInput();
            initScrollAnimate();

          if ((navigator.platform.indexOf("iPhone") != -1) || (navigator.platform.indexOf("iPod") != -1)) {
            $('html').addClass('i-phone');
          }
        });

        handleCheckboxes($btnCheckAll, 1);
        handleCheckboxes($btnClearAll, 0);
        handleSlidePage();

        handleTables();

        initSabNavMenu();

        hoverKeyTopics(subNav.$keyTopics,subNav.keyTopicTitle);

        Accordion(allStory.columnClickSelector, allStory.columnHideSelector, allStory.$parent, allStory.columnSelector,
            function () {

          return allStory.$mobileElement.css('display') == 'none';
        }, function ($element) { slideTo($element, 1000);});
        gamburgerMenu(nav.$gamburger, nav.$navbar, nav.$parent);

        Tabs(measure.buttonSelector, measure.hideInfoSelector, measure.$parent, measure.measureSelector, measure.isCloseAnother);

        Tabs(tabs.tabButtonSelector, tabs.tabsSelector, tabs.$parent, tabs.tabParentSelector, tabs.isCloseAnother,
            function (element) {
            // if (window.sessionStorage) {
            //   var _indexElem = $.inArray(element[0], $.makeArray($(tabs.tabButtonSelector)));
            //   sessionStorage.setItem(location.pathname, _indexElem);
            //   $(window).resize();
            // }
        }, function () {
            // if (window.sessionStorage) {
            //     console.log('test');
            //     sessionStorage.removeItem(location.pathname);
            //     $(window).resize();
            // }
        });

        addTablerotate();

        descriptionPopupInit();
        addTablerotate();
        fixedTable();
        tabs.$parent.find('a').addClass('not-link').click(function () {
            var _indexElem = $.inArray($(this).parents('.tab')[0], $.makeArray($(tabs.tabParentSelector)));
            sessionStorage.setItem(location.pathname, JSON.stringify({index: _indexElem, scrolposition: $(window).scrollTop()}));
            $(window).resize();
        });
        $('a:not(.not-link)').click(function () {
            sessionStorage.clear();
        });

        function handleSlidePage() {
          $lnkSlide.on('click', function (event) {
            event.preventDefault();
            var $link = $(this),
              href = $link.attr('href');

            if (href.length) {
              slideTo($(href), 500);
            }

          });
        }

        function handleCheckboxes($btn, flag) {
            if ($btn.length) {
                $btn.on('click', function (event) {
                  event.preventDefault();
                    $reportCheckboxes.prop('checked', flag);

                    flag
            ? $btnGenerate.removeClass(inactiveClass).attr('disabled', false)
            : $btnGenerate.add($btnDownload).attr('disabled', true).add($status).addClass(inactiveClass);
                });
            }
        }

        $('.custom-checkbox').on('click', function(){
            var checkbox = $(this).find('input[type="checkbox"]'),
                value = checkbox.prop('checked');

            checkbox.prop('checked', !value);

            var checked = checkbox.parents('.list-checkbox').find(':checked');

            checked.length
            ? $btnGenerate.removeClass(inactiveClass).attr('disabled', false)
            : $btnGenerate.add($btnDownload).attr('disabled', true).add($status).addClass(inactiveClass);

            $notice.hide();
            return false;
        });

        function Report() {
          this.loading = false;
                this.buildFileList = function() {
                    var dataList = [],
                $pdfList = $('.report .custom-checkbox');

                    $pdfList.each(function () {
                        var $checkbox = $(this).find('input[type="checkbox"]');
                        if ($checkbox.prop('checked')) {
                            dataList.push($checkbox.val());
                        }
                    });
                    return dataList;
                };

          this.sendRequest = function (filelist) {
            this.loading = true;
            var $reportForm = $('.report-form'),
              request = {
                url      : onereport.ajaxurl, // Global access to the WordPress ajax handler file
                type     : 'POST',
                dataType : 'json',
                data     : {
                  action   : 'generatePDF', // Your custom hook/action name
                  // security : onereport.nonce, // A nonce value
                  filelist : filelist // The value you want to send
                }
              };

            $reportForm.addClass(loadingClass);
            $reportForm.find('.btn-generate').removeClass(inactiveClass).attr('disabled', this.loading);

            var self = this;
            $.ajax(request).success(function (response) {
              if (response.success) {
                $reportForm.attr('action', response.data).removeClass(loadingClass);
                $status.removeClass(inactiveClass);
                if(response.data.length){
                    downloadReport(response.data);
                }
              } else {
                $notice.text(response.data).fadeIn();
              }
              self.loading = false;
            });
          }
        }

        function downloadReport(data) {
            var a = document.createElement('a');
            a.href = data;
            a.download = 'Southwest-One-Report-2019.pdf';
            a.click();
        }
        function gamburgerMenu($clickElement, $hideElement, $parent) {
          $clickElement.click(function (event) {
            event.preventDefault();

            if($(this).hasClass(openClass)) {
              Close($clickElement, $hideElement, $parent);
            } else {
              Open($clickElement, $hideElement, $parent);
            }
          });

          function resize() {
            if($clickElement.css('display') == 'none') {
              Clear($clickElement, $hideElement, $parent);
            }
          }

          $window.resize(resize);
        }
        function Accordion(selectorClickElement, selectorHideElement, $parent, selectorChild, desktopCheck, callBackOpen, calBackClose) {
            var $clickElements = $parent.find(selectorClickElement),
                _callBackOpen = (typeof callBackOpen == "undefined") ? function() {} : callBackOpen,
                _callBackClose = (typeof calBackClose == "undefined") ? function() {} : calBackClose;


          $clickElements.click(function (event) {
                if (desktopCheck()) {
                  return true;
                }

                event.preventDefault();

                var $this = $(this),
                  $thisParent = $this.parents(selectorChild);

                if ($this.hasClass(openClass)) {
                    Close($this, $thisParent.find(selectorHideElement), $thisParent,function () {
                      _callBackClose($this);
                    });
                }
                else {
                    Close($clickElements, $parent.find(selectorHideElement), $parent.find(selectorChild));
                    Open($this, $thisParent.find(selectorHideElement), $thisParent, function () {
                      _callBackOpen($this);
                    });
                }

                $window.resize(resize);

                function resize() {
                    if (desktopCheck()) {
                      Clear($clickElements, $parent.find(selectorHideElement), $parent.find(selectorChild));
                    }
                }
            });
        }

       /* //Functionality of Open/Close block
        openClose(footnotes.$footnotesButton, footnotes.$footnotesContent, footnotes.$footnotes, function () {
            slideTo(footnotes.$footnotesButton, 400);
        });*/
        /*function openClose($clickElement, $hideElement, $parent, callbackOpen) {
          var _callbackOpen = function () {};
          if(callbackOpen) {
            _callbackOpen = callbackOpen;
          }
          $clickElement.click(function (event) {
            event.preventDefault();
            if($(this).hasClass(openClass)) {
              Close($clickElement, $hideElement, $parent);
            } else {
              Open($clickElement, $hideElement, $parent);
              _callbackOpen();
            }
          });
        }*/

        function Tabs(selectorClickElement, selectorHideElement, $parent, selectorChild, isClose, callBackOpen, calBackClose) {
          var $clickElements = $parent.find(selectorChild + ' ' + selectorClickElement);
          var _callBackOpen = (typeof callBackOpen == "undefined") ? function() {} : callBackOpen;
          var _callBackClose = (typeof calBackClose == "undefined") ? function() {} : calBackClose;

          $clickElements.click(function (event) {
            event.preventDefault();

            var $this = $(this),
                $thisParent = $this.parents(selectorChild);
            if($this.hasClass(openClass)) {
              Close($thisParent.find(selectorClickElement), $thisParent.find(selectorHideElement), $thisParent, function () {
                  _callBackClose($this);
              });
            } else {
              if(isClose){
                Close($clickElements, $parent.find(selectorHideElement), $parent.find(selectorChild));
              }
              Open($thisParent.find(selectorClickElement), $thisParent.find(selectorHideElement), $thisParent, function() {
                slideTo($thisParent, 300);
                _callBackOpen($this);
              });
            }
          });
        }

        function slideTo($element, duration) {
            if(typeof $element.offset() !== "undefined") {
                var navOffset = ($nav.length) ? $nav.height() : 0;
                $('html, body').animate({
                    scrollTop: $element.offset().top - navOffset
                }, duration);
            }
        }

        function Open($clickElement, $hideElement, $parent, callback ) {
            var complete = (typeof callback == "undefined") ? function() {} : callback;
            $hideElement.slideDown({
                duration: 300,
                queue: false,
                complete: complete
          });

          $hideElement.addClass(openClass);
          $clickElement.addClass(openClass);
          $parent.addClass(openClass);
        }
        function Close($clickElement, $hideElement, $parent, callback) {
            var complete = (typeof callback == "undefined") ? function() {} : callback;
            $hideElement.slideUp({
                duration: 300,
                queue: false,
                complete: complete
          });

          $hideElement.removeClass(openClass);
          $clickElement.removeClass(openClass);
          $parent.removeClass(openClass);
        }
        function Clear($clickElement, $hideElement, $parent) {
            $hideElement.removeClass(openClass).css('display', '');
            $clickElement.removeClass(openClass).css('display', '');
            $parent.removeClass(openClass).css('display', '');
        }

        function addTablerotate() {
          var $tableWrap = $('.table-wrap'),
            $table = $tableWrap.find('.tablepress');

          if($tableWrap.width() < $table.width()) {
            $tableWrap.find('.tablepress-table-description').append('<div class="fin-nav"><span>To View Other Years </span><span class="arrow left">&#8249;</span> <span class="arrow right">&#8250;</span></div>');
            $tableWrap.find('.arrow.left').click(function () {
                var $scrollElement = $('.fixed-table-inner-wrap:last-child'),
                    $ths = $scrollElement.find('th');

                for(var i = $ths.length -1; i > 0; i--) {
                    if($($ths[i]).position().left - $($ths[0]).outerWidth() < -1) {
                        $scrollElement.scrollLeft($($ths[i]).position().left - $($ths[0]).outerWidth() + $scrollElement.scrollLeft());
                        break;
                    }
                }
            });
              $tableWrap.find('.arrow.right').click(function () {
                  var $scrollElement = $('.fixed-table-inner-wrap:last-child'),
                      $ths = $scrollElement.find('th');

                  for(var i = 1; i < $ths.length; i++) {
                      if($($ths[i]).position().left - $($ths[0]).outerWidth() > 1) {
                          $scrollElement.scrollLeft($($ths[i]).position().left - $($ths[0]).outerWidth() + $scrollElement.scrollLeft());
                          break;
                      }
                  }
              });
          }
        }

        function OpenTabs() {
            var _storage =  JSON.parse(sessionStorage.getItem(location.pathname));
            if(_storage) {
                var _$temp = $($(tabs.tabButtonSelector)[_storage.index]);
                var $thisParent = _$temp.parents(tabs.tabParentSelector);
                Open($thisParent.find(tabs.tabButtonSelector), $thisParent.find(tabs.tabsSelector), $thisParent, function () {
                    setTimeout(function () {
                        $(window).resize();
                        $(window).scrollTop(_storage.scrolposition);
                    }, 700);
                });
            }
        }
        function handleTables() {
          var $tableWrap = $('.table-wrap');
          $tableWrap.find('td[colspan]').addClass('colspan-cell');
          $tableWrap.find('td[rowspan]').addClass('rowspan-cell');
        }

        function fixedTable() {
            $('.tablepress').each(function () {
                var $this = $(this);

                $this.wrap('<div class="wrap-fixed-table"></div>');
                var $parent = $this.parent();

                $this.after($(this).clone().addClass('fixed-table'));
                $this.after($(this).clone().addClass('fixed-table'));
                $this.wrap('<div class="fixed-table-inner-wrap"></div>');

                $parent.find('.fixed-table').wrap('<div class="fixed-table-inner-wrap"></div>');

                $parent.find('.fixed-table-inner-wrap:last-child').append('<div class="scroll"></div>');
                $parent.find('.colspan-cell').each(function () {
                    $(this).html('<div class="inner-cell">' + $(this).html() + '</div>')
                });

                $parent.find('.fixed-table-inner-wrap:first-child').height($parent.find('.fixed-table-inner-wrap:first-child').find('th').outerHeight()).width($parent.find('.scroll').width());
                $parent.find('.fixed-table-inner-wrap:nth-child(2)').height($parent.find('.scroll').height()).width($parent.find('.scroll').width());
                $parent.find('.fixed-table-inner-wrap:last-child').scroll(function () {
                    $parent.find('.fixed-table-inner-wrap:first-child').scrollLeft($(this).scrollLeft());
                    $parent.find('.fixed-table-inner-wrap:nth-child(2)').scrollTop($(this).scrollTop());
                });
                $(window).resize(function () {
                    $parent.find('.fixed-table-inner-wrap:first-child').height($parent.find('.fixed-table-inner-wrap:first-child').find('th').outerHeight()).width($parent.find('.scroll').width());
                    $parent.find('.fixed-table-inner-wrap:nth-child(2)').height($parent.find('.scroll').height()).width($parent.find('.scroll').width());
                });
                $(window).scroll(function () {
                    var scrollTop = $(this).scrollTop() + $nav.height(),
                        elementOffsetTop = $parent.offset().top;

                    if(elementOffsetTop < scrollTop && (elementOffsetTop + $parent.height()) > scrollTop) {
                        if(!$parent.hasClass(fixClass)) {
                            $parent.addClass(fixClass);
                        }
                    } else {
                        if($parent.hasClass(fixClass)) {
                            $parent.removeClass(fixClass);
                        }
                    }
                })
            });
        }

        function descriptionPopupInit() {
            var openClass = 'open',
              preOpenClass = 'pre-open',
              popupClass = 'descriptions-popup',
              $body = $('body'),
              zIndex = 'js-z-index',
              $parent = $('.footnote-link').parent('div'),
              $slickArrow = $('.slick-arrow');

            $body.append('<div class="background-element"></div>');

            var $backgroundElement = $('.background-element');

            $backgroundElement.click(function () {
                $('.' + popupClass + '.' + openClass).removeClass(openClass);
                $backgroundElement.removeClass(openClass);
                $slickArrow.removeClass('under');
            });

            $body.on('click', '.footnote-link', function () {
                var $this = $(this),
                $descriptionsPopup = $this.find('.' + popupClass);

                $descriptionsPopup.addClass(preOpenClass);

                positionPopup($descriptionsPopup);
                $this.parent('div').addClass(zIndex);
                $descriptionsPopup.addClass(openClass).removeClass(preOpenClass);
                $backgroundElement.addClass(openClass);
                // return false;

                // slick arrows must be show under description popup
                if ($descriptionsPopup.hasClass(openClass)) {
                    $slickArrow.addClass('under');
                }
            });

            $window.resize(function () {
            var $descriptionsPopup = $('.descriptions-popup.open');
            if($descriptionsPopup.length > 0) {
              positionPopup($descriptionsPopup);
            }
            });

          function positionPopup($descriptionsPopup) {

            // console.log();

            $descriptionsPopup.css('margin-left', 0);

            var descriptionsOffsetLeft = $descriptionsPopup.offset().left,
              descriptionsWidth = $descriptionsPopup.outerWidth(),
              MARGIN_SIDE = 20;

            if($descriptionsPopup.height() < $descriptionsPopup.find('.descriptions-popup-inner').height()) {
              $descriptionsPopup.addClass('scroll');
            } else {
              $descriptionsPopup.removeClass('scroll');
            }
            if(descriptionsOffsetLeft < MARGIN_SIDE) {
              $descriptionsPopup.css('margin-left', MARGIN_SIDE - descriptionsOffsetLeft + 'px');
            }
            if(descriptionsOffsetLeft + descriptionsWidth + MARGIN_SIDE > $(window).width()) {
              $descriptionsPopup.css('margin-left',
                $(window).width() - (descriptionsOffsetLeft + descriptionsWidth + MARGIN_SIDE)+ 'px');
            }
          }
        }

        function DynamicPopupInit() {
          var options = {
            openedPopupCookie: 'popupOpened',
            countPageCookie: 'countPage',
            timeStartCookie: 'timePopup',
            $popup: $('.survey-popup-wrap'),
            timer: null,
            // timeForOpenPopup: 120000,
            // scrollPath: 0.6,
            cookieOptions: {
              path: '/'
            },
            openDynamicPopup: function () {
              if(getCookie(this.openedPopupCookie) === 'true') {
                return;
              }
              this.$popup.fadeIn(300).addClass('open');

              setCookie(this.openedPopupCookie, true, this.cookieOptions);
            },
            closeDynamicPopup: function () {
              this.$popup.removeClass('open').css('display', 'none');
            }
          };

          if(getCookie(options.openedPopupCookie) === 'true') {
            return;
          }

          //Disable show pop-up by timer

        /* if(getCookie(options.timeStartCookie)) {
            var timeOnSite = Date.now() - getCookie(options.timeStartCookie);
            if(timeOnSite >= options.timeForOpenPopup) {
              options.openDynamicPopup(options.$popup);
            } else {
              setTimeout(function () {
                options.openDynamicPopup(options.$popup);
              }, options.timeForOpenPopup - timeOnSite);
            }
          } else {
            setCookie(options.timeStartCookie, Date.now(), options.cookieOptions);
          }*/

          if(getCookie(options.countPageCookie)) {
            var numberPage = getCookie(options.countPageCookie) - 0;
            if(numberPage >= 2) {
              options.openDynamicPopup(options.$popup);
            } else {
              setCookie(options.countPageCookie, numberPage+1, options.cookieOptions);
            }
          } else {
            setCookie(options.countPageCookie, 1, options.cookieOptions);
          }

            //Disable show pop-up by scroll
/*        $window.scroll(function () {
            if($window.scrollTop()/$('body').height() >= options.scrollPath) {
              options.openDynamicPopup(options.$popup);
            }
          });*/

          options.$popup.find('.close').click(function () {
            options.closeDynamicPopup(options.$popup);
          });

          return options;
        }
        function SurveyFormPopupInit() {
          var dynamicPopup = DynamicPopupInit(),
              $formLeaveModal = $('.form-leave-wrap'),
              dirtyForm = false,
              $surveyFormPopup = $('.open-survey-form').magnificPopup({
                items: {
                  src: '#survey-form',
                  type: 'inline'
                },
                modal: true,
                callbacks: {
                  open: function () {
                    var that = this;

                    if(dynamicPopup){
                      dynamicPopup.closeDynamicPopup();
                    }

                    var $survey = $('#form-survey-from'),
                      $success = $('.success-message'),
                      $error = $survey.find('.error');
                    $survey.removeClass('close');
                    $success.text('');
                    $error.text('');

                    $('html').addClass('stop-scroll');

                    $('.mfp-wrap').click(function (e) {
                      if(e.target.className == 'mfp-content' && $(window).width() > 800) {

                          //hotfix for ga outside click. Need transfer to ga.js
                          var page_name = $('meta[property="og:title"]').attr('content').replace(/\s+\| Southwest One Report$/g, '');
                          if (page_name == 'Southwest One Report') {
                              page_name = 'Home'
                          }
                          ga('send', {
                              hitType: 'event',
                              eventCategory: 'Pop-up Survey',
                              eventAction: 'pop-up survey click',
                              eventLabel: 'Pop-up Survey is closed on "' + page_name + '" page by outside without completing'
                          });

                          //end hotfix

                        closeModal(that);
                      }
                    });
                    $('.mfp-close-custom').click(function () {
                      that.close();
                    });
                    $('.magnific-form').find('input, select, textarea').click(function () {
                      dirtyForm = true;
                    });
                  },
                  beforeClose: function () {
                    dirtyForm = false;
                    $('html').removeClass('stop-scroll');
                  }
                }
              });

          function closeModal(that) {
            if(dirtyForm && $(window).width() > 800) {
              $formLeaveModal.find('.not-leave').click(function () {
                closeFormLeaveModal();
              });
              $formLeaveModal.find('.leave').click(function () {
                closeFormLeaveModal();
                that.close();
              });
              openFormLeaveModal();
            } else {
              that.close();
            }
          }
          function openFormLeaveModal() {
            $formLeaveModal.addClass('open');
          }
          function closeFormLeaveModal() {
            $formLeaveModal.removeClass('open');
          }
        }

        function getCookie(name) {
          var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
          ));
          return matches ? decodeURIComponent(matches[1]) : undefined;
        }
        function setCookie(name, value, options) {
          options = options || {};

          var expires = options.expires;

          if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
          }
          if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
          }

          value = encodeURIComponent(value);

          var updatedCookie = name + "=" + value;

          for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
              updatedCookie += "=" + propValue;
            }
          }

          document.cookie = updatedCookie;
        }

        /**
         * Send Survey form
         */
        function sendSurveyForm() {
            var $survey = $('#form-survey-from'),
                $success = $('.success-message'),
                $error = $survey.find('.error');

            if($survey.length) {
                $survey.find('.other').each(function () {
                  var $this = $(this),
                      $thisRadio = $this.parent().parent().find('.other-radio');

                  $this.focusin(function () {
                    $thisRadio.val($this.val());
                    $thisRadio.prop("checked", true);
                  });
                  $this.keyup(function () {
                    $thisRadio.val($this.val());
                  });
                });

                $survey.submit(function (event) {
                    event.preventDefault();
                    $error.text('');
                    if(!testFields($survey)) {
                      // $error.text("Survey wasn't send. Please fill in at least one option.");
                      $error.text("You must fill in at least one option in order to submit.");
                      return;
                    }
                    var request = {
                            url      : onereport.ajaxurl, // Global access to the WordPress ajax handler file
                            type     : 'POST',
                            dataType : 'json',
                            data     : {
                                action   : 'surveyForm', // Your custom hook/action name
                                security : onereport.nonce, // A nonce value
                                survey   : $survey.serializeArray() // The value you want to send
                            }
                        };
                    $survey.addClass(loadingClass);

                    $.ajax(request).
                        success(function (response) {
                            if (response.success) {
                              $survey[0].reset();
                              $survey.removeClass(loadingClass);
                              $survey.addClass('close');
                              $success.text(response.data);
                              setTimeout(function () {
                                $.magnificPopup.close();
                              }, 2000);
                            } else {
                                $survey.removeClass(loadingClass);
                                $error.text(response.data);
                            }
                        });
                });
            }

            function testFields($form) {
              var testResilt = false;
              $form.find('input[type="radio"]:checked,input[type="checkbox"]:checked, textarea').each(function () {
                if($.trim($(this).val()).length != 0 ) {
                  testResilt = true;
                }
              });
              return testResilt;
            }
        }

        function activeStateSearchInput() {
            var $searchForm = $('.search-form'),
                $input = $searchForm.find('.search');

            if($searchForm.length) {
                $input.focusin( function() {
                  $searchForm.addClass('active-search');
                });
                $input.focusout( function() {
                  $searchForm.removeClass('active-search');
                });
            }
        }

        function initSabNavMenu() {
          var  subNav = {
            $parent: $('.navbar-nav'),
            subMenuSelector: '.sub-menu',
            subMenuClickSelector: '.menu-item-has-children > a > span',
            subMenuParentSelector: '.menu-item-has-children'

          };
          $('.menu-item-has-children > a').append('<span></span>');

          Accordion(subNav.subMenuClickSelector, subNav.subMenuSelector, subNav.$parent, subNav.subMenuParentSelector, function () {
            return nav.$gamburger.css('display') == 'none';
          });
        }

        function hoverKeyTopics(element, clElement) {
          if($(window).width() > 800) {
            element.mouseenter(function () {
              $(this).parents(clElement).addClass('active');
            });
            element.mouseleave(function () {
              $(this).parents(clElement).removeClass('active');
            });
          }
        }


        function initScrollAnimate() {
          var classAnimateElement = 'animate-element',
              classAnimate = 'animate',
              $animateElement = $('.' + classAnimateElement),
              $linearListGraphs = $('.linear-chart'),
              $circleGraphs = $('.wrap-circle-graph');
          initAnimateLinearGraph();

          findAnimateElement();
          $window.scroll(findAnimateElement);

          function findAnimateElement() {
            var windowHeight = $window.height(),
                scrollPosition = $window.scrollTop() + windowHeight*0.9;

            $animateElement.each(function () {
              var customPosition = scrollPosition;

              if($(this).data('bottom-position')) {
                customPosition =  $window.scrollTop() + (windowHeight/100*$(this).data('bottom-position'));
              }
              if(customPosition >= $(this).offset().top) {
                $(this).addClass(classAnimate);
              }
            });

            $circleGraphs.find('.circle-graph').peity("donut");
            $('.custom-circle-graph').css('display', 'block');

            $circleGraphs.each(function () {
              var customPosition = $window.scrollTop();

              if($(this).data('bottom-position')) {
                customPosition =  $window.scrollTop() + (windowHeight/100*$(this).data('bottom-position'));
              }
              if(customPosition >= $(this).offset().top && !$(this).hasClass(classAnimate)) {
                $(this).addClass(classAnimate);
                animateCircleGraph($(this).find('.circle-graph'), $(this).find('.circle-graph').data('percent'), 1000);
              }
            });
          }
          
          function initAnimateLinearGraph() {
            var maxData = 0;


            $linearListGraphs.each(function () {
              var $this = $(this);
              if(maxData < $this.data('percent')-0) {
                maxData = $this.data('percent')-0;
              }
            });

            $linearListGraphs.each(function () {
              var $this = $(this);
              var viewPercent = $this.data('percent')/maxData*100;
              $this.css('width', viewPercent + '%');
              if($this.width() < 40) {
                $this.addClass('outer-number');
              }
            });
          }
          
          function animateCircleGraph($graph, percent, time) {
            setTimeout(function () {
              changePositionGraph($graph, 0, percent, percent/(time/5));
            }, 5);


            $window.resize(function () {
              $graph.peity();
            });

            function changePositionGraph($graph, nowPercent, percent, step) {
              $graph.text(nowPercent + '/' + 100).change();
              $graph.next().css('transform', 'rotate(-' + (3.6 * (nowPercent/2) + 45) + 'deg)');

              var newPercent = nowPercent + step;

              if(newPercent > percent) {
                return
              }
              setTimeout(function () {
                changePositionGraph($graph, newPercent, percent, step);
              }, 4);
            }
          }
        }

	});
})(jQuery);