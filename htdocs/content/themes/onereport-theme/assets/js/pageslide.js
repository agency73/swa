(function ($) {
  $(function () {
    $(window).on('load', function () {

      /*
      SetTimeout fixed IE and Safari
       */

        setTimeout(function () {
            var hash = location.hash;
            // location.hash = '';
            if (hash && hash !== '#') {
                $('html, body').scrollTop($(hash).offset().top - $('.wrap-navigation').height());
                // window.history.pushState(null, null, hash)
                window.history.replaceState(null, null, '');
            }
        }, 0);



      $('.current-menu-item .sub-menu a').on('click', function (e) {
        var navH, href, pathParts, sectionTop, $link, $section, $gamburger;

        navH = $('.wrap-navigation').height();
        $link = $(this);
        href = $link.attr('href');
        pathParts = href.split('#');

        $gamburger = $('.gamburger');
        if (typeof pathParts[1] !== 'undefined') {
          $section = $('#' + pathParts[1]);
          if (typeof $section.offset() !== 'undefined') {
            if ($gamburger.hasClass('open')) {
              $gamburger.click();
            }
            sectionTop = $section.offset().top;
            $('html, body').animate({
              scrollTop: sectionTop - navH
            }, 2000);
            e.preventDefault();
          }
        }
      });

      $('.button-block .wrap-button a').on('click', function (e) {
        var navH, href, pathParts, sectionTop, $link, $section, $gamburger;

        navH = $('.wrap-navigation').height();
        $link = $(this);
        href = $link.attr('href');
        pathParts = href.split('#');

        $gamburger = $('.gamburger');
        if (typeof pathParts[1] !== 'undefined') {
          $section = $('#' + pathParts[1]);
          if (typeof $section.offset() !== 'undefined') {
            if ($gamburger.hasClass('open')) {
              $gamburger.click();
            }
            sectionTop = $section.offset().top;
            $('html, body').animate({
              scrollTop: sectionTop - navH
            }, 2000);
            e.preventDefault();
          }
        }
      });
      
    });
  });
})(jQuery);