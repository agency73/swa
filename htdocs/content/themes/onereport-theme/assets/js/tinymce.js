(function () {
  if(typeof tinymce == 'undefined') return;


  var iconPath = window.location.origin + '/content/themes/onereport-theme/icons/';

  tinymce.create('tinymce.plugins.Onereport', {
    init: function(editor, url) {
      // Buttons
      editor.addButton('accordionBtn', {
        title: 'Accordion',
        cmd: 'accordionBtnCmd',
        image: iconPath + 'icon-accordion.png'
      });

      editor.addButton('quoteBtn', {
        title: 'Select text for marking it as a quote - [quote] shortcode',
        cmd: 'quoteBtnCmd',
        image: iconPath + 'icon-quote.png'
      });

      editor.addButton('iconstatsBtn', {
        title: 'Add story icons - [iconstats] shortcode',
        cmd: 'iconstatsBtnCmd',
        image: iconPath + 'icon-icons.png'
      });

      editor.addButton('testimonialBtn', {
        title: 'Add testimonial box - [testimonial] shortcode',
        cmd: 'testimonialBtnCmd',
        image: iconPath + 'icon-testimonial.png'
      });

      editor.addButton('contentimageBtn', {
        title: 'Add image box with text - [imagetext] shortcode',
        cmd: 'contentimageBtnCmd',
        image: iconPath + 'icon-image.gif'
      });

      editor.addButton('videoBtn', {
        title: 'Video url - [videobox] shortcode',
        cmd: 'videoBtnCmd',
        image: iconPath + 'icon-player.png'
      });

      editor.addButton('infographicBtn', {
        title: 'Infographic - [infographic] shortcode',
        cmd: 'infographicBtnCmd',
        image: iconPath + 'icon-infographic.png'
      });

      editor.addButton('hlBtn', {
        title: 'Select text for marking it as a hyperlink - [hl] shortcode',
        cmd: 'hlBtnCmd',
        image: iconPath + 'icon-hyperlink.png'
      });

      editor.addButton('footnoteBtn', {
          title: 'Select text for marking it as a footnote - [footnote] shortcode',
          cmd: 'footnoteBtn',
          image: iconPath + 'icon-footnote.png'
      });

      editor.addCommand('accordionBtnCmd', function() {
        var win = editor.windowManager.open({
          title: 'Accordion properties',
          body: [
            {
              type: 'textbox',
              name: 'accordionId',
              label: 'Fill in an accordion ID (can be found in the side panel "Accordions" section)',
              minWidth: 50,
              value: ''
            }
          ],
          buttons: [
            {
              text: 'Ok',
              subtype: 'primary',
              onclick: function() {
                win.submit();
              }
            },
            {
              text: 'Cancel',
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e) {
            var params = [],
              shortParams = '';

            if (e.data.accordionId.length) {
              if (isNaN(parseFloat(e.data.accordionId)) || !isFinite(e.data.accordionId)) {
                alert('Accordion ID must be numeric');
                return;
              }

              var titleSlide = 'id="' + e.data.accordionId + '"';
              params.push(titleSlide);
            }

            if (params.length) {
              shortParams = params.join(' ');
              var returnText = '[accordion ' + shortParams + ']';
              editor.execCommand('mceInsertContent', false, returnText);
            }
          }
        });
      });

      editor.addCommand('quoteBtnCmd', function() {
        var return_text;
        var selected_text = editor.selection.getContent();
        if(selected_text.length) {
          return_text = '[quote]' + selected_text + '[/quote]';
          editor.execCommand('mceInsertContent', 0, return_text);
        }
      });

      editor.addCommand('hlBtnCmd', function() {
        var return_text;
        var selected_text = editor.selection.getContent();
        if(selected_text.length) {
          return_text = '[hl]' + selected_text + '[/hl]';
          editor.execCommand('mceInsertContent', 0, return_text);
        }
      });

      editor.addCommand('footnoteBtn', function() {
          var return_text;
          var selected_text = editor.selection.getContent();
          if(selected_text.length) {
              return_text = '[footnote number="' + selected_text + '"]' + selected_text + '[/footnote]';
              editor.execCommand('mceInsertContent', 0, return_text);
          }
      });

      editor.addCommand('iconstatsBtnCmd', function() {
          var return_text = '[iconstats]';
          editor.execCommand('mceInsertContent', 0, return_text);
      });

      editor.addCommand('testimonialBtnCmd', function() {
        var return_text = '[testimonial]';
        editor.execCommand('mceInsertContent', 0, return_text);
      });

      editor.addCommand('contentimageBtnCmd', function() {
        var return_text = '[imagetext]';
        editor.execCommand('mceInsertContent', 0, return_text);
      });

      editor.addCommand('videoBtnCmd', function() {
        var win = editor.windowManager.open({
          title: 'Video properties',
          body: [
            {
              type: 'textbox',
              name: 'videoUrl',
              label: 'Please, insert an EMBED video url',
              minWidth: 400,
              value: ''
            }
          ],
          buttons: [
            {
              text: 'Ok',
              subtype: 'primary',
              onclick: function() {
                win.submit();
              }
            },
            {
              text: 'Cancel',
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e) {
            if (e.data.videoUrl.length) {
              var returnText = '[videobox]'+e.data.videoUrl+'[/videobox]';
              editor.execCommand('mceInsertContent', false, returnText);
            }
          }
        });
      });

      editor.addCommand('infographicBtnCmd', function() {
        var return_text = '[infographic]';
        editor.execCommand('mceInsertContent', 0, return_text);
      });

    }
  });
  tinymce.PluginManager.add('onereport', tinymce.plugins.Onereport);
}());