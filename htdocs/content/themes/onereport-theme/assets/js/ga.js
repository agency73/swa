/**
 * Google Analytics Events Code.
 */
(function ($) {
    $(function () {

        var PAGE_TITLE = $('meta[property="og:title"]').attr('content');
        var PAGE_NAME = PAGE_TITLE ? PAGE_TITLE.replace(/\s+\| Southwest One Report$/g, '') : '';

        /**
         * Every accordion open (on all pages – Performance, People, Planet, GRI Index – what accordions are people opening and clicking on).
         */
        $('.tab-button').on('click', function () {
            var $tab = $(this),
                $tab_name = $tab.text().trim();

            if ($tab.hasClass('open')) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Accordion',
                    eventAction: 'accordion open',
                    eventLabel: $tab_name
                });
            }
        });

        /**
         * Every link on the site (what buttons are people clicking on).
         */
        $('a, button').not('.open-survey-form, .mfp-close-custom, .service-info-inner, .promo-img > .main-button, .ga-footnote ').on('click', function () {
            var $link = $(this),
                $link_text = $link.text().trim();
            if ($link_text.length > 0) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Link/Button',
                    eventAction: 'link/button click',
                    eventLabel: $link_text
                });
            }
        });

        /**
         * Which story tiles people open and click on (and from what page – homepage vs. detail page).
         */
        $('.service-info-inner').on('click', function () {
            var $link = $(this),
                $link_text = $link.find('h3').text().trim();

            // var page_name = $('title').text().replace(/\s+\| Southwest One Report$/g, '');
            var page_name = PAGE_NAME;
            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }
            ga('send', {
                hitType: 'event',
                eventCategory: 'Story tiles',
                eventAction: 'story tile click',
                eventLabel: 'Story tile "' + $link_text + '" opened ' + ' from "' + page_name + '" page'
            });
        });

        /**
         * Close story tiles people open and click on (and from what page – homepage vs. detail page).
         */

        $('.promo-img > .main-button').on('click', function () {
            var $link = $(this);

            // var page_name = $('title').text().replace(/\s+\| Southwest One Report$/g, '');
            var page_name = PAGE_NAME;
            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }
            ga('send', {
                hitType: 'event',
                eventCategory: 'Story tiles',
                eventAction: 'story tile click',
                eventLabel: 'Story tile closed ' + ' from "' + page_name + '" page'
            });
        });

        /**
         * Navigation dropdown menu (what sections do people click to direct from the menu?).
         */
        $('.navbar-nav a').on('click', function () {
            var $link = $(this),
                $nav_item = $link.text().trim(),
                $section_item = $link.parents('.menu-item').find('a')[0].innerText;

            var page_name = PAGE_NAME;
            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }

            ga('send', {
                hitType: 'event',
                eventCategory: 'Navigation dropdown menu',
                eventAction: 'navigation click',
                eventLabel: "Navigation open '" + $nav_item + "' in section" + ' "' + $section_item + '"' +" from page " + ' "' + page_name + '"'
            });
        });

        /**
         * Footnote clicks in text (how many clicks are there on footnotes, and on what pages do people click on the footnotes in the body of the text)
         */
        $('.footnote-link').not('.ga-footnote').on('click', function () {
            var page_name = PAGE_NAME;
            var footItem = $(this).find('strong').text();
            // var page_name = $('title').text().replace(/\s+\| Southwest One Report$/g, '');

            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }

            ga('send', {
                hitType: 'event',
                eventCategory: 'Footnotes',
                eventAction: 'footnote click',
                eventLabel: 'Footnote click' + ' from "' + page_name + '" page, foot-number - ' + footItem
            });
        });

        $('.ga-footnote').on('click', function () {
            var page_name = PAGE_NAME;
            var footItem = $(this).text();
            // var page_name = $('title').text().replace(/\s+\| Southwest One Report$/g, '');

            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }

            ga('send', {
                hitType: 'event',
                eventCategory: 'Footnotes',
                eventAction: 'footnote click',
                eventLabel: 'Footnote click into pop-up' + ' from "' + page_name + '" page and redirect to Footnotes & Disclosures, foot-number - ' + footItem + ")"
            });
        });


        /**
         * Clicks on the footnote bar at the bottom of every page (how many people click to open that bar)
         */
        $('.footnotes-button').on('click', function () {
            if ($(this).hasClass('open')) {
                var page_name = PAGE_NAME;
                // var page_name = $('title').text().replace(/\s+\| Southwest One Report $/g, '');

                if (page_name == 'Southwest One Report') {
                    page_name = 'Home'
                }
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Footnotes',
                    eventAction: 'footnotes bar click',
                    eventLabel: 'Footnotes bar is open on "' + page_name + '" page'
                });
            }
        });

        /**
         * Clicks on the social buttons from the footer.
         */
        $('.social-icons-lists a').on('click', function () {
            var $link = $(this),
                $link_href = $link.attr('href');
            if ($link_href.length > 0) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Social buttons from footer',
                    eventAction: 'social buttons click',
                    eventLabel: $link_href
                });
            }
        });

        /**
         * Number of searches conducted (and terms).
         */
        $('.search-form').on('submit', function () {
            var $form = $(this);
            var search_term = $form.find("input[name='s']").val();

            ga('send', {
                hitType: 'event',
                eventCategory: 'Search',
                eventAction: 'search form submit',
                eventLabel: search_term
            });
        });


        /**
         * Download the Report events
         */
        $('.report-block a.download-icon').on('click', function () {
            var $link = $(this),
                $link_name = $link.text().trim();

            ga('send', {
                hitType: 'event',
                eventCategory: 'Download the Report',
                eventAction: 'download report button click',
                eventLabel: $link_name
            });
        });

        $('.reports-download .download-icon').on('click', function () {            
            var $link = $(this),
                $link_name = $link.parent('.image-wrap').find('.image-tile-title').text().trim();

            ga('send', {
                hitType: 'event',
                eventCategory: 'Report Archive',
                eventAction: 'download report archive',
                eventLabel: $link_name
            });
        });

        /**
         * Build Your Own Report events.
         */
        $('.report-form .btn-generate').on('click', function () {           
            var checkboxes = $('.list-checkbox').find('input[type=checkbox]:checked');
            var checkboxes_text = '';
            var checkboxes_text_array = [];           
            if (checkboxes.length > 0) {
                checkboxes.each(function () {
                    checkboxes_text_array.push($(this).parent('.custom-checkbox').find('span').text());
                });
                if (checkboxes_text_array.length > 0) {
                    checkboxes_text = checkboxes_text_array.join(' | ');

                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Create Your Own Report',
                        eventAction: 'create own report',
                        eventLabel: checkboxes_text
                    });
                }
            }
        });

        $('.report-form .btn-download').on('click', function () {           
            var checkboxes = $('.list-checkbox').find('input[type=checkbox]:checked');
            var checkboxes_text = '';
            var checkboxes_text_array = [];
            if (checkboxes.length > 0) {
                checkboxes.each(function () {
                    checkboxes_text_array.push($(this).parent('.custom-checkbox').find('span').text());
                });
                if (checkboxes_text_array.length > 0) {
                    checkboxes_text = checkboxes_text_array.join(' | ');

                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Create Your Own Report',
                        eventAction: 'download own report',
                        eventLabel: checkboxes_text
                    });
                }
            }
        });


        /**
         * Clicks on the survey form
         */
        $('a.popup-button:not(.popup-button.close)').on('click', function () {
            var page_name = PAGE_NAME;

            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }
            ga('send', {
                hitType: 'event',
                eventCategory: 'Pop-up Survey',
                eventAction: 'pop-up survey click',
                eventLabel: 'Pop-up Survey is open from pop-up on "' + page_name + '" page'
            });
        });

        $('.popup-button.close, .mfp-close-custom').on('click', function (e) {
            var page_name = PAGE_NAME,
                $clickEl = $(this),
                $clickElItem = $clickEl.text().trim();

            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }
            ga('send', {
                hitType: 'event',
                eventCategory: 'Pop-up Survey',
                eventAction: 'pop-up survey click',
                eventLabel: 'Pop-up Survey is closed on "' + page_name + '" page by ' + '"' + $clickElItem + '"'
            });
        });

        /**
         * close Survey form outside
         */
        // $('.mfp-wrap').click(function () {
        //     var page_name = PAGE_NAME;
        //     ga('send', {
        //         hitType: 'event',
        //         eventCategory: 'Pop-up Survey',
        //         eventAction: 'pop-up survey click',
        //         eventLabel: 'Pop-up Survey is closed on "' + page_name + '" page by outside without completing'
        //     });
        // });

        /**
         * Clicks on the survey form from footer
         */
        $('.survey-block .open-survey-form').on('click', function () {
            var page_name = PAGE_NAME;

            if (page_name == 'Southwest One Report') {
                page_name = 'Home'
            }

            ga('send', {
                hitType: 'event',
                eventCategory: 'Pop-up Survey',
                eventAction: 'pop-up survey click',
                eventLabel: 'Pop-up Survey is open from footer on "' + page_name + '" page'
            });
        });


        /**
         * Surveys response
         */

        $('#form-survey-from .form-control.submit').on('click', function (e) {
            var checked = $('p').find('input:checked'),
                checkedValue = '',
                checkedValueArray = [];

            if (checked.length > 0) {
                checked.each(function () {
                    checkedValueArray.push($(this).val());
                });
                if (checkedValueArray.length > 0) {
                    checkedValue = checkedValueArray.join(' | ');
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Pop-up Survey',
                        eventAction: 'pop-up survey click',
                        eventLabel: checkedValue
                    });
                }
            }
        })
    });
})(jQuery);