(function ($) {
    
})(jQuery);
jQuery(document).ready(function(){    
    jQuery('#hideshow').on('click', function(event) { 
       var lable = jQuery("#hideshow").text().trim();       
       if(lable == "Learn more") {
         jQuery("#hideshow").text("Show less");
         jQuery("#oa_moredetails").slideToggle(700);
        //  jQuery('#hideshow').removeClass('third-button');
         jQuery('#hideshow').addClass('third-button_hover');
       }
       else {          
         jQuery("#hideshow").text("Learn more");
         jQuery("#oa_moredetails").slideToggle(700);
        //  jQuery('#hideshow').addClass('third-button');
         jQuery('#hideshow').removeClass('third-button_hover');
       }
    });
    jQuery('#show_less').on('click', function(event) {
      jQuery( '#hideshow' ).trigger( "click" );
    });

    jQuery('#goals_hideshow').on('click', function(event) { 
       var lable = jQuery("#goals_hideshow").text().trim();       
       if(lable == "Full SDG Alignment") {
         jQuery("#goals_hideshow").text("Show less");
         jQuery("#sdg_details_div").slideToggle(700);
        //  jQuery('#goals_hideshow').removeClass('third-button');
         jQuery('#goals_hideshow').addClass('third-button_hover');
       }
       else {
         jQuery("#goals_hideshow").text("Full SDG Alignment");
         jQuery("#sdg_details_div").slideToggle(700);
        //  jQuery('#goals_hideshow').addClass('third-button');
         jQuery('#goals_hideshow').removeClass('third-button_hover');
       }
    });
    jQuery('#goals_show_less').on('click', function(event) {
      jQuery( '#goals_hideshow' ).trigger( "click" );
    });
});