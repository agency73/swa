(function ($) {
  $(function () {
    var Cookie = {
      create : function (name, value, days) {
        var expires = "";
        if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
          expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
      },
      read : function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') c = c.substring(1, c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
      },
      erase : function (name) {
        this.create(name, "", -1);
      }
    };

    var section = Cookie.read('section');

    if (section && $('#' + section).length) {
      var navH = $('.wrap-navigation').length ? $('.wrap-navigation').height() : 0;
      var position = $('#' + section).find('.service-info').offset().top - navH;
      $(window).scrollTop(position);

      Cookie.erase('section');
    }

    $('.service-info').on('click', function () {
      var $block = $(this),
        $serviceBlock = $block.parents('.topic'),
        id = $serviceBlock.length ? $serviceBlock.attr('id') : 'stories',
        service = $serviceBlock.length ? $serviceBlock.data('service') : null;

      Cookie.create('section', id);
      Cookie.create('service', service);
    });

    $('.navbar-nav li a').on('click', function(){
      Cookie.erase('section');
      Cookie.erase('service');
    });

  });
})(jQuery);
