<?php

/*----------------------------------------------------*/
// WordPress database
/*----------------------------------------------------*/
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', 'utf8mb4_unicode_ci');
$table_prefix = getenv('DB_PREFIX') ? getenv('DB_PREFIX') : 'wp_';

/*----------------------------------------------------*/
// Illuminate database
/*----------------------------------------------------*/
$capsule = new Illuminate\Database\Capsule\Manager();
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => DB_HOST,
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASSWORD,
    'charset'   => DB_CHARSET,
    'collation' => DB_COLLATE,
    'prefix'    => $table_prefix
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$GLOBALS['themosis.capsule'] = $capsule;

/*----------------------------------------------------*/
// Authentication unique keys and salts
/*----------------------------------------------------*/
/*
 * @link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service 
 */
define('AUTH_KEY',         '*Tr$6A;0~WGDB/k/H^fc+M,!=X-40|.fiBd|c7^( -*NR> y,}U3$Tw+O,-j*Fi;');
define('SECURE_AUTH_KEY',  'y]z(dQ-7ax[vq0q2|RX`bIXQ2;P0I()xy9 D<fCwH{d/|ac,%[W ?wAl[P]i.+s|');
define('LOGGED_IN_KEY',    'jHc(kY7*Tm7@>A/&C>Xh{geyg+BYdIOy*5@a,N[tH>yczo e%TO`+k/]xwxEB&Il');
define('NONCE_KEY',        'D0-a9{!o9t:DkJ,tt#4pl._Tjv-*H|]gl2]HM|T_g-jVNp|&~rQVc%GpUb=9<kl:');
define('AUTH_SALT',        'wJO=&/2)MQu4zCn9}+/a8,Ss(g7MOz%)Zt(mHbxCr$N5MMk:)F=!;g+6M/<f$+kb');
define('SECURE_AUTH_SALT', '*5wK+^+^(`8*]kp!>JPp|mEO-$^l|.h1-&iS;$p6[HV<,^_Rq$+Z]K9-xBbE=J z');
define('LOGGED_IN_SALT',   'CE!#+y+kfQY#]pQ$NwIq-|WM[3,{+?jD`Km^;8b76?2*GSIWhdyK(H8|T6|m7+<w');
define('NONCE_SALT',       'Qqq80W8Hq<QPP4gNn@ -T(I5YV{K+3ZZ|w*KTNj|mXk-A`GZa7gmFU;?%o;Nn&>0');

/*----------------------------------------------------*/
// Custom settings
/*----------------------------------------------------*/
define('WP_AUTO_UPDATE_CORE', false);
define('DISALLOW_FILE_EDIT', true);
define('WP_DEFAULT_THEME', 'themosis-theme');


/* Switch site to https */
define('FORCE_SSL_ADMIN', false);
// in some setups HTTP_X_FORWARDED_PROTO might contain
// a comma-separated list e.g. http,https
// so check for https existence
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
    $_SERVER['HTTPS']='on';

/* That's all, stop editing! Happy blogging. */
